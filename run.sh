#!/bin/bash

shift $((OPTIND-1))

if [ "$@" = "redis" ]; then
    echo "Redis Server Yeniden Başlatılıyor"
    redis-server /usr/local/etc/redis.conf
    echo "Başlatıldı."
fi

if [ "$@" = "elastic" ]; then
    echo "Elasticsearch'ün işi uzun sen başka bir terminal session'ına geç istersen."

    elasticsearch
fi
