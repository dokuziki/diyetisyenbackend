<?php

/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 22/02/16
 * Time: 16:59
 */

namespace App\Observers;

use Elasticsearch\ClientBuilder;

class StoreObserver
{
    public function created($model)
    {
        $this->reIndex($model);
    }

    public function inserted($model)
    {
        $this->reIndex($model);
    }

    public function saved($model)
    {
        $this->reIndex($model);
    }

    public function updated($model)
    {
        $this->reIndex($model);
    }

    public function deleting($model)
    {
        $this->deleteIndex($model);
    }

    private function deleteIndex($model)
    {

        $client = ClientBuilder::create()->build();
        $params = [
            'index' => 'stores',
            'type' => 'stores',
            'id' => $model->id
        ];

        $client->delete($params);
    }

    private function reIndex($model)
    {
        $model->addToIndex();
    }
}