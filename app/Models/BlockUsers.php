<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockUsers extends Model
{
    protected $fillable = ['user_id', 'block_id'];
}
