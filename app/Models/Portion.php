<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portion extends Model
{

    public function portion(){
        return $this->belongsTo('App\PortionType', 'type_id');
    }
}
