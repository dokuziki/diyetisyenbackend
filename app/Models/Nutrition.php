<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nutrition extends Model
{
    public $table = 'users';
    protected $hidden = ['password', 'remember_token'];


    public function getPicAttribute($value)
    {
        if($value === ''){
            return asset('uploads/default.jpg');
        }
        return asset('uploads/'.$value);
    }

    public function newQuery($excludeDeleted = true)
    {
        $query = parent::newQuery($excludeDeleted);
        $query->where('status',1);
        return $query;
    }
}
