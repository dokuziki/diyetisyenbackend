<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auth extends Model
{
    //
    public $fillable = ['user_id','token','last_activity','onesignal','device'];

    public function user(){
        return $this->belongsTo('\App\Models\User');
    }
}
