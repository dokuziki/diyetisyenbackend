<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Likes extends Model
{
    protected $casts = [
        'user_id' => 'integer',
        'meal_id' => 'integer'
    ];

    protected $with=['user'];

    public function user(){
        return $this->belongsTo('\App\Models\User', 'user_id');
    }
}
