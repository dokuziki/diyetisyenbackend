<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = ['user_id', 'target_weight', 'purpose', 'height', 'q1', 'q2', 'q3', 'q4'];
    protected $casts = [
        'user_id' => 'integer',
        'id' => 'integer',
        'height' => 'integer'
    ];

    public function getHeightAttribute($value){
        return (integer) $value;
    }

    public function users(){

        return $this->belongsTo('\App\Models\User', 'user_id')
            ->select(['id','first_name','last_name','nickname','pic']);
    }
}
