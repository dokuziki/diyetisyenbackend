<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Times extends Model
{
    public function meal(){
        return $this->hasOne('\App\Models\Meal', 'time_id');
    }
}
