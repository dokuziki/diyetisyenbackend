<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NutritionComments extends Model
{
    protected $fillable = ['id', 'user_id', 'nutritionist_id', 'content'];
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('\App\Models\User', 'user_id');
    }
}
