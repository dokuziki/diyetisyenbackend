<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NutritionistDetail extends Model
{
    public $timestamps = false;
}
