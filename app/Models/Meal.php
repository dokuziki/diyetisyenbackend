<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth as AuthFacade;

class Meal extends Model
{
    protected $table = 'meals';
    protected $fillable = ['user_id', 'pic', 'date', 'time_id', 'approved', 'desc', 'point', 'push_status'];
    protected $appends = ['comment_count','favorite_count','like_count','is_mine','is_liked', 'isFavorite'];

    protected $casts = [
        'user_id' => 'integer',
        'time_id' => 'integer',
        'approved' => 'integer',
        'point' => 'integer',
        'push_status' => 'integer',
        'like_count' => 'integer',
        'comment_count' => 'integer',
        'favorite_count' => 'integer',
        'is_mine' => 'integer'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id')->select(['first_name','last_name','nickname','pic','id','gender','status','nutritionist_id']);
    }

    public function getPicAttribute($value)
    {
        return asset('uploads/meals/'.$value);
    }

    public function getDateAttribute($value)
    {
        return date('d.m.Y', strtotime($value));
    }

    public function scopeActive($query){
        return $query->where('approved',1)->where('status', 1);
    }

    public function scopeNotDeleted($query){
        return $query->where('status',1);
    }

    public function comments(){
            return $this->hasMany('\App\Models\Comment', 'meal_id')->orderBy('id','DESC');
    }


    public function likes(){
        return $this->hasMany('\App\Models\Likes', 'meal_id');
    }


    public function favorites(){
        return $this->hasMany('\App\Models\Favorites', 'meal_id');
    }

    public function time(){
        return $this->belongsTo('\App\Models\Times','time_id')->select(['name','id','push_title']);
    }

    public function getCommentCountAttribute($attribute){
        return $this->comments ? count($this->comments) : 0;
    }

    public function getLikeCountAttribute($attribute){
        return $this->likes ? count($this->likes) : 0;
    }


    public function getFavoriteCountAttribute($attribute){
        return $this->favorites ? count($this->favorites) : 0;
    }

    public function getIsMineAttribute($attribute){
       $user = AuthFacade::user();
        if($user == null){
            return false;
        }else{
            if($user->id == $this->user_id){
                return true;
            }else{
                return false;
            }
        }
    }

    public function scopeOrder($query){
        return $query->orderBy('date','DESC');
    }

    public function scopeNutrition($query,$nutritionist_id){
        return $query->whereHas('user', function($query) use($nutritionist_id){
            return $query->where('nutritionist_id',$nutritionist_id);
        });
    }

    public function nutrition_code(){
        return $this->belongsTo('\App\Models\NutritionCode','user_id','user_id');
    }

    public function payment(){
        return $this->belongsTo('\App\Models\Payments', 'user_id', 'user_id');
    }


    public function getIsLikedAttribute(){
        $user = AuthFacade::user();
        if($user != null && $this->likes()->where('user_id', $user->id)->first() !== null){
            return true;
        }else{
            return false;
        }
    }


    public function getIsFavoriteAttribute(){
        $user = AuthFacade::user();
        if($user != null && $this->favorites()->where('user_id', $user->id)->first() !== null){
            return true;
        }else{
            return false;
        }
    }

}
