<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NutritionCode extends Model
{

    protected $casts = [
        'user_id' => 'integer',
        'used' => 'integer',
        'nutritionist_id' => 'integer'
    ];

    public function user(){
        return $this->belongsTo('\App\Models\User', 'user_id');
    }
}
