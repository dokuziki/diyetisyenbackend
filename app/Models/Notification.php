<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['user_id','sender_id','message','content_type','content_id','is_global'];
    protected $appends = ['sender'];

    protected $casts = [
        'user_id' => 'integer',
        'sender_id' => 'integer',
        'content_id' => 'integer',
        'is_global' => 'integer'
    ];

    public function getSenderAttribute($value){

        if((int)$this->sender_id === 0){
            return [
                'type' => 'system',
                'display_name' => 'Cepte Diyetisyen',
                'display_pic' => asset('default/logo.png')
            ];
        }else{
           $user = User::find($this->sender_id);

            if((int)$user->status === 1){
                return [
                    'type' => 'nutritionist',
                    'display_name' => $user->first_name . ' '. $user->last_name,
                    'display_pic' => $user->pic
                ];

            }else{

                return [
                    'type' => 'user',
                    'display_name' => $user->first_name . ' '. $user->last_name,
                    'display_pic' => $user->pic
                ];
            }
        }
    }

    public function user(){
        return $this->belongsTo('\App\Models\User', 'user_id')
            ->select(['id','first_name','last_name','pic']);
    }

    public function scopeOrder($query){
        return $query->orderBy('id','DESC');
    }

    public function meal(){
        return $this->belongsTo('App\Models\Meal', 'content_id')
            ->select('id', 'user_id', 'pic','date','time_id','point','desc')->with(['time']);
    }

}
