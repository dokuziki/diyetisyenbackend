<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortionType extends Model
{
    protected $fillable = ['id','name', 'description'];
    public $timestamps = false;
    public function portion(){
        return $this->hasMany('App\Models\Portion', 'type_id');
    }
}
