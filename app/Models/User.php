<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth as UserAuth;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'nickname', 'pic', 'first_name', 'last_name', 'password', 'birthday', 'location', 'bio', 'phone', 'fb_id', 'fb_token', 'gender', 'nutritionist_id', "forcep"];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['isPremium', 'isTrial', 'remaining', 'isPrivate', 'dietPayment', 'total_point', 'full_point', 'is_blocked', 'meal_count'];
    protected $with = ['details'];

    protected $casts = [
        'id' => 'integer',
        'nutritionist_id' => 'integer',
        'gender' => 'string',
        'status' => 'string',
        'forcep' => 'integer'
    ];


    public function getKeyForAuth()
    {

        $shuffled = str_shuffle('8Vyeb7vhaliVf0vQCY600lFrN72ALkjtMEAADJ0R3GH9EBAI2g3UKYgPTQyXGc4xXwz1FMqXHwn37qe72ALkjtjUl');


        $auth = Auth::create([
            'user_id' => $this->id,
            'token' => $shuffled,
            'device' => $GLOBALS['client']->type === null || $GLOBALS['client']->type == '' || $GLOBALS['client']->type == 'android' ? '1' : '0'
        ]);

        return $auth->token;
    }


    public function meal()
    {
        return $this->hasMany('\App\Models\Meal', 'user_id');
    }

    public function comment()
    {
        return $this->hasMany('\App\Models\Comment', 'user_id');
    }

    public function like()
    {
        return $this->hasMany('\App\Models\Likes', 'id');
    }

    public function nutritionist()
    {
        return $this->belongsTo('\App\Models\User', 'nutritionist_id', 'id')
            ->select(['first_name', 'last_name', 'pic', 'id', 'gender', 'status']);
    }

    public function notification()
    {
        return $this->hasMany('\App\Models\Notification', 'user_id');
    }

    public function scopeNutrition($query, $nutritionist_id)
    {
        return $query->whereHas('user', function ($query) use ($nutritionist_id) {
            return $query->where('nutritionist_id', $nutritionist_id)
                ->select(['first_name', 'last_name', 'pic', 'id', 'gender', 'status']);
        });
    }

    public function getIsPrivateAttribute()
    {
        $codes = NutritionCode::where('user_id', $this->id)->orderBy('id', 'DESC')->first();

        if ($codes === null) {
            return false;
        } else {
            return true;
        }
    }

    public function getIsBlockedAttribute()
    {
        $login = UserAuth::user();

        $result = false;
        if ($login !== null) {
            $result = BlockUsers::where('user_id', $login->id)->where('block_id', $this->id)->count() > 0 ? true : false;
        }

        return $result;
    }

    public function details()
    {
        return $this->hasOne('\App\Models\UserDetail', 'user_id', 'id')->orderBy('id', 'DESC');
    }

    public function trial_expire_date()
    {


        $user = User::find($this->id);
        /*
        $date = new Carbon($user->created_at);

        return $date->addDays(7)->format('Y-m-d');
        */

        $date = new Carbon($user->created_at);
        return $date->format('Y-m-d');

    }

    public function weightLogs()
    {
        return $this->hasMany('\App\Models\Weight', 'user_id', 'id')->orderBy('id', 'ASC')->take(9);
    }

    public function nutrition_code()
    {
        return $this->hasMany('\App\Models\NutritionCode', 'user_id');
    }

    public function getPicAttribute($value)
    {

        if ($value === '') {
            $pic = asset('default/default.png');
            if ($this->gender === "0") {
                $pic = asset('default/male.png');
            } else if ($this->gender === "1") {
                $pic = asset('default/female.png');
            }

            return $pic;

        }
        return asset('uploads/' . $value);
    }

    public function getDietPaymentAttribute()
    {
        $lastEntity = $this->payment()->orderBy('id', 'DESC')->where('type', 1)->first();

        if ($lastEntity !== null && $lastEntity->isEnd === false && 1 === $lastEntity->status) {
            return true;
        } else {
            return false;
        }
    }

    public function payment()
    {
        return $this->hasMany('\App\Models\Payments', 'user_id');
    }

    public function getTotalPointAttribute()
    {
        $total_point = Meal::where('user_id', $this->id)->sum('point');

        if ($total_point !== null) {
            return (int)$total_point;
        } else {
            return 0;
        }
    }

    public function getMealCountAttribute()
    {
        $meal_count = Meal::where('user_id', $this->id)->count();

        if ($meal_count !== null) {
            return (int)$meal_count;
        } else {
            return 0;
        }
    }

    public function getFullPointAttribute()
    {
        $full_point = Meal::where('user_id', $this->id)->where('point', 10)->count();

        if ($full_point !== null) {
            return (int)$full_point;
        } else {
            return 0;
        }
    }

    public function getLocationAttribute($value)
    {
        if ($value === null) {
            $value = '';
        }
        return $value;
    }

    public function getBioAttribute($value)
    {
        if ($value === null) {
            $value = '';
        }
        return $value;
    }

    public function scopeDietPayment($query)
    {
        return $query->whereHas('payment', function ($query) {
            $query->where('type', 1);
            $query->where('payment_status', 1);
            $query->where('end_date', '<', Carbon::now()->format('Y-m-d'));
        })->count();
    }

    public function scopeMealPayment($query)
    {
        return $query->whereHas('payment', function ($query) {
            $query->where('type', 0);
            $query->where('payment_status', 1);
            $query->where('end_date', '<', Carbon::now()->format('Y-m-d'));
        })->count();
    }


    public function getIsTrialAttribute()
    {
        /*
            $value = false;
        $date = new Carbon($this->created_at);

        if ($date->format('Y-m-d H:i:s') > Carbon::now()->subDays(7)->format('Y-m-d')) {
            $value = true;
        }

        return $value;

         */


        return false;
    }

    public function getIsPremiumAttribute()
    {


        if ($this->getIsTrialAttribute()) {
            return true;
        } else {
            $payment = Payments::where('user_id', $this->id)->where('type', 0)->where('payment_status', 1)->orderBy('id', 'DESC')->first();

            if ($payment === null) {
                return false;
            } else if ($payment->end_date > Carbon::now()) {
                return true;
            } else {
                return false;

            }
        }
    }

    public function getRemainingAttribute()
    {

        $user = User::find($this->id);
        if ($user->created_at !== null)
            $date = new Carbon($user->created_at);
        else
            $date = Carbon::now();

        if ($date->format('Y-m-d') > Carbon::now()->subDays(7)->format('Y-m-d')) {
            return (int)$date->diffInDays(Carbon::now()->subDays(7));
        } else {
            return 0;
        }

    }

    public function getMealCount()
    {
        return Meal::where('user_id', $this->id)->count();
    }
}
