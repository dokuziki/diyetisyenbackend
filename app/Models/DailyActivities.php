<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyActivities extends Model
{
    protected $fillable = ['water', 'activity', 'user_id','date'];
}
