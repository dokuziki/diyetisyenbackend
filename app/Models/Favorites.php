<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorites extends Model
{
    protected $casts = [
        'user_id' => 'integer',
        'meal_id' => 'integer'
    ];

    protected $with = ['user'];

    public function user()
    {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }

    public function meal()
    {
        return $this->belongsTo('\App\Models\Meal', 'meal_id');
    }
}
