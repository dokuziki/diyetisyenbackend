<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coop extends Model
{
    protected $table = 'coop';
    protected $fillable = ['email_extension', 'end_date'];

}


