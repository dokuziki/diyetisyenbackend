<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth as AuthFacade;


class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = ['comment', 'user_id', 'meal_id'];
    protected $appends = ['isNutritionist'];

    protected $casts = [
        'user_id' => 'integer',
        'meal_id' => 'integer'
    ];

    public function meal(){
        return $this->belongsTo('App\Models\Meal', 'meal_id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id')->select(['first_name','last_name','id','pic']);
    }

    public function getIsNutritionistAttribute(){
        $user = AuthFacade::user();
        if($this->user_id != $user->nutritionist_id){
            return false;
        }
        return true;
    }
}
