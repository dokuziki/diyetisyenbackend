<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $appends = ['isEnd'];

    protected $casts = [
        'payment_status' => 'integer',
        'user_id' => 'integer',
        'type' => 'integer',
        'isforced' => 'integer'
    ];

    public function newQuery()
    {
        $query = parent::newQuery();
        $query->orderBy('id', 'DESC');

        return $query;
    }

    public function scopeOrder($query){
        return $query->orderBy('id','DESC');
    }

    public function scopeDietlist($query){
        return $query->where('type',1);
    }

    public function scopePremiumt($query){
        return $query->where('type',0);
    }

    public function getIsEndAttribute($attribute){
        return $this->end_date < Carbon::now()->format('Y-m-d') ? true : false;
    }

    public function user(){
        return $this->belongsTo('\App\Models\User','user_id');
    }


}
