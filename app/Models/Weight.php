<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weight extends Model
{
    protected $fillable = ['user_id', 'weight'];

    protected $casts = [
        'user_id' => 'integer'
    ];

    public function getWeightAttribute($value){
        return (int) $value;
    }
}
