<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChangeNutritionist extends Model
{
    protected $fillable = ['id','user_id', 'nutritionist_id', 'content', 'status'];

    public function users()
        {
            return $this->belongsTo('App\Models\User', 'user_id');
        }
}
