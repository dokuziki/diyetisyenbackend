<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spam extends Model
{
    protected $fillable = ['id', 'user_id', 'adder_id'];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id')->select(['first_name','last_name','nickname','pic','id','gender','status','nutritionist_id']);
    }
    public function adder() {
        return $this->belongsTo('App\Models\User', 'adder_id')->select(['first_name','last_name','nickname','pic','id','gender','status','nutritionist_id']);
    }
}
