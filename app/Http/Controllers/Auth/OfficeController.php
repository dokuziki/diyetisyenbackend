<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class OfficeController extends Controller
{
    public function getLogin(){
        if (Auth::check()) {
            return redirect()->route('office.dash');
        }

        return view('office.auth.index');
    }

    public function postLogin()
    {

        $input = [
            'email' => Input::get('email'),
            'password' => Input::get('password')
        ];


        $user = User::where('email', $input['email'])->first();

        if (Hash::check($input['password'], $user->password)) {
            Auth::login($user, true);

            if ((int)Auth::user()->status !== 2) {
                Auth::logout();
                return [
                    'status' => false,
                    'message' => config('messages.admin.unauthorized')
                ];
            }


            return [
                'status' => true,
                'message' => ''
            ];

        } else {
            return [
                'status' => false,
                'message' => config('messages.admin.loginFailed')
            ];
        }


    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->action('Auth\OfficeController@getLogin');
    }
}
