<?php

namespace App\Http\Controllers\Panel;

use App\Models\Comment;
use App\Models\Dietlist;
use App\Models\Meal;
use App\Models\NutritionCode;
use App\Models\User;

use App\Models\UserPanel;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{

    public function getIndex()
    {

        View::share('title', 'Dashboard');

        $nutritionist_id = $this->userAttr('id');

        $usersIDs = $this->cacheHelp($nutritionist_id . ':genel_user_list', UserPanel::select(['id'])->where('nutritionist_id', $nutritionist_id)->get()->toArray(), 1);

        $usersIDs = collect($usersIDs)->pluck('id')->toArray();

        //Toplam Üye Sayısı
        $data['user_count'] = (int)count($usersIDs);


        //Premium Hasta Sayısı
        $data['premium'] = (int)$this->cacheHelpFunc($nutritionist_id . ':meal_premium2', function () use ($nutritionist_id) {
            return User::where('nutritionist_id', $nutritionist_id)->whereHas('payment', function ($query) {
                        $query->where('type', 0);
                        $query->where('payment_status', 1);
                        $query->where('end_date', '<', Carbon::now()->format('Y-m-d'));
                  })->count();
        }
        , 60);

        //Kod Kullanan Hasta Sayısı
        $data['codes'] = (int)$this->cacheHelp($nutritionist_id . ':codes_count', NutritionCode::where('nutritionist_id', $nutritionist_id)->where('used', 1)->count(), 2);

        $dietlistAll = $this->cacheHelp($nutritionist_id . ':dietlist_count', Dietlist::where('nutritionist_id', $nutritionist_id)->select(['status', 'id'])->get(), 4);
        //Diyet Listesi Satın Alan Hasta Sayısı
        $data['diet_list'] = (int)count($dietlistAll);

        //Yorum Yapılmayan Öğün Sayısı
        $data['comments'] = (int) Meal::whereIn('user_id', $usersIDs)->has('comments', '=', 0)->count();

        $diet = collect($dietlistAll)->where('status', 0)->count();
        $data['diet'] = (int)$diet;


        $mData = $this->cacheHelpFunc($nutritionist_id . ':comment_by_m', function () use ($usersIDs) {
            $mData = [];

            $lastSix = collect(Comment::select(['created_at','id'])->where('created_at', '>', Carbon::now()->subMonths(6)->subDay()->format('Y-m-d H:i:s'))->whereIn('user_id', $usersIDs)->get()->toArray());

            $currentM = (int)Carbon::now()->format('m');

            for ($i = 0; $i < 6; ++$i) {
                $t = $currentM - $i;
                if ($t === 0) {
                    $t = 12;
                } elseif ($t < 0) {
                    $t = 12 - $t;
                }

                $mData[$t] = (int) $lastSix->where('created_at', '>', Carbon::create(Carbon::now()->format('Y'),$t,1,0,0))->where('created_at', '<', Carbon::create(Carbon::now()->format('Y'),$t,31,23,59))->count();
            }

            return $mData;
        }, 20);


        $data['commentsByR'] = Meal::whereIn('user_id', $usersIDs)->where('chat', 1)->orderBy('id', 'desc')->take(10)->get();

        $data['commentByM'] = $mData;

        return view('admin.dash.index', $data);
    }


}
