<?php

namespace App\Http\Controllers\Panel;

use App\Models\NutritionCode;
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class CodesController extends Controller
{
    public function getIndex(){
        View::share('title', 'Özel Hasta Kodu');
        $nutritionist_id = $this->userAttr('id');
        $data['items'] = NutritionCode::where('nutritionist_id',$nutritionist_id)->orderBy('id', 'desc')
            ->with('user')->get();
        return view('admin.codes.index', $data);
    }

    public function postIndex(){
        $input = Input::all();
        $nutritionist_id = $this->userAttr('id');
        for($count = 0; $count <(int)$input['code_count']; $count++) {
            $data[] = strtoupper(str_random(6));
        }

       for($saves=0; $saves<count($data); $saves++){
           $codes = new NutritionCode();
           $codes->used = 0;
           $codes->nutritionist_id = $nutritionist_id;
           $codes->code = $data[$saves];
           $codes->created_at = Carbon::now();
           $codes->save();
        }
        return redirect()->action('Panel\CodesController@getIndex');
    }
}
