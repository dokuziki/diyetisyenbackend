<?php

namespace App\Http\Controllers\Panel;

use App\Models\NutritionistDetail;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class DetailController extends Controller
{
    public function getIndex(){
        View::share('title','Diyetisyen Detayı');
        $data['detail'] = NutritionistDetail::where('nutritionist_id', $this->userAttr('id'))->orderBy('id','DESC')->first();
        return view('admin.detail.index',$data);
    }

    public function postIndex(){

        $input = Input::all();


        $detail = NutritionistDetail::where('nutritionist_id', $this->userAttr('id'))->first();

        if($detail === null){
            $detail = new NutritionistDetail();

        }

        $detail->title = $input['title'];
        $detail->nutritionist_id = $this->userAttr('id');
        $detail->video = $input['video'];
        $detail->information = $input['egitim'];
        $detail->description = $input['description'];
        $detail->instagram = $input['instagram'];
        $detail->facebook = $input['facebook'];
        $detail->twitter = $input['twitter'];
        $detail->linkedin = $input['linkedin'];



        if($detail->save()){
            Session::flash('alert', 'Kayıt Edildi.');
            return redirect()->back();

        }else{
            Session::flash('error', 'Hata');
            return redirect()->back();
        }
    }
}
