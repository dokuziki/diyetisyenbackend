<?php

namespace App\Http\Controllers\Panel;

use App\Library\Push\PushHelper;
use App\Models\Comment;
use App\Models\DailyActivities;
use App\Models\Dietlist;
use App\Models\Meal;
use App\Models\Nutrition;
use App\Models\Payments;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserPanel;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class MealController extends Controller
{
    protected $page_count = 40;

    public function getIndex(){
        View::share('title', 'Hasta Öğünleri');

        $nutritionist_id = $this->userAttr('id');
        $page = Input::get('page',1);
        $data['page'] = $page;


        if (Input::get('type') == 1 /* Genel Onaylanmamış Öğünler */){
            $data['type'] = 1;
             $mealsQuery = Meal::where('approved',0)->where('status',1)
                ->nutrition($nutritionist_id)
                ->with(['user','comments']);



            $data['meals'] = $mealsQuery->orderBy('created_at','DESC')->take(1000)->get();
            return view('admin.meals.index', $data);
        }else { /* Tüm Öğünler */
            $data['type'] = 0;

             $mealsQuery = Meal::nutrition($nutritionist_id)
                ->with(['user','comments'])->where('status',1);


            $data['meals'] =  $mealsQuery->orderBy('created_at','DESC')->take(1000)->get();

            return view('admin.meals.index', $data);

        }
    }

    public function getPremium(){
        View::share('title', 'Premium Hasta Öğünleri');
        $data['type'] = 0;


        $nutritionist_id = $this->userAttr('id');
        $data['codes'] = Meal::where('status',1)->orderBy('created_at','DESC')->take(1000)
            ->with(['user','comments'])->nutrition($nutritionist_id)->has('nutrition_code')
            ->get();

        $data['premiums'] = Meal::where('status',1)->orderBy('created_at','DESC')->take(1000)
            ->with(['user','comments'])->nutrition($nutritionist_id)->whereHas('payment',function($q){
              $q->where('payment_status',1)->where('type',0)->where('end_date','>',Carbon::now()->format('Y-m-d'));
            })
            ->get();
        return view('admin.meals.premium', $data);
    }

    public function getUpsert($id=false){
        View::share('title', 'Hasta Öğün Detayı');

        $target = Meal::find($id);

        $data['items'] = Meal::with('time', 'user')->where('user_id',$target->user_id)->where('approved',0)->where('status',1)->orderBy('time_id','ASC')->get();
        $data['info'] = User::with('details')->with('weightLogs')->find($target->user_id);
        $data['comments'] = Meal::where('user_id', $target->user_id)->where('approved',1)->has('comments','=',0)->orderBy('id','DESC')->take(20)->get();
        $data['payments']['list'] = Payments::where('type', 1)->where('payment_status',1)->where('user_id',$target->user_id)->orderBy('id', 'DESC')->first();
        $data['purpose'] = UserDetail::where('user_id',$target->user_id)->first();
        $data['payments']['premium'] = Payments::where('type', 0)->where('payment_status',1)->where('user_id',$target->user_id)->orderBy('id', 'DESC')->first();
        $data['dietlist'] = Dietlist::where('user_id',$target->user_id)->where('status',1)->orderBy('id','DESC')->first();
        $data['lists'] = Dietlist::where('user_id',$target->user_id)->where('status',1)->get();

        $data['water'] = DailyActivities::where('user_id', $target->user_id)->take(10)->orderBy('id','DESC')->get();
        return view('admin.meals.upsert', $data);
    }

    public function postUpsert(){

        $input = Input::all();
        $nutritionist_id = $this->userAttr('id');
        $meal = Meal::find(Input::get('id'));
        $comment = new Comment();


        if ($meal !== null) {
            $meal->id = $input['id'];
            $comment->meal_id = $input['id'];

            $comment->user_id = $nutritionist_id;
            $comment->comment = $input['comment'];
            $meal->approved = 1;
            $meal->point = $input['point'];

            if ($input['comment'] != null && $input['comment'] !== ""){
                PushHelper::sendMealComment($meal->user->id,$meal->id);
                $comment->save();
            }


            if($meal->save()){
                PushHelper::sendMealApply($meal->user->id,$meal->id);
                Session::flash('alert', 'Kayıt Edildi.');

                return Redirect::back();

            }else{
                Session::flash('error', 'Hata');
                return Redirect::back();
            }
        }

    }

    public function postComment(){
        $input = Input::all();
        $comment = new Comment();
        $nutritionist = Auth::user();
        $comment->user_id = $nutritionist->id;
        $comment->meal_id = $input['id'];
        $comment->comment = $input['comments'];

        $meal = Meal::find($input['id']);
        $meal->chat = 0;
        $meal->save();

        if($comment->save()){
            PushHelper::sendMealComment($meal->user->id,$meal->id);
            Session::flash('alert', 'Kayıt Edildi.');
            return Redirect::back();

        }else{
            Session::flash('error', 'Hata');
            return Redirect::back();
        }

    }

    public function getChat($id = false){
        View::share('title', 'Premium Hasta Öğünleri');

        $nutritionist_id = $this->userAttr('id');

        $usersIDs = $this->cacheHelp($nutritionist_id . ':genel_user_list', UserPanel::select(['id'])->where('nutritionist_id', $nutritionist_id)->get()->toArray(), 4);
        $usersIDs = collect($usersIDs)->pluck('id')->toArray();


        if($id){
            $data['chat'] = Meal::with(['comments','comments.user'])->whereIn('user_id', $usersIDs)->where('chat',1)->where('id', $id)->first();

            if($data['chat'] === null){
                return Redirect::route('admin.meals.chat');
            }
        }else{
            $data['chat'] = Meal::with(['comments','comments.user'])->whereIn('user_id', $usersIDs)->where('chat',1)->first();
        }

        if($data['chat'] === null ){
            Session::flash('alert', 'Bekleyen mesaj yok.');
            return Redirect::route('admin.dash');

        }
        $data['items'] = Meal::where('chat',1)->has('comments','>',1)->whereIn('user_id', $usersIDs)->orderBy('id','desc')->take(100)->get();

        return view('admin.meals.chat', $data);
    }

    public function getCloseChat(){
        $meal = Meal::find(Input::get('id'));
        $meal->chat = 0;

        if($meal->save()){
            Session::flash('alert', 'Kayıt Edildi.');
            return Redirect::route('admin.meals.chat');

        }else{
            Session::flash('error', 'Hata');
            return Redirect::route('admin.meals.chat');
        }
    }
}
