<?php

namespace App\Http\Controllers\Panel;

use App\Models\Auth;
use App\Models\Dietlist;
use App\Models\Payments;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserPanel;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class InfoController extends Controller
{
    public function getIndex(){
        View::share('title', 'Hasta Detayları');
        $nutritionist_id = $this->userAttr('id');

        $userQuery = UserPanel::where('id' , '>', 0);

        if(Input::get('f_key',false) && Input::get('f_key') !== ''){
            $userQuery= $userQuery->where('first_name', 'LIKE', '%'.Input::get('f_key').'%');
        }

        if(Input::get('l_key',false) && Input::get('l_key') !== ''){
            $userQuery = $userQuery->where('last_name', 'LIKE', '%'.Input::get('l_key').'%');
        }


        $data['users'] = $userQuery->where('nutritionist_id',$nutritionist_id)->paginate(40);



        return view('admin.userinfo.index',$data);
    }

    public function getUpsert($id=false){
        View::share('title', 'Hasta Detayı');
        $data['item'] = User::with('details')->with('weightLogs')->find($id);
        $data['purpose'] = UserDetail::where('user_id',$id)->first();
        $device = Auth::where('user_id',$id)->orderBy('id','DESC')->first();
        $data['lists'] = Dietlist::where('user_id',$id)->where('status',1)->get();
        $data['payments']['list'] = Payments::where('type', 1)->where('payment_status',1)->where('user_id',$id)->orderBy('id', 'DESC')->first();
        $data['payments']['premium'] = Payments::where('type', 0)->where('payment_status',1)->where('user_id',$id)->orderBy('id', 'DESC')->first();

        $data['device'] = '';
        if($device === null){
            $data['device'] = 'Belirsiz';

        }else{

            if ($device->device === 'ios'){
                $data['device'] = 'IOS';
            }else{
                $data['device'] = 'Android';
            }

        }

        return view('admin.userinfo.upsert',$data);
    }
}
