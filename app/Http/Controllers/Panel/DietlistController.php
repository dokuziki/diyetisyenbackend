<?php

namespace App\Http\Controllers\Panel;

use App\Library\Push\PushHelper;
use App\Models\Dietlist;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class DietlistController extends Controller
{
    public function getIndex(){

        View::share('title', 'Diyet Listesi');
        $nutritionist_id = $this->userAttr('id');
        $data['diets'] = Dietlist::where('nutritionist_id', $nutritionist_id)
            ->where('status',0)
            ->with(['users'])
            ->get();

        $data['lists'] = Dietlist::where('nutritionist_id', $nutritionist_id)
            ->where('status',1)
            ->with(['users'])
            ->get();

        return view('admin.list.index',$data);
    }

    public function getUpsert($id = false){
        View::share('title', 'Diyet Listesi Detayı');
        $data['item'] = Dietlist::with(['users'])->find($id);
        $data['info'] = User::with('details')->with('weightLogs')->find($data['item']['users']->id);
        return view('admin.list.upsert',$data);
    }

    public function postUpsert(){
        $input = Input::all();
        $nutritionist_id = $this->userAttr('id');

        $list = new Dietlist();
        $list->exists = Input::get('id');

        if ($list->exists) {
            $list->id = Input::get('id');
        }
        $list->title = $input['title'];
        $list->date = $input['date'];
        $list->purpose = $input['purpose'];
        $list->description = $input['description'];
        $list->sabah = $input['sabah'];
        $list->kahvalti = $input['kahvalti'];
        $list->ara_ogun1 = $input['ara_ogun1'];
        $list->ogle = $input['ogle'];
        $list->ara_ogun2 = $input['ara_ogun2'];
        $list->aksam = $input['aksam'];
        $list->ara_ogun3 = $input['ara_ogun3'];
        $list->notlar = $input['notlar'];
        $list->status = 1;

        if($list->save()){
            PushHelper::sendDietList($list->id);
            Session::flash('alert', 'Diyet Listesi Kayıt Edilmiştir.');
            return redirect()->action('Panel\DietlistController@getIndex');

        }else{
            Session::flash('error', 'Diyet Listesi Kayıt Edilememiştir.');
            return redirect()->action('Panel\DietlistController@getIndex');
        }
    }
}
