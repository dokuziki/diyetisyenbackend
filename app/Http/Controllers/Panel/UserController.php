<?php

namespace App\Http\Controllers\Panel;

use App\Models\User;
use App\Models\UserPanel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function getIndex(){

        if ($this->userRole !==  'admin') {
            Session::flash('error', 'Bu Sayfayı Görüntüleme Yetkiniz Yok');
            return redirect()->action('Panel\HomeController@getIndex');
        }

        View::share('title', 'Kullanıcı Hesapları');

        $data['users'] = UserPanel::all();

        return view('admin.user.index', $data);
    }

    public function getUpsert($id=false){
        $data['item'] = User::find($id);
        return view('admin.user.upsert',$data);
    }

    public function postUpsert(){

        $input = Input::all();


        $user = new User();

        $user->exists = Input::get('id');

        if ($user->exists) {
            $user->id = Input::get('id');
        }

        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->email = $input['email'];
        $user->status = $input['role'];


        if($input['password'] !== "" && $input['password'] !== null){
            $user->password = bcrypt($input['password']);
        }


        if($user->save()){
            Session::flash('alert', Config::get('messages.admin.editUserTrue'));
            return redirect()->action('Panel\UserController@getIndex');

        }else{
            Session::flash('error', Config::get('messages.admin.editUserFalse'));
            return redirect()->action('Panel\UserController@getIndex');
        }
    }

    public function getDelete($id){

        $user = User::where('id',$id)->first();
        $user->status = 3;

        $user->save();

        Session::flash('alert', 'Kullanıcı İnaktif Hale Getirildi.');
        return Redirect::back();

    }
}
