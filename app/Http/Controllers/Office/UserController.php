<?php

namespace App\Http\Controllers\Office;

use App\Models\Comment;
use App\Models\Meal;
use App\Models\Payments;
use App\Models\Spam;
use App\Models\User;
use App\Models\UserPanel;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function getIndex(){

        View::share('title', 'Kullanıcı Hesapları');

        $userQuery = UserPanel::where('id' , '>', 0);

        if(Input::get('f_key',false) && Input::get('f_key') !== ''){
            $userQuery= $userQuery->where('first_name', 'LIKE', '%'.Input::get('f_key').'%');
        }

        if(Input::get('l_key',false) && Input::get('l_key') !== ''){
            $userQuery = $userQuery->where('last_name', 'LIKE', '%'.Input::get('l_key').'%');
        }


        $data['users'] = $userQuery->paginate(40);
        $data['loginas'] = false;


        return view('office.user.index', $data);
    }

    public function getUpsert($id=false){
        View::share('title', 'Kullanıcı Bilgileri');

        $data['item'] = User::with('details')->with('weightLogs')->find($id);

        $data['total_count'] = Meal::where('user_id', $id)->count();
        $data['comment_count'] = Comment::where('user_id',$id)->count();
        $data['point_count'] = Meal::where('user_id',$id)->where('point',10)->count();
        $data['approve_count'] = Meal::where('user_id', $id)->where('approved',1)->count();
        $data['premium'] = Payments::where('user_id', $id)
            ->where('end_date','>',Carbon::now()->format('Y-m-d'))->where('payment_status',1)
            ->where('type',0)->first();
        $data['diet_list'] = Payments::where('user_id', $id)
            ->where('end_date','>',Carbon::now()->format('Y-m-d'))->where('payment_status',1)
            ->where('type',1)->first();
        return view('office.user.upsert',$data);
    }

    public function postUpsert(){

        $input = Input::all();


        $user = new User();

        $user->exists = Input::get('id') === "false" ? false : true;

        if ($user->exists) {
            $user->id = Input::get('id',false);
        }

        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->email = $input['email'];
        $user->status = $input['role'];
        $user->forcep = $input['forcep'];

        if(isset($input['image'])){

            $name = str_random(15).'.jpg';
            $filename = 'uploads/'.$name;
            $file = public_path($filename);

            if(Image::make(Input::file('image'))->save($file)){
                $user->pic = $name;

            }else{
                Session::flash('error', 'Kullanıcı Fotoğrafı Yüklenirken Bir Sorun Oluştu');
                return redirect()->back();
            }
        }
        //
        //



        if($input['password'] !== "" && $input['password'] !== null){
            $user->password = Hash::make($input['password']);
        }

        if($user->save()){
            Session::flash('alert', Config::get('messages.admin.editUserTrue'));
            return redirect()->back();

        }else{
            Session::flash('error', Config::get('messages.admin.editUserFalse'));
            return redirect()->action('Office\UserController@getIndex');
        }
    }

    public function getDelete($id){

        $user = new User();
        $user = User::where('id',$id)->first();
        $user->status = 3;

        $user->save();

        Session::flash('alert', 'Kullanıcı İnaktif Hale Getirildi.');
        return Redirect::back();

    }


    public function getSpam(){
        View::share('title', 'Kullanıcı Hesapları');

        $data['spams'] = Spam::all();

        return view('office.user.spam', $data);
    }


    public function getDeleteSpam($id=false){
        Spam::where('id',$id)->delete();
        return redirect()->back();
    }

    public function getDiyetisyen(){
        View::share('title', 'Diyetisyen Hesapları');

        $data['users'] = User::where('status', 1)->paginate(20);
        $data['loginas'] = true;

        return view('office.user.index', $data);
    }


    public function getLogas($id=false){

        $user = User::find($id);
        Auth::login($user, true);

        return redirect()->action('\App\Http\Controllers\Panel\HomeController@getIndex');
    }

}
