<?php

namespace App\Http\Controllers\Office;

use App\Models\About;
use App\Models\Terms;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class AboutController extends Controller
{
    public function getAbout(){

        View::share('title', 'Hakkımızda');
        $data['item'] = About::find(1);
        return view('office.pages.about', $data);
    }

    public function postAbout(){
        $id=1;
        $input = Input::all();
        $about = new About();


        $about->exists = (Input::get('id') === "false") ? false : true;

        if ($about->exists) {
            $about->id = $id;
        }

        $about->content = $input['description'];

        if($about->save()){
            Session::flash('alert', 'Kayıt Edildi.');
            return redirect()->back();

        }else{
            Session::flash('error', 'Hata');
            return redirect()->back();
        }
    }

    public function getBlog(){

        View::share('title', 'Blog');
        $data['item'] = About::find(2);
        return view('office.pages.blog', $data);
    }

    public function postBlog(){
        $id=2;
        $input = Input::all();
        $about = new About();


        $about->exists = (Input::get('id') === "false") ? false : true;

        if ($about->exists) {
            $about->id = $id;
        }

        $about->content = $input['description'];

        if($about->save()){
            Session::flash('alert', 'Kayıt Edildi.');
            return redirect()->back();

        }else{
            Session::flash('error', 'Hata');
            return redirect()->back();
        }
    }
}
