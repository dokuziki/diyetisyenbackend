<?php

namespace App\Http\Controllers\Office;

use App\Models\Payments;
use App\Models\User;
use App\Models\UserPanel;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    public function getIndex() {

        View::share('title', 'Dashboard');

        //Toplam Üye Sayısı
        $data['user_count'] = UserPanel::count();

        //Toplam Diyetisyen Sayısı
        $data['nutrition_count'] = UserPanel::where('status',1)->count();

        //Premium Üye Sayısı
        $data['premium'] = User::whereHas('payment', function ($query){
            $query->where('type',0)->where('payment_status',1);
        })->count();

        //Diyet Listesi Alan Üye Sayısı
        $data['diet_list'] = User::whereHas('payment', function ($query){
            $query->where('type',1)->where('payment_status',1);
        })->count();

        //Haftalık Ödeme
        $pay_weeks = Payments::where('payment_status',1)
            ->where('start_date','>', Carbon::now()->subWeeks(12)->format('Y-m-d'))
            ->where('type',0)->distinct('user_id')
            ->get();

        $weekNumber = Carbon::now()->format('W');
        $weeks = [];

        for ($i = 0; $i<12; ++$i){
            $weeks[$weekNumber-$i] = [];
        }

        foreach($pay_weeks as $payment){
            $start_date= $payment->start_date;
            $date = new \DateTime($start_date);
            $week = $date->format("W");
            $weeks[$week][$payment->user_id][] = $payment->id;
        }

        $calculated_week =  [];

        foreach ($weeks as $k=>$w){
            $calculated_week[$k] = count($w);
        }

        $data['pay_weeks'] = $calculated_week;
        $data['paid_users'] =  Payments::where('type',1)->where('payment_status',1)
            ->whereBetween('start_date', [Carbon::now()->subDays(10)->format('Y-m-d') , Carbon::now()->subDays(6)->format('Y-m-d')])
            ->with(['user'])->get();

        return view('office.dash.index', $data);
    }


}
