<?php

namespace App\Http\Controllers\Office;

use App\Models\Comment;
use App\Models\Meal;
use App\Models\Payments;
use App\Models\Spam;
use App\Models\User;
use App\Models\UserPanel;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function getIndex()
    {
        set_time_limit(99999999);
        ini_set('max_execution_time', 0);
        ini_set('default_socket_timeout', 0);

        $time = Input::get('time','w');

        if($time === 'm'){
            $startDate = Carbon::now()->subMonth();
        }elseif ($time === 'y'){
            $startDate = Carbon::now()->subYear();
        }elseif ($time === 'w'){
            $startDate = Carbon::now()->subWeek();
        }

        $endDate = Carbon::now();


    return  $this->getDownload($startDate,$endDate);
    }

    public function getDownload($startDate,$endDate)
    {


        Excel::create('detailed_report_started_at_' . $startDate->format('d-m-Y'), function ($excel) use ($startDate, $endDate) {

            // Set the title
            $excel->setTitle('Cepte Diyetisyen Detaylı Excel Raporu');

            // Chain the setters
            $excel->setCreator('coskudemirhan')
                ->setCompany('Ward Agency');

            $users = User::where('status', 0)->select(['first_name', 'last_name', 'nickname', 'fb_id', 'email', 'birthday', 'phone', 'location', 'gender', 'nutritionist_id'])->get();
            $list = [];
            foreach ($users as $user) {

                if ($user->birthday !== null)
                    $birth = (int)Carbon::createFromFormat('Y-m-d', $user->birthday)->diff(Carbon::now())->format('%y');
                else
                    $birth = '';

                $gender = $user->gender == 1 ? 'Kadın' : 'Erkek';

                if ($user->gender == 2) {
                    $gender = 'Belirsiz';
                }

                if ($user->nutritionist_id !== null) {
                    $nutritionist = $user->nutritionist->first_name . ' ' . $user->nutritionist->last_name;
                } else {
                    $nutritionist = '';
                }

                $list[] = [
                    'id' => $user->id,
                    'İsim' => $user->first_name,
                    'Soyisim' => $user->last_name,
                    'Nick' => $user->nickname,
                    'Email' => $user->email,
                    'Telefon' => $user->phone,
                    'Lokasyon' => $user->location,
                    'Cinsiyet' => $gender,
                    'Diyetisyen' => $nutritionist,
                    'Yaş' => $birth,
                    'Facebook' => $user->fb_id === null ? 'Yok' : 'Var',
                ];
            }

            $excel->sheet('Kullanıcılar', function ($sheet) use ($list) {
                $sheet->setOrientation('landscape');
                $sheet->fromArray($list);
            });


            $payments = Payments::where('payment_status', 1)->whereBetween('created_at', [$startDate, $endDate])->orderBy('created_at', 'desc')->get();
            $list2 = [];
            foreach ($payments as $payment) {

                if ($payment->type === 1)
                    $type = 'Diyet Listesi';
                else
                    $type = 'Premium';

                $list2[] = [
                    'Tip' => $type,
                    'Kullanıcı #Id' => $payment->user_id,
                    'Başlangıç Tarihi' => $payment->start_date,
                    'Bitiş Tarihi' => $payment->end_date,
                    'Ödeme Durumu' => 1
                ];
            }


            $excel->sheet('Ödeme Kayıtları', function ($sheet) use ($list2) {
                $sheet->setOrientation('landscape');
                $sheet->fromArray($list2);
            });


        })->download('xls');;


    }


}
