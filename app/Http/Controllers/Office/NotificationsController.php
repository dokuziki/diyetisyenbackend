<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Api\PushController;
use App\Http\Responses\Success;
use App\Library\Push\PushHelper;
use App\Models\Auth;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


class NotificationsController extends Controller
{

    public function __construct()
    {
    }

    public function getIndex($page=0) {

        View::share('title', 'Bildirimler');
        $data['pushs'] = PushHelper::GetPushs($page);

        return view('office.notifications.index',$data);
    }

    public function postIndex(){
        $input = Input::all();


        $userQuery = User::select(['id']);

        if((int)$input['gender'] !== 2){
            $userQuery = $userQuery->where('gender',$input['gender']);
        }


        if($input['type'] === 'premium'){
            $userQuery = $userQuery->whereHas('payment', function($query){
                $query->where('payment_status', 1)->where('end_date', '>' , Carbon::now());
            });
        }

        if($input['type'] === 'normal'){
            $userQuery = $userQuery->whereHas('payment', function($query){
                $query->where(function($sub){
                    $sub->where('payment_status', 0)->where('end_date', '>' , Carbon::now());
                });
            });
        }

        if($input['type'] === 'list'){
            $userQuery = $userQuery->whereHas('payment', function($query){
                $query->where(function($sub){
                    $sub->where('payment_status', 1)->where('type',1)->where('start_date', '>' , Carbon::now()->subWeek(1));
                });
            });
        }

        if($input['type'] === 'ozel'){
            $userQuery = $userQuery->has('nutrition_code');
        }

        $users = $userQuery->get();


        if($input['type'] === 'all'){
            PushHelper::sendAllPush($input['message']);
        }else{
            PushHelper::sendPrivatePush($users,$input['message']);
        }

        Session::flash('alert', 'Bildirim Gönderildi.');
        return redirect()->action('Office\NotificationsController@getIndex');

    }

    public function postPersonal(){
        $input = Input::all();

        $user = User::where('id',(int)$input['username'])->get();

        if(count($user) > 0 ){
            PushHelper::sendPrivatePush($user,$input['message']);

            Session::flash('alert', 'Bildirim Gönderildi.');
            return redirect()->action('Office\NotificationsController@getIndex');
        }else{

            Session::flash('error', 'Kullanıcı Bulunamadı.');
            return redirect()->action('Office\NotificationsController@getIndex');
        }
    }

    public function getPersonalSearch(){
        $user = User::select('id','first_name','last_name');

            foreach(explode(' ',Input::get('q')) as $key => $element) {
                if($key == 0) {
                    $user->where('first_name', 'like', $element);
                }else{
                    $user->Where('last_name', 'like', $element);
                }
            }

        $data = $user->orderBy('first_name','ASC')->orderBy('last_name','ASC')->get()->toArray();

        return response()->json(['results' => $data]);
    }

    public function getPersonalIdSearch(){
        $user = User::select('id','first_name','last_name');

        foreach(explode(' ',Input::get('q')) as $key => $element) {
            $user->where('id',$element);
        }

        $data = $user->orderBy('first_name','ASC')->orderBy('last_name','ASC')->get()->toArray();

        return response()->json(['results' => $data]);
    }
}
