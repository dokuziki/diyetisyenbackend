<?php

namespace App\Http\Controllers\Office;

use App\Http\Responses\Fail;
use App\Http\Responses\Success;
use App\Models\ChangeNutritionist;
use App\Models\Comment;
use App\Models\Dietlist;
use App\Models\Meal;
use App\Models\NutritionCode;
use App\Models\NutritionComments;
use App\Models\NutritionistDetail;
use App\Models\Payments;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class NutritionController extends Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function getIndex()
    {

        View::share('title', 'Diyetisyen Hesapları');

        $data['users'] = User::where('status', 1)->get();

        return view('office.nutrition.index', $data);
    }

    public function getUpsert($id = false)
    {
        View::share('title', 'Diyetisyen Hesapları');

        $data['item'] = User::find($id);
        $data['detail'] = NutritionistDetail::find($id);
        $data['users'] = User::where('nutritionist_id', $id)->count();
        $data['codes'] = NutritionCode::where('nutritionist_id', $id)->where('used', 1)->count();


        $yil = Carbon::now()->format('Y');
        $ay = Carbon::now()->format('m');
        $days = cal_days_in_month(CAL_GREGORIAN, $ay, $yil);
        $ilk = strtotime($yil . '-' . $ay . '-01 00:00:00');
        $son = strtotime($yil . '-' . $ay . '-' . $days . ' 00:00:00');
        for ($x = 1; $x <= $days; $x++) {
            $gun_al = strtotime($yil . '-' . $ay . '-' . $x . ' 00:00:00');
            $gun_bas[] = date('d.m.Y', $gun_al);
        }

        $dys = [];

        for ($i = 30; $i > 0; --$i) {
            $dys[Carbon::now()->subDays($i)->format('Y-m-d')] = [
                (int)0 => array(),
                (int)1 => array()
            ];
        }

        $diet_lists = Payments::whereBetween('start_date', [Carbon::now()->subDays(30), Carbon::now()])
            ->where('payment_status', 1)->whereHas('user', function ($query) use($id){
                $query->where('nutritionist_id', $id);

            })->get();




        foreach ($diet_lists as $pym) {

            $day = date("Y-m-d", strtotime($pym->start_date));

            $dys[(string)$day][(int)$pym->type][] = [];
        }

        foreach ($dys as &$ref) {
            if(!isset($ref[0])){
                $ref[0] = [];
            }

            if(!isset($ref[1])){
                $ref[1] = [];
            }
        }

        $data['diet_lists'] = $dys;
        $data['approve_list'] = Dietlist::where('status',1)->where('nutritionist_id', $id)->count();
        $data['wait_list'] = Dietlist::where('status',0)->where('nutritionist_id', $id)->count();

        $commentsByM =  Comment::where('user_id',$id)->get()->groupBy(function($val) {
            return Carbon::parse($val->created_at)->format('m');
        })->toArray();

        $mData = [];

        $mData['01'] = [];
        $mData['02'] = [];
        $mData['03'] = [];
        $mData['04'] = [];
        $mData['05'] = [];
        $mData['06'] = [];
        $mData['07'] = [];
        $mData['08'] = [];
        $mData['09'] = [];
        $mData['10'] = [];
        $mData['11'] = [];
        $mData['12'] = [];

        foreach ($commentsByM as $key => $comments){
            $mData[$key] = count($comments);
        }
        $data['commentByM'] = $mData;



        $data['meals'] = Meal::where('approved', 0)->where('status', 1)->nutrition($id)
            ->with(['user'])->count();

        $data['approve_meal'] = Meal::where('approved', 1)->where('status', 1)->nutrition($id)
            ->with(['user'])->count();
        return view('office.nutrition.upsert', $data);
    }

    public function postUpsert()
    {

        $input = Input::all();


        $user = new User();

        $user->exists = Input::get('id');

        if ($user->exists) {
            $user->id = Input::get('id');
        }

        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->email = $input['email'];
        $user->status = $input['role'];

        if ($user->save()) {
            Session::flash('alert', Config::get('messages.admin.editUserTrue'));
            return redirect()->action('Office\UserController@getIndex');

        } else {
            Session::flash('error', Config::get('messages.admin.editUserFalse'));
            return redirect()->action('Office\UserController@getIndex');
        }
    }

    public function getChange()
    {

        View::share('title', 'Diyetisyen Değişiklik Talebi');
        $data['changes'] = ChangeNutritionist::where('status', 0)
            ->with(['users', 'users.nutritionist'])
            ->orderBy('id', 'DESC')
            ->get();
        $data['approves'] = ChangeNutritionist::where('status', 1)
            ->with(['users', 'users.nutritionist'])
            ->get();
        return view('office.nutrition.change', $data);
    }

    public function getDetail($id = false)
    {
        View::share('title', 'Diyetisyen Değişiklik Talebi');
        $data['item'] = ChangeNutritionist::with(['users', 'users.nutritionist'])->find($id);
        $data['nutritions'] = User::where('status', 1)->get();
        return view('office.nutrition.detail', $data);
    }

    public function postDetail()
    {
        $input = Input::all();
        $user = User::where('id', $input['userId'])->first();
        $change = ChangeNutritionist::where('id', $input['id'])->first();
        $change->status = 1;
        $user->nutritionist_id = $input['nutritionist'];

        if ($change->save()) {
            $user->save();
            Session::flash('alert', Config::get('messages.admin.editUserTrue'));
            return redirect()->action('Office\NutritionController@getChange');
        }
        Session::flash('alert', Config::get('messages.admin.editUserTrue'));
        return redirect()->back();
    }

    public function getComments($id = false)
    {

        $data['users'] = NutritionComments::where('status', 0)->with(['user'])->get();

        if (!$id) {
            View::share('title', 'Yorumlar');
            $data['comment'] = NutritionComments::first();
        }else{
            View::share('title', 'Yorumlar');
            $data['comment'] = NutritionComments::find($id);
        }

        $data['usr'] = User::with(['nutritionist'])->find($data['comment']['user_id']);

        return view('office.nutrition.comments', $data);
    }

    public function postComments()
    {
        $comments = NutritionComments::find(Input::get('id'));
        $comments->status = 1;

        if ($comments->save()) {
            return redirect()->action('Office\NutritionController@getComments');
        }

    }

    public function getComment($id)
    {
        View::share('title', 'Diyetisyen Yorumları');

        $day = Input::get('day',0);

        $comments = Comment::where('user_id',$id)->orderBy('id','DESC')->where('created_at', '>=', Carbon::now()->subDays($day))->where('created_at', '<', Carbon::now()->subDays($day-1))->get();

        return view('office.nutrition.comment', ['comments' => $comments,'day' => $day]);
    }


    public function getDelete($id)
    {

        $user = new User();
        $user = User::where('id', $id)->first();
        $user->status = 3;

        $user->save();

        Session::flash('alert', 'Kullanıcı İnaktif Hale Getirildi.');
        return Redirect::back();

    }

    public function getChangeDelete($id){
        $change = ChangeNutritionist::find($id);
        $change->delete();
        Session::flash('alert', 'Diyetisyen Değiştirme Talebi Silinmiştir.');
        return Redirect::back();
    }

    public function getCommentDelete($id){
        $change = NutritionComments::where('id',$id)->delete();
        Session::flash('alert', 'Yorum Silinmiştir.');
        return redirect()->action('Office\NutritionController@getComments');
    }


}
