<?php

namespace App\Http\Controllers\Office;

use App\Models\Comment;
use App\Models\Coop;
use App\Models\Meal;
use App\Models\Payments;
use App\Models\Spam;
use App\Models\User;
use App\Models\UserPanel;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class CoopController extends Controller
{
    public function __construct()
    {
        parent::__construct();

    }
    public function getIndex(){

        View::share('title', 'Email Kampanyaları');
        $data['coops'] = Coop::get();

        return view('office.coop.index', $data);
    }

    public function getUpsert($id = false){
        View::share('title', 'Kullanıcı Bilgileri');

        $data['coop'] = Coop::where('id',$id)->first();
        return view('office.coop.upsert',$data);

    }

    public function postUpsert(){
        $coop = new Coop();
        $coop->exists = Input::get('id',false) === "false" ? false : true;

        if ($coop->exists) {
            $coop->id = Input::get('id',false);
        }


        $coop->email_extension = Input::get('email');
        $coop->end_date = Input::get('end_date');


        if ($coop->save()){
            Session::flash('warning', 'Kayıt Eklendi.');
            return redirect()->action('Office\CoopController@getIndex');
        }
        Session::flash('alert', 'Kayıt Eklenemedi.');
        return Redirect::back();
    }

    public function getDelete($id){
        $coop = Coop::find($id);
        if ($coop->delete()){
            Session::flash('warning', 'Mail Silindi');
            return Redirect::back();
        }
        Session::flash('alert', 'Mail Silinemedi.');
        return Redirect::back();
    }

}
