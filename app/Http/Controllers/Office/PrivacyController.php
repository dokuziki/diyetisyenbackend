<?php

namespace App\Http\Controllers\Office;

use App\Models\Privacy;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class PrivacyController extends Controller
{
    public function getPrivacy(){

        View::share('title', 'Gizlilik Koşulları');
        $data['item'] = Privacy::find(1);
        return view('office.pages.privacy', $data);
    }

    public function postPrivacy(){
        $id=1;
        $input = Input::all();
        $privacy = new Privacy();


        $privacy->exists = (Input::get('id') === "false") ? false : true;

        if ($privacy->exists) {
            $privacy->id = $id;
        }

        $privacy->content = $input['description'];

        if($privacy->save()){
            Session::flash('alert', 'Kayıt Edildi.');
            return redirect()->back();

        }else{
            Session::flash('error', 'Hata');
            return redirect()->back();
        }
    }
}
