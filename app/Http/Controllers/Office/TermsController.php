<?php

namespace App\Http\Controllers\Office;

use App\Models\Terms;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class TermsController extends Controller
{
    public function getTerms(){

        View::share('title', 'Üyelik Sözleşmesi ve Kullanım Koşulları');
        $data['item'] = Terms::find(1);
        return view('office.pages.terms', $data);
    }

    public function postTerms(){
        $id=1;
        $input = Input::all();
        $terms = new Terms();


        $terms->exists = (Input::get('id') === "false") ? false : true;

        if ($terms->exists) {
            $terms->id = $id;
        }

        $terms->content = $input['description'];

        if($terms->save()){
            Session::flash('alert', 'Kayıt Edildi.');
            return redirect()->back();

        }else{
            Session::flash('error', 'Hata');
            return redirect()->back();
        }
    }
}
