<?php

namespace App\Http\Controllers\Office;

use App\Models\Portion;
use App\Models\PortionType;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

class PortionController extends Controller
{
    public function getIndex($id = false){

        if (!$id){
            View::share('title', 'Porsiyon Listesi');
            $data['items'] = PortionType::all();
        }
        View::share('title', 'Porsiyon Listesi');
        $data['id'] = $id;
        $data['portion'] = PortionType::find($id);
        $data['items'] = PortionType::all();


        return view('office.portion.index', $data);
    }

    public function postIndex(){
        $input = Input::all();
        $portion = new PortionType();

        $portion->exists = Input::get('id') === "false" ? false : true;

        if ($portion->exists) {
            $portion->id = Input::get('id');
        }
        $portion->name = $input['title'];
        $portion->description = $input['description'];


        if($portion->save()){
            Session::flash('alert', Config::get('messages.admin.editUserTrue'));
            return redirect()->action('Office\PortionController@getIndex');

        }
            Session::flash('error', Config::get('messages.admin.editUserFalse'));
            return redirect()->action('Office\PortionController@getIndex');

    }

    public function getDetail($id = false){

        if (!$id){
            return redirect()->action('Office\PortionController@getIndex');
        }

        View::share('title', 'Porsiyon Listesi');
        $data['id'] = $id;
        $data['items'] = Portion::where('type_id', $id)->get();
        /*$data['portion'] = Portion::find($id);*/


        return view('office.portion.detail', $data);
    }

    public function postDetail(){
        $input = Input::all();
        $portion = new Portion();

        $portion->exists = Input::get('id') === "false" ? false : true;

        if ($portion->exists) {
            $portion->id = Input::get('id');
        }
        $portion->content = $input['title'];
        $portion->type_id = $input['type_id'];



        if($portion->save()){
            Session::flash('alert', Config::get('messages.admin.editUserTrue'));
            return redirect()->back();

        }
        Session::flash('error', Config::get('messages.admin.editUserFalse'));
        return redirect()->back();

    }

    public function getDeleteCategory($id){

        $category = PortionType::find($id);
        if ($category->delete()){
            $portion = Portion::where('type_id',$category->id)->delete();
        }
        Session::flash('alert', 'Kategori Silindi.');
        return Redirect::back();
    }

    public function getDeleteDetail($id){
        $portion = Portion::find($id);
        if ($portion->delete()){
            Session::flash('warning', 'İçerik Silindi');
            return Redirect::back();
        }
        Session::flash('alert', 'İçerik Silinemedi..');
        return Redirect::back();
    }
}
