<?php

namespace App\Http\Controllers\Office;

use App\Models\Dietlist;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class DietListController extends Controller
{
    public function getIndex(){
        View::share('title','Diyet Listesi Talebi');
        return view('office.nutrition.dietlist');

    }
    public function postIndex()
    {
        View::share('title','Diyet Listesi Talebi');

        if ((int)Input::get('username') === 0){
            Session::flash('error','Listeden Kullanıcı Seçiniz');
            return redirect()->back();
        }
        $user = User::find((int)Input::get('username'));
        $count = (int)Input::get('message') === 0 ? 1: (int)Input::get('message');

        for ($i = 0; $i<$count; $i++){
            $dietlist = new Dietlist();
            $dietlist->user_id = $user->id;
            $dietlist->nutritionist_id = $user->nutritionist_id;
            $dietlist->status = 0;
            $dietlist->date = Carbon::now()->format('Y-m-d H:i:s');
            $dietlist->purpose = 'Yönetici Talebi.';
            $dietlist->sabah = '';
            $dietlist->kahvalti = '';
            $dietlist->ara_ogun1 = '';
            $dietlist->ogle = '';
            $dietlist->ara_ogun2 = '';
            $dietlist->aksam = '';
            $dietlist->ara_ogun3 = '';
            $dietlist->notlar = '';
            $dietlist->save();

        }
        Session::flash('alert','Diyet Listesi Talebi Başarıyla Oluşturuldu');
        return redirect()->back();
    }

    public function postId()
    {
        View::share('title','Diyet Listesi Talebi');

        if ((int)Input::get('number') === 0){
            Session::flash('error','Listeden Kullanıcı Seçiniz');
            return redirect()->back();
        }
        $user = User::find((int)Input::get('number'));
        $count = (int)Input::get('message') === 0 ? 1: (int)Input::get('message');

        for ($i = 0; $i<$count; $i++){
            $dietlist = new Dietlist();
            $dietlist->user_id = $user->id;
            $dietlist->nutritionist_id = $user->nutritionist_id;
            $dietlist->status = 0;
            $dietlist->date = Carbon::now()->format('Y-m-d H:i:s');
            $dietlist->purpose = 'Yönetici Talebi.';
            $dietlist->sabah = '';
            $dietlist->kahvalti = '';
            $dietlist->ara_ogun1 = '';
            $dietlist->ogle = '';
            $dietlist->ara_ogun2 = '';
            $dietlist->aksam = '';
            $dietlist->ara_ogun3 = '';
            $dietlist->notlar = '';
            $dietlist->save();

        }
        Session::flash('alert','Diyet Listesi Talebi Başarıyla Oluşturuldu');
        return redirect()->back();
    }
}
