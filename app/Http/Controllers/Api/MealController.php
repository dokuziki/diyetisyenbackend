<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\Bad;
use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Models\Coop;
use App\Models\DailyActivities;
use App\Models\Meal;
use App\Models\Payments;
use App\Models\Times;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class MealController extends Controller
{

    public function getMeal($id)
    {

        $meal = Meal::find($id);

        if ($meal === null) {
            return NotFound::message(['Kayıt Bulunamadı.'])->send();
        } else {
            return Success::fill(['meal' => $meal])->send();
        }
    }

    public function getList()
    {
        return $this->mustLogin(function ($user) {
            //TODO: 7 gün * öğün sayısı

            $days = [];

            $today = Carbon::now()->format('Y-m-d');
            if (Input::get('today', false)) {
                $today = Carbon::createFromFormat('Y-m-d', Input::get('today'));
                $yesterday = $today->addDays(1)->format('Y-m-d');
                //$monday = date('Y-m-d', strtotime('monday this week', strtotime($yesterday)));
                $monday = date("Y-m-d", strtotime('monday this week', strtotime($yesterday)));
            } else {
                $monday = date("Y-m-d", strtotime(date('o-\\WW')));
            }

            $times = Times::all();

            for ($i = 0; 7 > $i; ++$i) {
                $tempDay = date("Y-m-d", strtotime(date("Y-m-d", strtotime($monday)) . '+' . $i . ' days'));

                foreach ($times as $time) {
                    $days[(string)$tempDay][$time->id] = null;
                }

            }

            $meals = Meal::notDeleted()->where('date', '>=', date("Y-m-d", strtotime($monday)))
                ->with(['likes', 'likes.user', 'favorites', 'favorites.user'])
                ->where('date', '<=', date("Y-m-d", strtotime(date("Y-m-d", strtotime($monday)) . '+ 6 days')))
                ->where('user_id', $user->id)
                ->get();

            foreach ($meals as $meal) {
                $day = date("Y-m-d", strtotime($meal->date));
                $days[(string)$day][$meal->time->id] = $meal->toArray();

            }

            $data = json_decode(json_encode($days), true);

            $temp = [];

            foreach ($data as $key => $d) {
                $tD = [];
                $count = 0;
                foreach ($d as $k => $v) {
                    $tc = $v === null ? 0 : 1;
                    $count += $tc;
                    $tD[] = (Array)['time_id' => $k, 'meal' => ['time' => Times::find($k), 'data' => $v === null ? new \StdClass() : $v, 'isAdded' => $v === null ? false : true]];

                }
                $dailyActivityT = DailyActivities::where('date', $key)->where('user_id', $user->id)->first();
                if ($dailyActivityT === null) {

                    $dailyActivityT = new \StdClass();
                }

                $temp[] = (Array)['date' => $key, 'daily_activity' => $dailyActivityT, 'addedMeals' => $count, 'data' => $tD, 'isAfterToday' => $key > $today ? true : false];
            }


            return Success::fill(['calendar' => $temp, 'monday' => $monday,])->send();

        });

    }

    public function postMeal()
    {
        return $this->mustLogin(function ($user) {

            $emailExtension = explode('@',$this->userAttr('email'))[1];
            $coop = Coop::where('email_extension', $emailExtension)->where('end_date', '>=',Carbon::now()->format('Y-m-d') )->first();

            if($coop === null){
                $trial = false;

                $trial = $user->isTrial;

                if (!$trial && $user->forcep === 0) {
                    //TODO: Bugün içerisinde postlanan meal sayısı
                    $isPostedToday = Meal::where('user_id', $user->id)->where('date', Carbon::now()->format('Y-m-d'))->count();

                    //TODO: Eğer 0'dan büyükse ödeme yapmış olmaı gerekiyor
                    if ($isPostedToday > 0) {

                        //TODO: Son ödeme logu
                        $payment = Payments::where('user_id', $user->id)->where('type', 0)->where('payment_status',1)->orderBy('id', 'DESC')->first();


                        //TODO: Daha önce hiç ödeme yapmamış ya da yapmış ama tarihi geçmiş
                        if ($payment === null || $payment->isEnd) {
                            return Success::fill(['message' => 'Günlük Öğün Yükleme Sınırını Doldurdunuz.', 'meal' => [], 'isLimitExceeded' => true, 'isFirst' => false, 'isTrial' => $trial])->send();
                        }

                    }
                }

            }




            $meal = $this->insertMeal($user);

            if (!$meal) {
                return Bad::fill([])->error(['Öğün Görselini Yüklerken Bir Hata Oluştu.'])
                    ->message(['Öğün Görselini Yüklerken Bir Hata Oluştu'])->send();
            }

            if (!$meal->save()) {
                return Bad::fill([])->error(['Öğün Yüklenirken Bir Hata Oluştu'])
                    ->message(['Öğün Yüklenirken Bir Hata Oluştu'])->send();
            }

            $totalCount = Meal::where('user_id', $user->id)->count();

            if ($totalCount === 1) {
                $isTheFirst = true;
            } else {
                $isTheFirst = false;
            }

            if ($trial) {
                $lastDate = Carbon::createFromTimestamp(strtotime($user->created_at))->addDays(7);
                $remaining = Carbon::now()->diffInDays($lastDate);
            } else {
                $remaining = 0;
            }

            return Success::fill(['meal' => $meal, 'isFirst' => $isTheFirst, 'isTrial' => $trial, 'daysRemaining' => $remaining, 'isLimitExceeded' => false])->send();


        });
    }

    private function insertMeal($user)
    {
        if (!Input::hasFile('photo')) {
            return false;
        }

        $meal = new Meal();
        $meal->user_id = $user->id;

        $name = str_random(15) . '.jpg';
        $filename = 'uploads/meals/' . $name;
        $file = public_path($filename);

        if (!Image::make(Input::file('photo'))->save($file)) {
            return false;
        }
        $meal->pic = $name;
        if (Input::get('date', false)) {
            $meal->date = Carbon::createFromFormat('Y-m-d', Input::get('date'))->format('Y-m-d');
        } else {
            $meal->date = Carbon::now()->format('Y-m-d');
        }
        $meal->time_id = Input::get('time');
        $meal->desc = Input::get('description');

        return $meal;
    }

    public function postDelete()
    {
        return $this->mustLogin(function ($user) {
            $meal = Meal::where('id', Input::get('meal_id'))->where('user_id', $user->id)->first();

            if ($meal !== null) {
                $meal->status = 0;
                if ($meal->save()) {
                    return Success::fill(['meal' => 'Öğün başarıyla silindi.'])->send();
                }
            }
            return Bad::fill()->error([config('messages.error.missedData')])->message(["Kayıt Silinemedi."])->send();


        });

    }

    public function postAddWater()
    {
        return $this->mustLogin(function ($user) {

            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'cup' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();


            $today = Input::get('date', false);
            if ($today) {
                $dailyActivity = DailyActivities::where('date', $today)->where('user_id', $user->id)->first();

                if ($dailyActivity === null) {

                    $dailyActivity = DailyActivities::create(['date' => $today, 'user_id' => $user->id]);
                }

                $dailyActivity->water = (int)$input['cup'];

                if (!isset($dailyActivity->activity)) {
                    $dailyActivity->activity = '';
                }
                if ($dailyActivity->save()) {
                    return Success::fill(['daily_activity' => $dailyActivity])->send();
                }
            } else {
                return Bad::fill()->error([config('messages.error.missedData')])->message(["Yanlış gün bilgisi."])->send();
            }


        });
    }

    public function postAddActivity()
    {
        return $this->mustLogin(function ($user) {
            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'activity' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();

            $today = Input::get('date', false);

            if ($today) {

                $dailyActivity = DailyActivities::where('date', $today)->where('user_id', $user->id)->first();

                if ($dailyActivity === null) {

                    $dailyActivity = DailyActivities::create(['date' => $today, 'user_id' => $user->id]);
                }

                $dailyActivity->activity = $input['activity'];

                if ($dailyActivity->save()) {
                    return Success::fill(['daily_activity' => $dailyActivity])->send();
                }
            } else {

                return Bad::fill()->error([config('messages.error.missedData')])->message(["Yanlış gün bilgisi."])->send();
            }

        });
    }
}
