<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\Bad;
use App\Http\Responses\Fail;
use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Models\Comment;
use App\Http\Controllers\Controller;
use App\Models\Meal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class CommentController extends Controller
{
    public function getIndex() {
        return 'ok';
    }

    public function getByMeal(){

        return $this->mustLogin(function ($user) {

            $meal_id = Input::get('id');
            $meal = Meal::find($meal_id);

            if ($user->id == null || $user->id != $meal->user_id) {
                return "Yorumları Görme Yetkiniz Bulunmamaktadır.";
            }
            else{
                $comment = Comment::where('meal_id', $meal_id)
                /*    ->where('user_id',$user->id)*/
                    ->with(['user'])
                    ->orderBy('id','ASC')
                    ->get();

                if($comment === null){
                    return NotFound::message(['Yorum Bulunamadı'])->send();
                }else{
                    return Success::fill(['comment' => $comment])->send();
                }
            }


        });
    }

    public function postByMeals() {

        return $this->mustLogin(function($user) {

            $meal_id = Input::get('meal_id');
            $meal = Meal::find($meal_id);
            if ($meal->user_id !== $user->id){
                return Bad::fill()->error('Bu öğüne yorum yapamazsınız.')
                    ->message(['Bu öğüne yorum yapamazsınız.'])->send();
            }
            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'meal_id' => 'required',
                'comment' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();

            $comment = Comment::create([
                'user_id' => $user->id,
                'meal_id' => $meal_id,
                'comment' => $input['comment'],
            ]);

            if ($comment){
                $meal->chat = 1;
                $meal->save();
                return Success::fill(['comments' => $comment, 'user' => $comment->user])->send();
            }
            else
                return Fail::fill()->error('Yorum sırasında bir hata oluştu.')
                    ->message(['Yorum sırasında bir hata oluştu.'])->send();



        });
    }
}