<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\Success;
use App\Library\Push\PushHelper;
use App\Models\Favorites;
use App\Models\Meal;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class FavoriteController extends Controller
{
    public $page_count = 10;

    public function postFav(){
        return $this->mustLogin(function ($user){

            $meal_id = Input::get('id');
            $favCheck = Favorites::where('user_id',$user->id)->where('meal_id',$meal_id)->first();

            $message = '';
            if($favCheck === null){
                $meal = Meal::find($meal_id);
                $liked = $meal->user->id;

                $Favorites = new Favorites();
                $Favorites->user_id = $user->id;
                $Favorites->meal_id = $meal_id;
                $Favorites->timestamps = false;
                $Favorites->save();
                $message = 'faved';

                if ($meal->user_id !== $user->id){
                    PushHelper::sendFavorite($user->id,$liked,$meal_id);
                }
            }else{
                $favCheck->delete();
                $message = 'unfaved';
            }

            return Success::fill(['status' => $message])->send();

        });
    }


    public function getList(){
        return $this->mustLogin(function ($user){
            $page =  Input::get('page',0);
            $favorites = Favorites::where('user_id',$user->id)->select(['meal_id'])->orderBy('id', 'desc')

                ->with(['meal','meal.user','meal.time', 'meal.user.nutritionist','meal.likes.user'])
                ->skip($this->page_count * $page)
                ->take($this->page_count)
                ->get();

            return Success::fill(['favorites' => $favorites])->send();
        });
    }
}
