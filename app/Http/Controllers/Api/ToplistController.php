<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\NotFound;
use App\Models\Meal;
use App\Http\Responses\Success;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;

class ToplistController extends Controller
{
    protected $week;
    protected $point_count = 20;
    protected $start_date = "2016-09-14";
    protected $end_date = "2016-09-21";

    public function getIndex()
    {

        if (!Cache::has('meal_toplist')) {
            $result = Cache::remember('meal_toplist', 60, function () {
                $meals = Meal::whereBetween('date', [$this->start_date, $this->end_date])
                    ->with(['user', 'user.nutritionist'])
                    ->get();

                $user = [];
                foreach ($meals as $meal) {
                    if (!isset($user[$meal->user->id])) {
                        $user[$meal->user->id] = $meal->user->toArray();
                        $user[$meal->user->id]['point'] = 0;
                    }

                    $user[$meal->user->id]['point'] += $meal->point;
                    $user[$meal->user->id]['meals'][] = $meal->toArray();
                }

                usort($user, [self::class, 'sortByPoint']);


                $user = array_slice($user, 0, $this->point_count);
                return $user;
            });

        } else {
            $result = Cache::get('meal_toplist');
        }

        if ($result === null) {
            return NotFound::message(['Kayıt Bulunamadı'])->send();
        } else {
            return Success::fill(['toplist' => $result])->send();
        }

    }

    public function sortByPoint($a, $b)
    {
        return $b['point'] - $a['point'];
    }
}
