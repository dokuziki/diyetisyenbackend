<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Models\Notification;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class NotificationController extends Controller
{
    protected $page_count = 30;


    public function getIndex()
    {

        return $this->mustLogin(function ($user) {

            $page = Input::get('page') === null ? 0 : Input::get('page');

            $notification = Notification::where(function ($query) use ($user) {
                $query->where('user_id', $user->id)
                    ->orWhere('is_global', 1);
            })
                ->select('id', 'message', 'sender_id', 'user_id', 'content_id', 'content_type', 'created_at')
                ->with(['user', 'meal'])->order()
                ->take($this->page_count)->skip($this->page_count * $page)->get();


            return Success::fill(['notifications' => $notification])->send();

        });

    }
}
