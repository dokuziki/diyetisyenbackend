<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\Success;
use App\Models\Dietlist;
use App\Models\Likes;
use App\Models\Meal;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ListController extends Controller
{
    public function getIndex(){
        return $this->mustLogin(function ($user){
            $dietlist = Dietlist::where('user_id', $user->id)->orderBy('date','desc')->get();
            return Success::fill(['diet-list' => $dietlist])->send();
        });
    }
}
