<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\Bad;
use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Library\Email\EmailWorker;
use App\Models\Dietlist;
use App\Models\Payments;
use App\Models\User;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class PaymentController extends Controller
{
    public function getPayment()
    {

        return $this->mustLogin(function ($user) {

            $payment_type = Input::get('type');
            $payment = Payments::where('user_id', $user->id)->where('type', $payment_type)
                ->order()->first();

            if ($payment === null) {
                return NotFound::message(['Kayıt Bulunamadı.'])->send();
            } else {
                return Success::fill(['payment' => $payment->toArray()])->send();
            }
        });
    }

    public function postPay()
    {

        return $this->mustLogin(function ($user) {

            try {
                $input = Input::all();

                $payment = new Payments();

                //is forced payment
                $torpil = Payments::where('user_id', $user->id)->where('is_forced', 1)->where('end_date', '>', Carbon::now());
                if ($torpil->count() > 0) {
                    return Success::fill(['payment' => $torpil->first()])->send();
                }

                $oldPayment = Payments::where('user_id', $user->id)->where('end_date', '>', Carbon::now())->where('type', 0)->first();
                if ($oldPayment !== null && $oldPayment->payment_status === 1) {
                    return Success::fill(['payment' => $oldPayment])->send();
                }

                $nutritionist = User::where('id', $user->nutritionist_id)->first();

                $payment->user_id = $user->id;
                $payment->payment_status = $input['status'];
                $payment->start_date = Carbon::now();
                $payment->type = $input['type'];

                if (intval($input['type']) == 0) {
                    $payment->end_date = Carbon::now()->addDays(7);
                } elseif (intval($input['type']) == 1) {
                    $payment->end_date = Carbon::now()->addWeek(1);

                    if (intval($input['status']) === 1) {
                        $dietlist = new Dietlist();
                        $dietlist->user_id = $user->id;
                        $dietlist->nutritionist_id = $user->nutritionist_id;
                        $dietlist->status = 0;
                        $dietlist->date = Carbon::now()->format('Y-m-d H:i:s');
                        $dietlist->purpose = Input::get('purpose');
                        $dietlist->sabah = '';
                        $dietlist->kahvalti = '';
                        $dietlist->ara_ogun1 = '';
                        $dietlist->ogle = '';
                        $dietlist->ara_ogun2 = '';
                        $dietlist->aksam = '';
                        $dietlist->ara_ogun3 = '';
                        $dietlist->notlar = '';


                        $dietlist->save();


                        $data = [
                            'body' => $user->first_name . ' ' . $user->last_name . ' Diyet Listesi Satın Aldı.',
                            'from' => $user->email
                        ];
                        Mail::send('emails.change', $data, function ($m) use ($nutritionist) {
                            $m->from('info@ceptediyetisyen.com');
                            $m->to($nutritionist->email)->subject("Diyet Listesi Bekleniyor.");

                        });


                    }
                }

                if ($payment->save()) {

                    if ($oldPayment !== null && (int)$input['status'] === 1) {
                        $oldPayment->end_date = $payment->end_date;
                        $oldPayment->save();
                    }

                    return Success::fill(['payment' => $payment])->send();
                }

            }catch(\Exception $e){

                $data = [
                    'body' => $user->id . ' ID Numaralı' . $user->first_name . ' ' . $user->last_name . 'Ödeme Yapamadı' . '\n' .
                        'Hata Çıktısı : ' . $e->getMessage() . '\n' . $e->getCode() . '\n' . $e->getLine() . '\n' .
                        'Gönderilen Veriler : ' . 'Gelen Status ' . $input['status'] . 'Gelen Type : ' . $input['type'] . '\n' .
                        'Kullanıcı Auth : ' . $input['auth'],
                    'from' => $user->email
                ];
                Mail::send('emails.change', $data, function ($m) {
                    $m->from('info@ceptediyetisyen.com');
                    $m->to('cosku.demirhan@ward.agency')->subject("Ödeme Hatası");
                    $m->cc('okan.yuksel@ward.agency');

                });

            }

            return Bad::fill()->error('Ödeme Sırasında Bir Sorun Oluştu')
                ->message(['Ödeme Sırasında Bir Sorun Oluştu'])->send();
        });

}




    public function postPay2()
    {

        return $this->mustLogin(function ($user) {



            try {
                $input = Input::all();

                $control = null;
                if($input['receipt'] != 'valid' && $input['receipt'] != 'expired' && $input['receipt'] != 'never' && $input['receipt'] != 'failed'){
                    $control = Payments::where('receipt', $input['receipt'])->first();
                }

                if($control != null){

                    return Bad::fill()->error('odeme_onay')
                        ->message(['Bu ürün daha önce alınmış.'])->send();
                }

                $payment = new Payments();
                /*
                //is forced payment
                $torpil = Payments::where('user_id', $user->id)->where('is_forced', 1)->where('end_date', '>', Carbon::now());
                if ($torpil->count() > 0) {
                    return Success::fill(['payment' => $torpil->first()])->send();
                }
                */
                $oldPayment =  null;

                if(intval($input['type']) == 1){
                    $oldPayment = Payments::where('user_id', $user->id)->where('end_date', '>', Carbon::now())->where('type', 0)->orderBy('id','DESC')->first();
                }
                /*
                 if ($oldPayment !== null && $oldPayment->payment_status === 1) {
                    return Success::fill(['payment' => $oldPayment])->send();
                }
                */



                $nutritionist = User::where('id', $user->nutritionist_id)->first();

                $payment->user_id = $user->id;
                $payment->payment_status = $input['status'];
                $payment->start_date = Carbon::now();
                $payment->type = $input['type'];
                $payment->receipt = $input['receipt'];

                if (intval($input['type']) == 0) {
                    $payment->end_date = Carbon::now()->addDays(7);
                } elseif (intval($input['type']) == 1) {
                    $payment->end_date = Carbon::now()->addWeek(1);

                    if (intval($input['status']) === 1) {
                        $dietlist = new Dietlist();
                        $dietlist->user_id = $user->id;
                        $dietlist->nutritionist_id = $user->nutritionist_id;
                        $dietlist->status = 0;
                        $dietlist->date = Carbon::now()->format('Y-m-d H:i:s');
                        $dietlist->purpose = Input::get('purpose');
                        $dietlist->sabah = '';
                        $dietlist->kahvalti = '';
                        $dietlist->ara_ogun1 = '';
                        $dietlist->ogle = '';
                        $dietlist->ara_ogun2 = '';
                        $dietlist->aksam = '';
                        $dietlist->ara_ogun3 = '';
                        $dietlist->notlar = '';


                    }
                }

                if ($payment->save()) {

                    if(intval($input['type']) == 1 && isset($dietlist)){
                         $dietlist->save();

                            $data = [
                                'body' => $user->first_name . ' ' . $user->last_name . ' Diyet Listesi Satın Aldı.',
                                'from' => $user->email
                            ];




                            Mail::send('emails.change', $data, function ($m) use ($nutritionist) {
                                $m->from('info@ceptediyetisyen.com');
                                $m->to($nutritionist->email)->cc('cosku@dokuziki.com')->subject("Diyet Listesi Bekleniyor.");

                            });



                    }


                    if ($oldPayment !== null && (int)$input['status'] === 1) {
                        $oldPayment->end_date = $payment->end_date;
                        $oldPayment->save();
                    }

                    return Success::fill(['payment' => $payment])->send();
                }

            }catch(\Exception $e){

                $data = [
                    'body' => $user->id . ' ID Numaralı' . $user->first_name . ' ' . $user->last_name . 'Ödeme Yapamadı' . '\n' .
                        'Hata Çıktısı : ' . $e->getMessage() . '\n' . $e->getCode() . '\n' . $e->getLine() . '\n' .
                        'Gönderilen Veriler : ' . 'Gelen Status ' . $input['status'] . 'Gelen Type : ' . $input['type'] . '\n' .
                        'Kullanıcı Auth : ' . $input['auth'],
                    'from' => $user->email
                ];
                Mail::send('emails.change', $data, function ($m) {
                    $m->from('info@ceptediyetisyen.com');
                    $m->to('cosku.demirhan@ward.agency')->subject("Ödeme Hatası");

                });

                return Bad::fill()->error('Ödeme Sırasında Bir Sorun Oluştu')
                    ->message(['Ödeme Sırasında Bir Sorun Oluştu'])->send();
            }

            return Bad::fill()->error('Ödeme Sırasında Bir Sorun Oluştu')
                ->message(['Ödeme Sırasında Bir Sorun Oluştu'])->send();
        });

    }

}
