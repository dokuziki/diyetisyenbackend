<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Models\Dietlist;
use App\Models\Portion;
use App\Models\PortionType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class DietlistController extends Controller
{
    public function getList(){
        return $this->mustLogin(function ($user){
            $dietlistC = Dietlist::where('user_id', $user->id)->where('status',1)->orderBy('date','desc')->get();
            $dietlistI = Dietlist::where('user_id', $user->id)->where('status',0)->orderBy('date','desc')->get();

            $nutritionist = $user->nutritionist()->first();

            return Success::fill(['diet-list' => $dietlistC,'in-progress' => $dietlistI, 'nutritionist' => $nutritionist])->send();

        });
    }

    public function getPortion(){
        return $this->mustLogin(function($user){
            $portion = PortionType::with(['portion'])->get();
            return Success::fill(['portionlist' => $portion])->send();
        });
    }

    public function getDetail(){
        return $this->mustLogin(function ($user){
            $input = Input::get('id');
            $detail = Dietlist::find($input);
            if ($detail === null) {
                return NotFound::message(['Kayıt Bulunamadı.'])->send();
            } else {
                $nutritionist = $user->nutritionist()->first();

                return Success::fill(['details' => $detail, 'nutritionist' => $nutritionist])->send();
            }
        });
    }
}
