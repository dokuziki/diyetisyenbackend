<?php

/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 09/06/16
 * Time: 15:01
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\Bad;
use App\Http\Responses\Fail;
use App\Http\Responses\Success;
use App\Library\Push\PushHelper;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class DeviceController extends Controller
{

    public function postRegister(){

        $validation = $this->inputValidation
            (
                [
                    'language' => 'required',
                    'type' => 'required',
                    'app_version' => 'required'
                ]
            );

        if($validation->fails()){
            return Bad::fill()->error(config('messages.error.missedData'))->message($validation->errors()->all())->send();
        }

        $token = [
            'language' => Input::get('language'),
            'type' => Input::get('type'),
            'app_version' => Input::get('app_version')
        ];

        $response['token'] = JWT::encode($token, env('HASH_KEY'));


        return Success::fill($response)->send();
    }

    public function getInfo(){
        $response['data'] = JWT::decode(Input::get('token'), env('HASH_KEY'), ['HS256']);

        return Success::fill($response)->send();
    }

    public function postUpdatePush()
    {
        $input = Input::all();

        $messages = [
            'required' => Config::get('messages.error.required')
        ];

        $cases = [
            'registrationid' => 'required',
            'token' => 'required'
        ];

        $validator = Validator::make($input, $cases, $messages);

        $GLOBALS['client'] = JWT::decode(Input::get('token'), env('HASH_KEY'),['HS256']);


        if ($validator->fails())
            return Bad::fill()->error(config('messages.error.missedData'))->message($validator->errors()->all())->send();

        $result = PushHelper::addPlayer($input['registrationid']);

        if($result){
            return Success::fill(['push' => ['id' => $input['registrationid'], 'oneginal'=> $result]])->send();
        }else{
            return Fail::fill()->error('registrationid keydedilemedi')->message('registrationid keydedilemedi')->send();
        }

    }
}