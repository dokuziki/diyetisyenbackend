<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 29/06/16
 * Time: 13:24
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Responses\Bad;
use App\Http\Responses\Fail;
use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Library\Push\PushHelper;
use App\Models\BlockUsers;
use App\Models\ChangeNutritionist;
use App\Models\Meal;
use App\Models\Nutrition;
use App\Models\NutritionCode;
use App\Models\NutritionistDetail;
use App\Models\NutritionComments;
use App\Models\Spam;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\Weight;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Laravel\Socialite\Facades\Socialite;

class UserController extends Controller
{

    public $socialite;

    public function __construct(Socialite $socialite)
    {
        $this->socialite = $socialite::driver('social');
    }

    public function postUpdatePush()
    {
        return $this->mustLogin(function ($user) {
            $input = Input::all();

            $messages = [
                'required' => Config::get('messages.error.required')
            ];

            $cases = [
                'registrationid' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);


            if ($validator->fails())
                return Bad::fill()->error(config('messages.error.missedData'))->message($validator->errors()->all())->send();

            $result = PushHelper::addPlayer($input['registrationid'], $user->id);
            $auth = \App\Models\Auth::where('token', Input::get('auth'))->update([
                'onesignal' => $result,
                'device' =>  $GLOBALS['client']->type === 'android' ? 1:0
            ]);

            if ($result && $auth) {
                return Success::fill(['push' => ['id' => $input['registrationid'], 'oneginal' => $result]])->send();
            } else {
                return Fail::fill()->error('registrationid keydedilemedi')->message('registrationid keydedilemedi')->send();
            }
        });

    }

    public function postFacebookCheck()
    {
        $fbUser = $this->facebookUser(Input::get('accessToken'));

        if (!$fbUser) {
            return Bad::fill()->error([config('messages.error.invalidFBToken')])->message([config('messages.error.invalidFBToken')])->send();
        }

        if ($fbUser->email === null || $fbUser->email === "") {
            return Bad::fill()->error([config('messages.error.emptyEmail')])->message([config('messages.error.fbEmptyEmail')])->send();
        }

        $userCheck = User::where('email', $fbUser->email)->first();

        if ($userCheck === null) {
            return Success::fill(['user_facebook' => $fbUser->user])->send();
        } else {
            Auth::login($userCheck, true);
            return Success::fill(['user' => $this->transformUser($userCheck), 'auth' => $userCheck->getKeyForAuth(), 'isPremium' => $userCheck['isPremium'], 'isPrivate' => $userCheck['isPrivate'], 'meal_count' => $userCheck->getMealCount()])->send();


        }
    }

    private function facebookUser($accessToken)
    {
        try {
            return $this->socialite->userFromToken($accessToken);
        } catch (ClientException $err) {
            return false;
        }
    }

    public function
    transformUser($user)
    {

        if ($user->gender == null)
            $user->gender = 2;

        if ($user->pic == null)
            $user->pic = "";

        if ($user->fb_id == null)
            $user->fb_id = "";


        return [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'birthday' => $user->birthday == null ? "" : $user->birthday,
            'nickname' => $user->nickname,
            'gender' => $user->gender,
            'pic' => $user->pic,
            'phone' => $user->phone,
            'fb_id' => $user->fb_id,
            'location' => $user->location,
            'bio' => $user->bio,
            'isPremium' => $user->isPremium,
            'isTrial' => $user->isTrial,
            'trialRemaining' => $user->remaining,
            'meal_count' => $user->meal_count,
            'nutritionist' => $user->nutritionist()->first(),
            'trial_expire_date' => $user->trial_expire_date()
        ];
    }

    public function postFacebook()
    {
        $fbUser = $this->facebookUser(Input::get('accessToken'));

        if (!$fbUser) {
            return Bad::fill()->error([config('messages.error.invalidFBToken')])->message([config('messages.error.invalidFBToken')])->send();
        }

        if ($fbUser->email === null || $fbUser->email == "") {
            return Bad::fill()->error([config('messages.error.emptyEmail')])->message([config('messages.error.fbEmptyEmail')])->send();
        }

        $userCheck = User::where('email', $fbUser->email)->first();

        if ($userCheck === null) {


            $input = Input::except(['photo']);

            $messages = [
                'required' => Config::get('messages.error.required'),
                'unique' => Config::get('messages.error.unique'),
                'email' => Config::get('messages.error.invalidEmail'),
                'min' => Config::get('messages.error.min6Pass')
            ];

            $cases = [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'nickname' => 'required|unique:users',
                'weight' => 'required',
                'height' => 'required',
                'birthday' => 'required',
                'gender' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);


            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();


            $user = new User;
            $user->first_name = $fbUser->user['first_name'];
            $user->last_name = $fbUser->user['last_name'];
            $user->email = $fbUser->email;
            $user->fb_id = $fbUser->id;
            $user->fb_token = $fbUser->token;

            $user->birthday = isset($fbUser->user['birthday']) ? date("d-m-Y", strtotime($fbUser->user['birthday'])) : null;

            if ($fbUser->user['gender'] == 'male')
                $user->gender = 0;
            else if ($fbUser->user['gender'] == 'female')
                $user->gender = 1;
            else
                $user->gender = 2;


            $user->nickname = Input::get('nickname');

            $path = str_replace('normal', 'large', $fbUser->avatar);
            $randName = str_random(15) . '.jpg';
            $filename = 'uploads/' . $randName;
            $file = public_path($filename);

            $user->pic = $randName;
            Image::make($path)->resize(300, 300)->save($file);
            $user->save();


            UserDetail::create([
                'user_id' => $user->id,
                'height' => Input::get('height')
            ]);


            Weight::create([
                'weight' => Input::get('weight'),
                'user_id' => $user->id
            ]);


            $nutcode = NutritionCode::where('used', 0)->where('code', Input::get('code'))->first();
            if ($nutcode === null) {
                $user->save();
                $this->addNutritionistId($user);

            } else {

                $user->nutritionist_id = $nutcode->nutritionist_id;
                $user->save();
                $nutcode->used = 1;
                $nutcode->user_id = $user->id;
                $nutcode->save();
            }

            if ($user) {

                //TODO: usera token bilgisi eklenecek
                Auth::login($user, true);
                /*                $this->updateClient($user->getKeyForAuth());*/
                return Success::fill(['user' => $this->transformUser($user), 'auth' => $user->getKeyForAuth(), 'isPremium' => $user['isPremium'], 'isPrivate' => $user['isPrivate'],'meal_count' => 0])->send();
            }
        } else {
            Auth::login($userCheck, true);
            /*            $this->updateClient($userCheck->getKeyForAuth());*/
            return Success::fill(['user' => $this->transformUser($userCheck), 'auth' => $userCheck->getKeyForAuth(), 'meal_count' => $userCheck->getMealCount()])->send();
        }
    }

    private function addNutritionistId($registered)
    {
        $lastUser = User::orderBy('id', 'desc')->whereNotNull('nutritionist_id')->where('id', '<', $registered->id)->first();

        $lastN = User::orderBy('id', 'asc')->where('status', 1)->where('id', '>', $lastUser->nutritionist_id)->first();

        if ($lastN === null) {
            $firstN = User::orderBy('id', 'asc')->where('status', 1)->first();
            $registered->nutritionist_id = $firstN->id;
        } else {
            $registered->nutritionist_id = $lastN->id;
        }

        if ($registered->save()) {
            return true;
        } else {
            return false;
        }

    }

    public function postLogin()
    {
        $input = Input::all();

        $messages = [
            'required' => Config::get('messages.error.required'),
            'email' => Config::get('messages.error.invalidEmail'),
            'min' => Config::get('messages.error.min6Pass')
        ];

        $cases = [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ];

        $validator = Validator::make($input, $cases, $messages);


        if ($validator->fails())
            return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();

        $user = User::where('email', $input['email'])->first();
        if ($user === null) {
            return Fail::fill()->error([config('messages.error.userLogin')])->message([config('messages.error.userLogin')])->send();
        }

        if (Hash::check($input['password'], $user->password)) {
            Auth::login($user);
            /*            $this->updateClient(Auth::user()->getKeyForAuth());*/
            return Success::fill(['user' => $this->transformUser(Auth::user()), 'auth' => $user->getKeyForAuth(), 'isPremium' => $user['isPremium'], 'isPrivate' => $user['isPrivate'], 'meal_count' => $user->getMealCount()])->send();
        } else
            return Fail::fill()->error([config('messages.error.userLogin')])->message([config('messages.error.userLogin')])->send();

    }

    public function postRegisterCheck()
    {
        $input = Input::all();

        $messages = [
            'required' => Config::get('messages.error.required'),
            'unique' => Config::get('messages.error.unique'),
            'email' => Config::get('messages.error.invalidEmail'),
            'min' => Config::get('messages.error.min6Pass')
        ];

        $cases = [
            'email' => 'required|email|max:255|unique:users',
            'nickname' => 'required|max:255|unique:users',
            'password' => 'required|min:6',
        ];

        $validator = Validator::make($input, $cases, $messages);


        if ($validator->fails()) {
            return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();
        } else {
            return Success::fill([])->send();
        }
    }

    /*public function updateClient(){

    }*/

    public function postRegister()
    {
        $input = Input::except(['photo']);

        $messages = [
            'required' => Config::get('messages.error.required'),
            'unique' => Config::get('messages.error.unique'),
            'email' => Config::get('messages.error.invalidEmail'),
            'min' => Config::get('messages.error.min6Pass')
        ];

        $cases = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'nickname' => 'required|unique:users',
            'email' => 'required|unique:users|email',
            'weight' => 'required',
            'height' => 'required',
            'birthday' => 'required',
            'gender' => 'required'
        ];

        $validator = Validator::make($input, $cases, $messages);


        if ($validator->fails())
            return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();


        $name = '';


        if (Input::hasFile('photo')) {

            $name = str_random(15) . '.jpg';
            $filename = 'uploads/' . $name;
            $file = public_path($filename);

            Image::make(Input::file('photo'))->resize(300, 300)->save($file);
        }


        $user = User::create([
            'first_name' => trim($input['first_name']),
            'last_name' => trim($input['last_name']),
            'email' => trim($input['email']),
            'password' =>  Hash::make($input['password']),
            'pic' => $name,
            'birthday' => Input::get('birthday'),
            'gender' => Input::get('gender', 2),
            'nickname' => Input::get('nickname')
        ]);

        UserDetail::create([
            'user_id' => $user->id,
            'height' => Input::get('height'),
            'target_weight' => Input::get('weight')
        ]);

        Weight::create([
            'weight' => Input::get('weight'),
            'user_id' => $user->id
        ]);


        $nutcode = NutritionCode::where('used', 0)->where('code', Input::get('code'))->first();
        if ($nutcode === null) {
            $this->addNutritionistId($user);
        } else {
            $user->nutritionist_id = $nutcode->nutritionist_id;
            $user->save();

            $nutcode->used = 1;
            $nutcode->user_id = $user->id;
            $nutcode->save();
        }

        if ($user) {
            Auth::login($user, true);

            /*            $this->updateClient($user->getKeyForAuth());*/
            return Success::fill(['user' => $this->transformUser($user), 'auth' => $user->getKeyForAuth(), 'meal_count' => 0])->send();
        } else
            return Fail::fill()->error([config('messages.error.userCreate')])->message([config('messages.error.userCreate')])->send();

    }

    public function getProfile()
    {

        return $this->mustLogin(function ($auth) {
            $user = new \StdClass();

            $user->first_name = $auth->first_name;
            $user->last_name = $auth->last_name;
            $user->email = $auth->email;
            $user->birthday = $auth->birthday === null ? '' : $auth->birthday;
            $user->gender = $auth->gender;
            $user->nickname = $auth->nickname;
            $user->location = $auth->location;
            $user->bio = $auth->bio;
            $user->pic = $auth->pic;
            $user->phone = $auth->phone;
            $user->isPremium = $auth->isPremium;
            $user->trial_expire_date = $auth->trial_expire_date();

            $user->meal_count = $auth->meal()->count();

            $point = 0;
            $pointFull = 0;

            foreach ($auth->meal()->order()->active()->get() as $meal) {
                if ($meal->point === 10) {
                    ++$pointFull;
                }

                $point += $meal->point;
            }

            $user->total_point = $point;
            $user->full_point = $pointFull;

            $user->weight_logs = array_reverse(Weight::where('user_id', $auth->id)->orderBy('id', 'DESC')->take(10)->get()->toArray());

            $user->details = UserDetail::where('user_id', $auth->id)->first();

            $user->nutritionist = Nutrition::find($auth->nutritionist_id);


            if ($auth->fb_id === null || $auth->fb_id === '') {
                $user->has_password = 1;
            } else {
                $user->has_password = 0;
            }

            return Success::fill(['profile' => $user])->send();
        });
    }

    public function postUpdateWeights()
    {

        return $this->mustLogin(function ($user) {

            $input = Input::all();

            $messages = [
                'required' => Config::get('messages.error.required')
            ];

            $cases = [
                'weight' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);


            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();


            $weight = new Weight();

            $weight->user_id = $user->id;
            $weight->weight = $input['weight'];
            $weight->save();

/*
            Weight::create([
                'weight' => $input['weight'],
                'user_id' => $user->id
            ]);*/


            if ($weight) {

                return Success::fill(['weight' => $weight])->send();
            } else

                return Fail::fill()->error([config('messages.error.userUpdate')])
                    ->message([config('messages.error.userUpdate')])->send();


        });

    }

    public function postUpdate()
    {
        $input = Input::all();

        $messages = [
            'required' => Config::get('messages.error.required'),
            'unique' => Config::get('messages.error.unique'),
            'email' => Config::get('messages.error.invalidEmail'),
            'min' => Config::get('messages.error.min6Pass')
        ];

        $cases = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'nickname' => 'required|max:255',
            'email' => 'required|email|max:255',
            'gender' => 'required',
            'phone' => 'required'
        ];

        $validator = Validator::make($input, $cases, $messages);


        if ($validator->fails())
            return Bad::fill()->error([config('messages.error.missedData')])->message([config('messages.error.missedData')])->send();

        $create = [
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'email' => $input['email'],
            'gender' => $input['gender'],
            'nickname' => $input['nickname'],
            'bio' => $input['bio'],
            'location' => $input['location'],
            'phone' => $input['phone']
        ];


        $user = Auth::user();

        $user->update($create);


        if ($user) {
            return Success::fill(['user' => $this->transformUser($user)])->send();
        } else
            return Fail::fill()->error([config('messages.error.userUpdate')])->message([config('messages.error.userUpdate')])->send();

    }

    public function postPasswordUpdate()
    {
        return $this->mustLogin(function ($user) {
            $pass = Input::get(Input::get('password'));
            $updateUser = $user;
            $updateUser->password =  Hash::make($pass);

            if ($updateUser->save()) {
                return Success::fill(['user' => $this->transformUser($user)])->send();
            } else {
                return Fail::fill()->error(["Kullanıcı şifresi güncellemesi başarısız"])->message([config("Email adresi bulunamadı")])->send();
            }
        });
    }

    public function postLostPassword()
    {
        $user = User::where('email', Input::get('email'))->first();

        if ($user === null) {
            return NotFound::message(['Kayıt Bulunamadı.'])->send();
        } else {

            if ($user->fb_id !== '' && $user->fb_id !== null) {
                return Fail::fill()->error(["Facebook Kullanıcısı"])->message(["Facebook"])->send();
            }

            $pass = str_random(6);
            $updateUser = $user;
            $updateUser->password =  Hash::make($pass);

            if ($updateUser->save()) {
                Mail::send('emails.email', ['password' => $pass, 'user' => $user], function ($message) use ($user) {
                    $message->from('info@ceptediyetisyen.com');
                    $message->to($user->email)->subject("Şifre Değişiklik Talebi");
                });
                return Success::fill(['user' => $this->transformUser($user)])->send();

            } else {
                return Fail::fill()->error(["Kullanıcı şifresi güncellemesi başarısız"])->message([config("Email adresi bulunamadı")])->send();
            }

        }
    }

    public function postNutritionCode()
    {
        return $this->mustLogin(function ($user) {

            if (Input::get('nutrition_code') !== null && Input::get('nutrition_code') !== '') {
                $code = NutritionCode::where('code', Input::get('nutrition_code'))->first();

                if ($code !== null && $code->used !== 1) {
                    $user->nutritionist_id = $code->nutritionist_id;
                    $code->used = 1;
                    $code->user_id = $user->id;
                    $code->save();
                    $user->save();
                } else {
                    return Bad::fill()->message(['Diyetisyen Kodu Hatalıdır'])->send();
                }
                return Success::fill(['user' => $this->transformUser($user)])->send();
            }

        });
    }

    public function getLogout()
    {
        $user = Auth::user();

        if ($user) {
            $authEntity = \App\Models\Auth::where('token', Input::get('auth'))->first();
            $authEntity->delete();

            return Success::fill(['message' => 'Kullanıcı çıkışı başarılı'])->send();
        } else {

            return Success::fill(['message' => 'Kullanıcı login değil'])->send();
        }
    }

    public function getUserProfile($id = false)
    {
        $id = Input::get('id');

        if (Auth::user() === null || Auth::user()->id != $id) {
            $page = (int)Input::get('page',1) - 1;

            $detailO = User::where('id', $id)
                ->select('id', 'first_name', 'last_name', 'nickname','pic', 'nutritionist_id')
                ->with([ 'meal' => function ($q) use($page){
                    $q->order()->with('time')->select(['id', 'user_id', 'desc', 'pic', 'point', 'time_id', 'created_at'])->take(12)->skip($page*12);
                }, 'nutritionist','meal.comments'])->first();

            if($detailO === null){
                $detail = null;
            }else{
                $detail = $detailO->toArray();

                foreach ($detail['meal'] as &$meal) {
                    $meal['comments'] = [];
                }
                $detail['trial_expire_date'] = $detailO->trial_expire_date();

            }


        } else {
            return 'ok';
        }

        if ($detail === null) {
            return NotFound::message(['Kayıt Bulunamadı'])->send();
        } else {
            return Success::fill(['user' => $detail])->send();
        }

    }

    public function getDetail()
    {

        return $this->mustLogin(function ($user) {
            $detail = User::where('id', $user->id)->with('nutritionist', 'weightLogs')
                ->select('id', 'first_name', 'last_name', 'pic', 'nickname', 'email', 'gender', 'status', 'nutritionist_id')
                ->first();


            return Success::fill(['user' => $detail])->send();

        });
    }

    public function postDetails()
    {

        return $this->mustLogin(function ($user) {
            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'birthday' => 'required',
                'weight' => 'required',
                'gender' => 'required',
                'height' => 'required',
                'q1' => 'required',
                'q2' => 'required',
                'q3' => 'required',
                'q4' => 'required',
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message(["Size daha iyi yardımcı olabilmemiz için lütfen sağlık sorularına cevap yazmayı unutmayın"])->send();

            $data = [
                'height' => $input['height'],
                'weight' =>  $input['weight'],
                'q1' => $input['q1'],
                'q2' => $input['q2'],
                'q3' => $input['q3'],
                'q4' => $input['q4']

            ];

            $userData = [
                'birthday' => $input['birthday'],
                'gender' => $input['gender'],
            ];

            $weightData = [
                'weight' => $input['weight'],
            ];


            $userget = User::where('id', $user->id)->first();
            $details = UserDetail::where('user_id', $user->id)->first();
            $weights = Weight::where('user_id', $user->id)->first();


            if ($details->update($data) && $userget->update($userData) && $weights->update($weightData)) {
                return Success::fill(['detail' => $details, 'user' => $userget, 'weight' => $weights])->send();
            } else
                return Fail::fill()->error([config('messages.error.userUpdate')])->message([config('messages.error.userUpdate')])->send();

        });

    }

    public function postNutritionistComment()
    {
        return $this->mustLogin(function ($user) {

            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'comment' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();

            $comments = NutritionComments::create([
                'user_id' => $user->id,
                'nutritionist_id' => $user->nutritionist_id,
                'content' => $input['comment'],
            ]);

            if ($comments->save()) {
                return Success::fill(['comments' => $comments])->send();
            } else
                return Fail::fill()->error([config('messages.error.userUpdate')])->message([config('messages.error.userUpdate')])->send();

        });


    }


    public function postPurpose()
    {

        return $this->mustLogin(function ($user) {
            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'purpose' => 'required',
                'target_weight' => 'required',
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();

            $userdetail = $user->details()->first();
            $userdetail->purpose = $input['purpose'];
            $userdetail->target_weight = $input['target_weight'];


            if ($userdetail->save()) {
                return Success::fill(['detail' => $userdetail])->send();
            } else
                return Fail::fill()->error([config('messages.error.userUpdate')])->message([config('messages.error.userUpdate')])->send();

        });
    }

    public function getNutritionist()
    {

        return $this->mustLogin(function ($user) {
            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'nutritionist_id' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();


            $userT = User::find(Input::get('nutritionist_id'));


            $details = NutritionistDetail::where('nutritionist_id', Input::get('nutritionist_id'))->first();

            $isCommentOkay = false;

            if ($user->nutritionist_id === (int)Input::get('nutritionist_id')) {
                $isCommentOkay = true;
            }


            $perpage = 10;
            $page = Input::get('page', 0);

            if ($details !== null) {
                $comment = NutritionComments::where('nutritionist_id', Input::get('nutritionist_id'))->where('status',1)->with(['user'])->orderBy('id','DESC')->skip($perpage * $page)->take($perpage)->get();
                return Success::fill(['commentOpen' => $isCommentOkay, 'nutritionist' => $userT, 'detail' => $details, 'comments' => $comment])->send();
            } else
                return Fail::fill()->error([config('messages.error.userUpdate')])->message([config('messages.error.userUpdate')])->send();


        });
    }


    public function postBlock()
    {

        return $this->mustLogin(function ($user) {

            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'block_id' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();


            $block = BlockUsers::create(['user_id' => $user->id, 'block_id' => $input['block_id']]);

            return Success::fill(['proggress' => true, 'block' => $block])->send();

        });

    }


    public function postUnBlock()
    {

        return $this->mustLogin(function ($user) {

            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'block_id' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();


            BlockUsers::where('user_id', $user->id)->where('block_id', $input['block_id'])->delete();

            return Success::fill(['proggress' => true])->send();

        });

    }

    public function postTargetWeight()
    {

        return $this->mustLogin(function ($user) {
            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'target_weight' => 'required',
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();

            $userdetail = $user->details()->first();

            if ($userdetail === null) {
                return Bad::fill()->error(['Lütfen Sağlık Bilgilerinizi Güncelleyin.'])->message($validator->errors()->all())->send();
            }
            $userdetail->target_weight = $input['target_weight'];

            if ($userdetail->save()) {
                return Success::fill(['detail' => $userdetail])->send();
            } else
                return Fail::fill()->error([config('messages.error.userUpdate')])->message([config('messages.error.userUpdate')])->send();

        });
    }

    public function postChangeNutritionist()
    {
        return $this->mustLogin(function ($user) {
            $input = Input::all();
            $messages = [
                'required' => Config::get('messages.error.required'),
            ];

            $cases = [
                'content' => 'required'
            ];

            $validator = Validator::make($input, $cases, $messages);

            if ($validator->fails())
                return Bad::fill()->error([config('messages.error.missedData')])->message($validator->errors()->all())->send();

            $change = ChangeNutritionist::create(
                [
                    'user_id' => $user->id,
                    'nutritionist_id' => $user->nutritionist_id,
                    'content' => $input['content'],
                    'status' => 0
                ]);
            $data = [
                'body' => $input['content'],
                'from' => $user->email
            ];
            Mail::send('emails.change', $data, function ($m) use ($user) {
                $m->from($user->email);
                $m->to('info@ceptediyetisyen.com')->subject("Diyetisyen Değişiklik Talebi");

            });
            return Success::fill(['proggress' => true, 'demand' => $change])->send();
        });
    }

    public function postUpdatePhoto()
    {
        return $this->mustLogin(function ($user) {
            if (Input::hasFile('photo')) {

                $name = str_random(15) . '.jpg';
                $filename = 'uploads/' . $name;
                $file = public_path($filename);

                Image::make(Input::file('photo'))->resize(300, 300)->save($file);
            }
            $user->pic = $name;
            if (!$user->save()) {
                return Bad::fill()->message(['Fotoğraf Güncellenemedi'])->send();
            }
            return Success::fill(['user' => $user])->send();

        });
    }


    public function postSpam()
    {
        return $this->mustLogin(function ($user) {
            $userSpammed = User::find(Input::get('user_id'));

            if($userSpammed === null){
                return Bad::fill()->error(['Kullanıcı bulunamadı.'])->message("Kullanıcı bulunamadı")->send();
            }else{
                $spam = Spam::create(['user_id' => Input::get('user_id'), 'adder_id' => $user->id ]);

                return Success::fill(['spam' => $spam])->send();
            }
        });
    }
}
