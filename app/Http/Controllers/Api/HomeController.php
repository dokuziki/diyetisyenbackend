<?php

/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 09/06/16
 * Time: 15:01
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Models\About;
use App\Models\Meal;
use App\Models\Privacy;
use App\Models\Terms;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;


class HomeController extends Controller
{

    protected $page_count = 10;

    public function getIndex()
    {
        return $this->mustLogin(function($user){
            $page = Input::get('page') === null ? 0: Input::get('page');


            $mealsQuery = Meal::active()->order()->take($this->page_count)->skip($this->page_count * $page)->with([
                'user',
                'user.nutritionist',
                'time',
                'comments' => function ($q) use ($user) {
                    if ($user === null) {
                        $q->take(0);
                    } else {

                        $q->whereHas('meal', function ($m) use ($user) {
                            $m->where('user_id', $user->id);
                        })->where(function ($query) use ($user) {
                            $query->where('user_id', $user->id)
                                ->orWhere('user_id', $user->nutritionist_id);
                        });

                    }
                },
                'comments.user',
                'likes.user' => function ($q) {
                    $q->get(['id', 'first_name', 'last_name','pic', 'nickname']);
                }
            ]);


            $meals = $mealsQuery->get();


            if ($meals === null) {
                return NotFound::message(['Kayıt Bulunamadı.'])->send();
            } else {
                return Success::fill(['feed' => $meals->toArray()])->send();
            }
        });

    }

    public function getTerms(){
        $data['item'] = Terms::find(1);
        return view('out.terms', $data);
    }

    public function getPrivacy(){
        $data['item'] = Privacy::find(1);
        return view('out.privacy',$data);
    }

    public function getAbout(){
        $data['item'] = About::find(1);
        return view('out.about', $data);
    }

    public function getBlog(){
        $item = About::find(2);
        return Redirect::to($item->content);
    }



    public function postMessage(){
        return $this->mustLogin(function($user){

            $data = [
                'body' => 'Mesaj: </br>'. Input::get('message'),
                'from' => $user->email
            ];

            Mail::send('emails.change', $data, function ($m) use ($user) {
                $m->from($user->email);
                $m->to('info@ceptediyetisyen.com')->subject("Kullanıcı Mesajı.");

            });

            return Success::fill(['message' => true])->send();
        });
    }
}