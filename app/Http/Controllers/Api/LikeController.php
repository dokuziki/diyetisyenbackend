<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\Success;
use App\Library\Push\PushHelper;
use App\Models\Likes;
use App\Models\Meal;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class LikeController extends Controller
{
    public function postLike(){
        return $this->mustLogin(function ($user){

            $meal_id = Input::get('id');
            $likeCheck = Likes::where('user_id',$user->id)->where('meal_id',$meal_id)->first();

            $message = '';
            if($likeCheck === null){
                $meal = Meal::find($meal_id);

                $liked = $meal->user->id;

                $likes = new Likes();
                $likes->user_id = $user->id;
                $likes->meal_id = $meal_id;
                $likes->timestamps = false;
                $likes->save();
                $message = 'liked';

                if($meal->user_id !== $user->id){
                    PushHelper::sendLike($user->id,$liked,$meal_id);
                }
            }else{
                $likeCheck->delete();
                $message = 'unliked';
            }

            return Success::fill(['status' => $message])->send();

        });
    }
}
