<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $userRole;
    public function __construct()
    {

        if(!Auth::guest()){
            if($this->getUser()->status == 1){
                $this->userRole = 'nutritionist';
            }elseif ($this->getUser()->status == 2){
                $this->userRole = 'admin';
            }

            View::share('username', $this->userAttr('name'));
            View::share('usertype',  $this->userRole);

        }else{
            $this->userRole = false;
        }


    }

    public function cacheDbResult($key, $query, $expire = 10)
    {

        if (!Cache::has($key)) {
            $result = Cache::remember($key, $expire, function () use ($query) {
                return $query->get();
            });
        } else {
            $result = Cache::get($key);
        }

        return $result;
    }

    public function cacheHelp($key, $data, $expire = 10)
    {

        if (!Cache::has($key)) {
            $result = Cache::remember($key, $expire, function () use ($data) {
                return $data;
            });
        } else {
            $result = Cache::get($key);
        }

        return $result;
    }

    public function cacheHelpFunc($key, $data, $expire = 10)
    {

        if (!Cache::has($key)) {
            $result = Cache::remember($key, $expire, function () use ($data) {
                return $data();
            });
        } else {
            $result = Cache::get($key);
        }

        return $result;
    }



    public function userAttr($attr)
    {
        return Auth::user()->$attr;
    }

    public function getUser()
    {
        return Auth::user();
    }

    public function inputValidation($cases)
    {
        $input = Input::all();

        $messages = [
            'required' => Config::get('messages.error.required')
        ];

        return Validator::make($input, $cases, $messages);
    }

    public function mustLogin($func){
        $user = Auth::user();

        if($user === null){
            return response()->json([
                'status' => false,
                'error' => config('messages.error.missedToken'),
                'message' => 'Profil Bilgilerine Erişilemiyor',
                'data' => []
            ], 401);
        }else{
            return $func($user);
        }
    }
}
