<?php
use App\Http\Responses\BaseResponse;

/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 09/06/16
 * Time: 15:32
 */
namespace App\Http\Responses;


class Fail extends BaseResponse
{

    private static $instance = null;

    function __construct()
    {
        parent::setHeader('500');
        parent::setStatus(false);
        parent::setError('');
    }


    public static function fill($data = [])
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        parent::setdata($data);


        return self::$instance;
    }

    public function error($error){
        parent::setError($error);
        return $this;
    }

    public function message($message){
        parent::setMessages($message);
        return $this;
    }

    public function send(){
        if(parent::getMessages() == null)
            parent::setMessages("");

        return response()->json([
            'status' => parent::getStatus(),
            'error' => parent::getError(),
            'messages' => parent::getMessages(),
            'data' => parent::getData()
        ], parent::getHeader());
    }
}