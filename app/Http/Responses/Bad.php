<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 10/06/16
 * Time: 01:15
 */


namespace App\Http\Responses;

class Bad extends BaseResponse
{

    private static $instance = null;

    function __construct()
    {
        parent::setHeader('400');
        parent::setStatus(false);
        parent::setError('');
    }

    public static function fill($data=[])
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        parent::setdata($data);


        return self::$instance;
    }

    public function error($error){
        $errorS = "";
        if(is_array($error) && isset($error[0])){
            $errorS = $error[0];
        }

        parent::setError($errorS);
        return $this;
    }

    public function message($message){
        $messageS = [];
        if(is_array($message) && isset($message[0])){
            $messageS = $message;
        }else{
            $messageS = [$message];
        }
        parent::setMessages($messageS);
        return $this;
    }

    public function send(){
        if(parent::getMessages() == null)
            parent::setMessages("");

        if(parent::getError() == null)
            parent::setError(Config('messages.error.unknown'));

        return response()->json([
            'status' => parent::getStatus(),
            'error' => parent::getError(),
            'messages' => parent::getMessages(),
            'data' => parent::getData()
        ], parent::getHeader());
    }
}