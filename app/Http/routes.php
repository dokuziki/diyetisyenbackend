<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Route;


Route::get('_debugbar/assets/stylesheets', [
    'as' => 'debugbar-css',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@css'
]);

Route::get('_debugbar/assets/javascript', [
    'as' => 'debugbar-js',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@js'
]);

Route::get('_debugbar/open', [
    'as' => 'debugbar-open',
    'uses' => '\Barryvdh\Debugbar\Controllers\OpenController@handler'
]);


//diyetisyen panel
Route::get('/backoffice/login', 'Auth\AdminController@getLogin');
Route::post('/backoffice/login', 'Auth\AdminController@postLogin');
Route::get('/backoffice/logout', 'Auth\AdminController@getLogout');

//admin panel
Route::get('/admin/login', 'Auth\OfficeController@getLogin');
Route::post('/admin/login', 'Auth\OfficeController@postLogin');
Route::get('/admin/logout', 'Auth\OfficeController@getLogout');


Route::group(['prefix' => 'admin', 'middleware' => ['authOffice'], 'namespace' => 'Office'], function () {

    Route::controller('user', 'UserController',
        [
            'getIndex' => 'office.user.index',
            'getUpsert' => 'office.user.upsert',
            'postUpsert' => 'office.user.upsert',
            'getSpam' => 'office.user.spam',
            'getDiyetisyen' => 'office.user.diyetisyen',
            'getLogas' => 'office.user.logas'
        ]
    );

    Route::controller('nutrition', 'NutritionController',
        [
            'getIndex' => 'office.nutrition.index',
            'getUpsert' => 'office.nutrition.upsert',
            'postUpsert' => 'office.nutrition.upsert',
            'getChange' => 'office.nutrition.change',
            'getComments' => 'office.nutrition.comments',
            'postComments' => 'office.nutrition.comments'
        ]
    );

    Route::controller('portion', 'PortionController',
        [
            'getIndex' => 'office.portion.index',
            'postIndex' => 'office.portion.index',
            'getDetail' => 'office.portion.detail',
            'postDetail' => 'office.portion.detail'
        ]
    );

    Route::controller('notifications', 'NotificationsController',
        [
            'getIndex' => 'office.notifications.index',

        ]
    );

    Route::controller('blog', 'AboutController',
        [
            'getBlog' => 'office.pages.blog',
            'postBlog' => 'office.pages.blog'

        ]
    );

    Route::controller('terms', 'TermsController',
        [
            'getTerms' => 'office.pages.terms',
            'postTerms' => 'office.pages.terms'

        ]
    );
    Route::controller('privacy', 'PrivacyController',
        [
            'getPrivacy' => 'office.pages.privacy',
            'postPrivacy' => 'office.pages.privacy'

        ]
    );

    Route::controller('about', 'AboutController',
        [
            'getAbout' => 'office.pages.about',
            'postAbout' => 'office.pages.about'

        ]
    );

    Route::controller('list', 'DietListController',
        [
            'getIndex' => 'office.nutrition.dietlist'
        ]
    );

    Route::controller('coop', 'CoopController',
        [
            'getIndex' => 'office.coop.index',
            'getUpsert' => 'office.coop.upsert',
            'postUpsert' => 'office.coop.upsert'
        ]
    );

    Route::controller('export', 'ExcelController',
        [
            'getIndex' => 'office.excel.index'
        ]
    );


    Route::controller('/', 'HomeController', ['getIndex' => 'office.dash']);

});

Route::group(['prefix' => 'backoffice', 'middleware' => ['authAdmin'], 'namespace' => 'Panel'], function () {

    Route::controller('user', 'UserController',
        [
            'getIndex' => 'admin.user.index',
            'getUpsert' => 'admin.user.upsert',
            'postUpsert' => 'admin.user.upsert'
        ]
    );

    Route::controller('meal', 'MealController',
        [
            'getIndex' => 'admin.meals.index',
            'getPremium' => 'admin.meals.premium',
            'getUpsert' => 'admin.meals.upsert',
            'getChat' => 'admin.meals.chat',
            'getCloseChat' => 'admin.meals.closeChat'
        ]
    );

    Route::controller('userinfo', 'InfoController',
        [
            'getIndex' => 'admin.userinfo.index',
            'getUpsert' => 'admin.userinfo.upsert'
        ]
    );

    Route::controller('codes', 'CodesController',
        [
            'getIndex' => 'admin.codes.index',
            'postIndex' => 'admin.codes.index'
        ]
    );

    Route::controller('dietlist', 'DietlistController',
        [
            'getIndex' => 'admin.list.index',
            'getUpsert' => 'admin.list.upsert',
            'postUpsert' => 'admin.list.upsert'
        ]
    );

    Route::controller('detail', 'DetailController',
        [
            'getIndex' => 'admin.detail.index',
            /*'postIndex' => 'admin.detail.index'*/
        ]
    );


    Route::controller('/', 'HomeController', ['getIndex' => 'admin.dash']);

});

if (App::environment() == 'local') {
    $domain = ['namespace' => 'Api', 'prefix' => 'api'];
} elseif (App::environment() == 'prod') {
    $domain = ['namespace' => 'Api', 'domain' => 'apiv2.ceptediyetisyen.com'];
}


Route::group($domain, function () {

    Route::controller('device', 'DeviceController');
    Route::get('terms', 'HomeController@getTerms');
    Route::get('about', 'HomeController@getAbout');
    Route::get('privacy', 'HomeController@getPrivacy');
    Route::get('blog', 'HomeController@getBlog');


    Route::group(['middleware' => ['requestValidation', 'token']], function () {

        Route::controller('meal', 'MealController');
        Route::controller('user', 'UserController');
        Route::controller('toplist', 'ToplistController');
        Route::controller('notifications', 'NotificationController');
        Route::controller('comments', 'CommentController');
        Route::controller('payment', 'PaymentController');
        Route::controller('likes', 'LikeController');
        Route::controller('favorites', 'FavoriteController');
        Route::controller('dietlist', 'DietlistController');
        Route::controller('/', 'HomeController');
    });


});


Route::get('/', function () {

});

