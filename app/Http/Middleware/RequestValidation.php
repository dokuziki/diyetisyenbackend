<?php

namespace App\Http\Middleware;

use App\Http\Responses\Fail;
use Closure;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

class RequestValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->token === null){
            $request->token = Request::header('token');
            Input::merge(array('token' => Request::header('token')));
        }

        if($request->encr === null){
            $request->encr = Request::header('encr');
            Input::merge(array('encr' => Request::header('token')));
        }

        $allInputs = Input::except(['encr','photo']);

        ksort($allInputs);

        $encrSalt = '';
        foreach($allInputs as $key => $value){
            if(!is_array($value))
            $encrSalt .= $value;
        }

        $encrSalt .= env('REQUEST_KEY');
        $encr = Input::get('encr');

        if(hash('sha256',$encrSalt) === $encr || $encr === env('SPECIAL_KEY')){
            return $next($request);
        }else{
            return $this->error();
        }


    }


    private function error()
    {
        return response()->json([
            'status' => false,
            'error' => config('messages.error.invalidRequest'),
            'message' => 'invalidRequest',
            'data' => []
        ], 401);
    }
}
