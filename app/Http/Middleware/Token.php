<?php

namespace App\Http\Middleware;

use App\Models\Auth;
use App\Models\User;
use Carbon\Carbon;
use Closure;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

class Token
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->token === null){
            $request->token = Request::header('token');
            Input::merge(array('token' => Request::header('token')));
        }

        if($request->encr === null){
            $request->encr = Request::header('encr');
            Input::merge(array('encr' => Request::header('token')));
        }



        if (!(file_exists(public_path(substr($request->getPathInfo(),1))) && strpos($request->getPathInfo(), '.') )&& isset($request->token) && $request->token != "") {
            $key = env('HASH_KEY');

            try{
                $GLOBALS['client'] = JWT::decode(Input::get('token'), $key,['HS256']);

                if($request->auth !== null){
                    $authResult = Auth::where('token',$request->auth)->with('user')->first();

                    if($authResult !== null){
                        \Illuminate\Support\Facades\Auth::loginUsingId($authResult->user->id);
                        $current = Carbon::now();

                        $authResult->last_activity = $current->format('Y-m-d H:i:s');
                        $authResult->save();
                    }else{
                       return $this->error($request);
                    }
                }


            }catch(\Exception $err){
                return $this->errorInvalid($request);

            }


            return $next($request);

        }
        else if (($request->getPathInfo() == '/device/register'  && $request->getMethod() == 'POST')  ||  $request->getPathInfo() == '/splash') {

            return $next($request);

        } else {

            return $this->error($request);
        }

    }

    private function error($request)
    {
        return response()->json([
            'status' => false,
            'error' => config('messages.error.missedToken'),
            'message' =>  $request->getPathInfo() .' - '.config('messages.error.missedToken'),
            'data' => []
        ], 401);
    }

    private function errorInvalid($request)
    {
        return response()->json([
            'status' => false,
            'error' => 'invalid token',
            'message' => 'invalid token exception',
            'data' => []
        ], 401);
    }
}
