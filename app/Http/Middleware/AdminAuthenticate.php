<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\Auth\AdminController;
use Illuminate\Contracts\Auth\Guard;

class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    public function handle($request, Closure $next)
    {

        if ( $this->auth->guest() or $this->contentIncorrect($request) )
        {
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                return redirect()->action('Auth\AdminController@getLogin');
            }
        }

        $response = $next($request);

        if($request->ajax())
        {
            $collection = $response->original;
            $collection['token'] = csrf_token();
            $response->setContent($collection);
        }

        return $response;
    }

    private function contentIncorrect($request)
    {
        return false;
    }
}
