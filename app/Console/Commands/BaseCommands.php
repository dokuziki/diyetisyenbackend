<?php

/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 10/06/16
 * Time: 01:28
 */
namespace App\Console\Commands;
use Illuminate\Console\Command;

class BaseCommands extends Command
{

    public function runExec($command)
    {
        $this->comment('Running command: '. $command);
        exec($command, $output, $return);
        if(!empty($output)){
            foreach($output as $line){
                if($return !== false){
                    $this->info($line);
                } else {
                    $this->error($line);
                }
            }
        }
    }
}