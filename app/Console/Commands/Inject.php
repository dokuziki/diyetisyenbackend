<?php

namespace App\Console\Commands;


use App\Models\Meal;
use App\Models\MealOld;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserDetailOld;
use App\Models\UserOld;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use PDO;
use PDOException;

class Inject extends BaseCommands
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ward:inject';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Model and Migration for Laravel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Bağlanıyor');
        $count = 100;
        $i = 0;

        while (true) {

            $users = UserOld::orderBy('id', 'DESC')->take($count)->skip($count * $i)->get();

            if (count($users) === 0) {
                break;
            } else {
                ++$i;
            }

            foreach ($users as $user) {
                $this->info('Kullanıcı ' . $user->fname . ' ' . $user->lname . ' - ' . $user->id);

                $newUser = new User();
                $userDetail = new UserDetail();

                try {
                    $newUser->first_name = $user->fname;
                    $newUser->last_name = $user->lname;
                    $newUser->nickname = $user->user_name;
                    $newUser->pic = str_replace('users/', '', $user->image);
                    $newUser->fb_id = $user->fb_id;
                    $newUser->email = $user->email;
                    $newUser->password = Hash::make($user->password);

                    $newUser->status = 0;
                    $newUser->location = $user->il;
                    $newUser->gender = $user->cinsiyet === null ? 2 : $user->cinsiyet;
                    $newUser->nutritionist_id = 1;

                    $newUser->save();
                    $userDetailOld = UserDetailOld::where('user_id', $user->id)->first();
                    if ($userDetailOld === null) {
                        $userDetail->user_id = $newUser->id;
                        $userDetail->purpose = "";
                        $userDetail->height = $user->boy === null ? 0 : $user->boy;

                    } else {
                        $userDetail->user_id = $newUser->id;
                        $userDetail->purpose = $userDetailOld->amac;
                        $userDetail->height = $userDetailOld->boy === null ? 0 : $userDetailOld->boy;

                    }

                    $userDetail->target_weight = $user->hedef_kilo === null ? 0 : $user->hedef_kilo;

                    $userDetail->q1 = '';
                    $userDetail->q2 = '';
                    $userDetail->q3 = '';
                    $userDetail->q4 = '';
                    $userDetail->save();

                } catch (\ErrorException $err) {
                    print_r($newUser->toArray());
                    print_r($userDetailOld->toArray());
                    dd($err);

                } catch (QueryException $err) {
                    continue;
                }



                $meals = MealOld::where('user_id', $user->id)->get();

                foreach ($meals as $meal) {
                    $newMeal = new Meal();
                    $pic_path = public_path('/uploads/meals/'.str_replace('pics/', '', $meal->pic_name));
                        $newMeal->pic = str_replace('pics/', '', $meal->pic_name);

                        $newMeal->date = Carbon::createFromFormat('Y-m-d H:i:s', $meal->pic_date)->format('Y-m-d');
                        $newMeal->approved = $meal->approved;
                        $newMeal->status = 1;
                        $newMeal->point = $meal->puan === null ? 0 : (int)$meal->puan * 2;
                        $newMeal->user_id = $newUser->id;
                        $newMeal->desc = $meal->pic_desc;
                        $newMeal->push_status = $meal->push_sent;
                        $newMeal->time_id = $meal->pic_ogun;
                        $newMeal->save();

                }


            }


        }


    }
}




