<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 01/09/16
 * Time: 15:36
 */

namespace App\Library\Push;


use App\Models\Auth as AuthModel;
use App\Models\Dietlist;
use App\Models\Meal;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class PushHelper
{
    private static $onesignal;

    public static function __callStatic($method, $arguments)
    {
        if (method_exists(__CLASS__, $method)) {
            self::getInstance();
            return forward_static_call_array(array(__CLASS__,$method),$arguments);
        }
    }

    public static function getInstance()
    {
        $config = [
            'app_id' => '3889ad72-e0c9-44d8-af53-ab321001c9b3',
            'rest_api_key' => 'YWMyZTFmMzgtMzRhZi00MDU2LWFjNTktMmQ1ZjZiNDM4MGJh',
            'user_auth_key' => 'MmY5N2YyZWEtOWRkOC00M2RjLWFmYTYtYjgzZDVhNGMwYzJk',
        ];

        if (null === static::$onesignal) {
            static::$onesignal = new OneSignal($config['app_id'], $config['rest_api_key'], $config['user_auth_key']);
        }

        return static::$onesignal;
    }

    private static function addPlayer($registrationid,$userid=false){

        $deviceType = $GLOBALS['client']->type === 'android' ? 1:0;
        $tag = [];

        if($userid){
            $tag['user_id'] =  $userid;
        }
        $parameters = [
            'device_type' => $deviceType, //0 = iOS, 1 = Android,
            'identifier' => $registrationid,//registration id
            'language' => $GLOBALS['client']->language,
            'game_version' => $GLOBALS['client']->app_version,
            'tags' =>$tag,
            //'test_type' => 2 //  1 Development, 2
        ];

        return static::$onesignal->addPlayer($parameters);
    }

    private static function mealFormat($meal){
        return [
            "id"=> $meal->id,
            "user_id" => $meal->user_id,
            "pic" => $meal->pic,
            "date" =>  $meal->date,
            "time_id" => $meal->time_id,
            "desc" => $meal->desc,
            "comment_count" => $meal->comment_count,
            "favorite_count" => $meal->favorite_count,
            "like_count" => $meal->like_count,
            "is_mine" => $meal->is_mine,
            "is_liked" => $meal->is_liked,
            "isFavorite" => $meal->isFavorite,
            "comments" => $meal->comments
        ];
    }
    private static function sendMealComment($user_id,$meal_id){
        $user = User::find($user_id);
        $registrations = self::getRegistrationKeys(AuthModel::where('user_id',$user_id));

        if(count($registrations) === 0 || !is_array($registrations)){
            return true;
        }


        $meal = Meal::find($meal_id);
        $mealS = self::mealFormat($meal);

        $pushtitles = [
            1 => 'kahvaltınıza',
            2 => 'ara öğününüze',
            3 => 'öğle yemeğinize',
            4 => 'ara öğününüze',
            5 => 'akşam yemeğinize',
            6 => 'ara öğününüze'
        ];

        $head = "Cepte Diyetisyen";
        $message = "Diyetisyeniniz ".$pushtitles[$meal->time->id]." önerilerde bulundu";

        $parameters = [
             'contents' => ['en' => $message, 'tr' => $message],
             'headings' => ['en' => $head, 'tr' => $head],
             'include_player_ids' => $registrations,
             'data' => ['content_type' => 'meal', 'meal_id' => $meal_id]
        ];

        Notification::create([
            'user_id' => $user_id,
            'sender_id' => $user->nutritionist_id,
            'message' => $message,
            'content_type' => 'meal',
            'content_id' => $meal_id,
            'is_global' => 0
        ]);

        if(count($registrations) === 0){
            return true;
        }

        return static::$onesignal->sendPushToUser($parameters);

    }

    private static function sendMealApply($user_id,$meal_id){
        $user = User::find($user_id);

        $registrations = self::getRegistrationKeys(AuthModel::where('user_id',$user_id));



        $meal = Meal::find($meal_id);
        $mealS = self::mealFormat($meal);

        $pushtitles = [
            1 => 'kahvaltınızı',
            2 => 'ara öğününüzü',
            3 => 'öğle yemeğinizi',
            4 => 'ara öğününüzü',
            5 => 'akşam yemeğinizi',
            6 => 'ara öğününüzü'
        ];

        $head = "Cepte Diyetisyen";
        $message = "Diyetisyeniniz ".$pushtitles[$meal->time->id]." kontrol etti ve yıldız verdi";

        $parameters = [
            'contents' => ['en' => $message, 'tr' => $message],
            'headings' => ['en' => $head, 'tr' => $head],
            'include_player_ids' => $registrations,
            'data' => ['content_type' => 'meal','meal_id' => $meal_id]
        ];


        Notification::create([
            'user_id' => $user_id,
            'sender_id' => $user->nutritionist_id,
            'message' => $message,
            'content_type' => 'meal',
            'content_id' => $meal_id,
            'is_global' => 0
        ]);

        if(count($registrations) === 0){
            return true;
        }
        return static::$onesignal->sendPushToUser($parameters);

    }

    private static function sendDietList($list_id){


        $dietlist = Dietlist::find($list_id);

        $user = User::find($dietlist->user_id);
        $registrations = self::getRegistrationKeys(AuthModel::where('user_id',$dietlist->user_id));

        $head = "Cepte Diyetisyen";
        $message = "Diyetisyeniniz size özel diyet listenizi hazırladı";

        $parameters = [
            'contents' => ['en' => $message, 'tr' => $message],
            'headings' => ['en' => $head, 'tr' => $head],
            'include_player_ids' => $registrations,
            'data' => ['content_type' => 'dietlist','dietlist_id' => $list_id]
        ];

        Notification::create([
            'user_id' => $dietlist->user_id,
            'sender_id' => $user->nutritionist_id,
            'message' => $message,
            'content_type' => 'list',
            'content_id' => $list_id,
            'is_global' => 0
        ]);


        if(count($registrations) === 0){
            return true;
        }
        return static::$onesignal->sendPushToUser($parameters);

    }

    private static function sendLike($liker_id,$liked_id,$meal_id){

        $registrations = self::getRegistrationKeys(AuthModel::where('user_id',$liked_id));


        $meal = Meal::find($meal_id);
        $mealS = self::mealFormat($meal);


        $liker = User::find($liker_id);


        $pushtitles = [
            1 => 'kahvaltınızı',
            2 => 'ara öğününüzü',
            3 => 'öğle yemeğinizi',
            4 => 'ara öğününüzü',
            5 => 'akşam yemeğinizi',
            6 => 'ara öğününüzü'
        ];

        $head = "Cepte Diyetisyen";
        $message = $liker->first_name." ". $liker->last_name." ".$pushtitles[$meal->time->id]." beğendi.";

        $parameters = [
            'contents' => ['en' => $message, 'tr' => $message],
            'headings' => ['en' => $head, 'tr' => $head],
            'include_player_ids' => $registrations,
            'data' => ['content_type' => 'meal','meal_id' => $meal->id]
        ];

        Notification::create([
            'user_id' => $liked_id,
            'sender_id' => $liker_id,
            'message' => $message,
            'content_type' => 'meal',
            'content_id' => $meal_id,
            'is_global' => 0
        ]);

        if(count($registrations) === 0){
            return true;
        }
        return static::$onesignal->sendPushToUser($parameters);

    }

    private static function sendFavorite($liker_id,$liked_id,$meal_id){

        $registrations = self::getRegistrationKeys(AuthModel::where('user_id',$liked_id));



        $meal = Meal::find($meal_id);
        $mealS = self::mealFormat($meal);

        $liker = User::find($liker_id);

        $pushtitles = [
            1 => 'kahvaltınızı',
            2 => 'ara öğününüzü',
            3 => 'öğle yemeğinizi',
            4 => 'ara öğününüzü',
            5 => 'akşam yemeğinizi',
            6 => 'ara öğününüzü'
        ];

        $head = "Cepte Diyetisyen";
        $message = $liker->first_name." ". $liker->last_name." ".$pushtitles[$meal->time->id]." favorilerine ekledi.";

        $parameters = [
            'contents' => ['en' => $message, 'tr' => $message],
            'headings' => ['en' => $head, 'tr' => $head],
            'include_player_ids' => $registrations,
            'data' => ['content_type' => 'meal','meal_id' => $meal->id]
        ];

        Notification::create([
            'user_id' => $liked_id,
            'sender_id' => $liker_id,
            'message' => $message,
            'content_type' => 'meal',
            'content_id' => $meal_id,
            'is_global' => 0
        ]);

        if(count($registrations) === 0){
            return true;
        }
        return static::$onesignal->sendPushToUser($parameters);

    }

    private static function sendPrivatePush($user_ids,$message){



        $registrations = self::getRegistrationKeys(AuthModel::whereIn('user_id', self::takeIDs($user_ids)));



        $head = "Cepte Diyetisyen";

        $parameters = [
            'contents' => ['en' => $message, 'tr' => $message],
            'headings' => ['en' => $head, 'tr' => $head],
            'include_player_ids' => $registrations,
            'data' => ['content_type' => 'system']
        ];

        foreach(self::takeIDs($user_ids) as $_userid){
            Notification::create([
                'user_id' => $_userid,
                'sender_id' => 0,
                'message' => $message,
                'content_type' => 'system',
                'content_id' => 0,
                'is_global' => 0
            ]);
        }

        if(count($registrations) === 0){
            return true;
        }
        return static::$onesignal->sendPushToUser($parameters);

    }

    private static function sendAllPush($message){
        

        Notification::create([
            'user_id' => 0,
            'sender_id' => 0,
            'message' => $message,
            'content_type' => 'system',
            'content_id' => 0,
            'is_global' => 1
        ]);

        return static::$onesignal->sendNotificationToAll($message,null,['content_type' => 'system']);

    }

    private static function getRegistrationKeys($query){
        $auths = $query->whereRaw(DB::raw('`auths`.`device` is not null'))->get();
        $registrations = [];

        foreach($auths as $auth){
            if($auth->onesignal !== null && $auth->onesignal !== ''){
                $registrations[] = $auth->onesignal;
            }
        }

        return (array) array_values(array_unique($registrations));
    }

    private static function GetPushs($page){
        return static::$onesignal->getPushNotifications($page);
    }

    private static function takeIDs($user_ids){
        $ids = [];

        foreach($user_ids as $u){
            $ids[] =  $u->id;
        }

        return $ids;

    }

}