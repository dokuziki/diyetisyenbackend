<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 07/03/16
 * Time: 20:16
 */


namespace App\Library;

use Laravel\Socialite\Two\FacebookProvider;

class SocialConnect extends FacebookProvider {

    public function userFromToken($access_token)
    {
        $this->fields([
            'first_name', 'last_name', 'email', 'gender', 'birthday'
        ]);
        $user = $this->mapUserToObject($this->getUserByToken($access_token));
        return $user->setToken($access_token);
    }
}