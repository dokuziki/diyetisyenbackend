#!/usr/bin/env bash
curl -XDELETE 'http://127.0.0.1:9200/directik/'
curl -XPUT 'http://127.0.0.1:9200/directik/'

curl -XPUT 'http://127.0.0.1:9200/directik/_mapping/stores' -d '
 {
     "stores" : {
         "properties" : {
             "id" : {"type" : "integer" },
             "keys" : {
             	"type" : "string" 
             },
             "location" : {"type" : "geo_point" }
         }
     }
 }
'
