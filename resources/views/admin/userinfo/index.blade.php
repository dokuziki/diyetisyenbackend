@extends('admin.layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Hasta Bilgileri</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <form method="get">
                                    <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                        <label>İsim:<input type="search" name="f_key" class="form-control input-sm input-small input-inline" placeholder="İsim" aria-controls="DataTables_Table_0"></label>
                                        <label style="margin-left: 20px">Soyisim:<input type="search" name="l_key" class="form-control input-sm input-small input-inline" placeholder="Soy İsim" aria-controls="DataTables_Table_0"></label>

                                        <div class="btn-group" style="margin-left: 20px">
                                            <button class="btn sbold green"> Ara
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table {{--data-table--}} table-striped table-bordered table-hover table-checkable order-column"
                        >
                            <thead>
                            <tr>
                                <th> Adı Soyadı</th>
                                <th> Durum</th>
                                <th> Detay</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr class="odd gradeX">
                                    <td> {{$user->first_name}} {{$user->last_name}}</td>
                                    <td>@if($user->isPremium) Ücretli Üye @elseif($user->isPrivate) Özel Üye @else Normal Üye @endif</td>
                                    <td>
                                        <a href="{{asset('backoffice/userinfo/upsert')}}/{{$user->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i>Hasta Detayı</a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row ">
                            <div class="col-xs-12">
                                <center>
                                    {!! $users->render() !!}
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
</div>



    <script>
        $(document).ready(function () {

            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });

            var body = $('body');



            body.on('click','.deleteOpenModal',function(event){
                $('#deleteConfirm').modal('show');
                var dataID = $(this).attr('data-id');

                var button = $(this);
                var deleted = $(this).attr('data-deleted');

                $('.delete').click(function (){
                    window.location= '#'+'/'+dataID;
                });

            });
        });


    </script>
        @endsection