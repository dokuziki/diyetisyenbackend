@extends('admin.layouts.master')
@section('content')
    <style type="text/css">
        textarea {
            resize: none;
        }

        .modal-header {
            padding: 5px 10px;
            background-color: #61d6ae;
            color: #000000;
        }

        .modal_open{
            cursor:pointer;
        }

        /* Modal Body */
        .modal-body {padding: 2px 16px;}

        /* Modal Footer */

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #ffffff;
            width: 50%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(185, 255, 91, 0);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {top: -200px; opacity: 0}
            to {top: 0; opacity: 1}
        }

        @keyframes animatetop {
            from {top: -200px; opacity: 0}
            to {top: 0; opacity: 1}
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Hasta Detayı</span>
                    </div>

                </div>
                <div class="portlet-body">

                    <!-- BEGIN FORM-->
                    <form action="{{asset('backoffice/user/upsert')}}@if(isset($item['id']))/{{$item['id']}}@endif" method="post" class="horizontal-form">
                        <input type="hidden" name="id"   @if(isset($item['id'])) value="{{$item['id']}}" @else value="false" @endif />

                        <div class="form-body">
                            <h3 class="form-section">Hasta Bilgileri</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Kilo Grafiği</label>
                                        <canvas id="myChart" width="997px" height="200px">
                                            @foreach($item['weightLogs'] as $log)
                                                <div class="logs">{{$log->weight}}</div>
                                                <div class="dates">{{substr($log->created_at,0,-9)}}</div>
                                            @endforeach
                                        </canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <b>Diyet Listesi</b><br>
                                        @if( $payments['list'] !== null || count($lists)>0)
                                            @foreach($lists as $i => $list)
                                                <br>
                                                <span href="myModal{{$list->id}}" class="modal_open"> Liste {{$i + 1}} : {{$list->created_at}} </span>

                                                <div id="myModal{{$list->id}}" class="modal">

                                                    <!-- Modal content -->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <span class="close">&times;</span>
                                                            <h2>{{$list->title}}</h2>
                                                        </div>
                                                        <div class="modal-body">
                                                            <label class="control-label" for="single"> <b>Veriliş Tarihi</b> </label>
                                                            <input type="text" name="email" class="form-control" value="@if($list->date&&$list->date !== null){{$list->date}}@endif" disabled>

                                                            <label class="control-label" for="single"><b>Diyet Yapma Amacı</b></label>
                                                            <textarea class="form-control" rows="2"
                                                                      disabled>@if($purpose['purpose'] == 0) Kilo Vermek İstiyorum @elseif($purpose['purpose'] == 1) Mevcut Kilomu Korumak İstiyorum @elseif($purpose['purpose'] == 2) Kilo Almak İstiyorum @endif</textarea>

                                                            <label class="control-label" for="single"><b>Sabah</b></label>
                                                            <textarea class="form-control" disabled rows="5">@if($list->sabah&&$list->sabah !== null){{$list->sabah}} @endif</textarea>

                                                            <label class="control-label" for="single"><b>Kahvaltı</b></label>
                                                            <textarea class="form-control" disabled rows="5">@if($list->kahvalti&&$list->kahvalti !== null){{$list->kahvalti}}@endif</textarea>

                                                            <label class="control-label" for="single"><b>Ara Öğün</b></label>
                                                            <textarea class="form-control" disabled rows="5">@if($list->ara_ogun1&&$list->ara_ogun1 !== null){{$list->ara_ogun1}}@endif</textarea>

                                                            <label class="control-label" for="single"><b>Öğle</b></label>
                                                            <textarea class="form-control" disabled rows="5">@if($list->ogle&&$list->ogle !== null){{$list->ogle}}@endif</textarea>

                                                            <label class="control-label" for="single"><b>Ara Öğün</b></label>
                                                            <textarea class="form-control" disabled rows="5">@if($list->ara_ogun2&&$list->ara_ogun2 !== null){{$list->ara_ogun2}}@endif</textarea>

                                                            <label class="control-label" for="single"><b>Akşam </b></label>
                                                            <textarea class="form-control" disabled rows="5">@if($list->aksam&&$list->aksam !== null){{$list->aksam}}@endif</textarea>

                                                            <label class="control-label" for="single"><b>Ara Öğün</b></label>
                                                            <textarea class="form-control" disabled rows="5" disabled>@if($list->ara_ogun3&&$list->ara_ogun3 !== null){{$list->ara_ogun3}}@endif</textarea>

                                                            <label class="control-label" for="single"><b>Notlar</b></label>
                                                            <textarea class="form-control" disabled rows="5" disabled>@if($list->notlar&&$list->notlar !== null) {{$list->notlar}}@endif </textarea><br>



                                                        </div>
                                                    </div>

                                                </div>
                                            @endforeach
                                        @else
                                            Hiç alınmamış
                                        @endif

                                        <hr>


                                        <b>Online Sıkı Takip</b> <br>
                                        @if( $payments['premium'] !== null)
                                            Satın alma
                                            tarihi: {{\Carbon\Carbon::createFromFormat('Y-m-d',$payments['premium']->start_date)->format('d-m-Y')}}
                                            <br>
                                            Yenilenme
                                            tarihi: {{\Carbon\Carbon::createFromFormat('Y-m-d',$payments['premium']->end_date)->format('d-m-Y')}}
                                        @else
                                            Hiç alınmamış
                                        @endif
                                        <br><br>
                                    </div>

                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Adı</label>
                                        <input type="text" name="first_name" class="form-control" placeholder="" @if(isset($item['first_name'])) value="{{$item['first_name']}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Soyadı</label>
                                        <input type="text" name="last_name" class="form-control" placeholder="" @if(isset($item['last_name'])) value="{{$item['last_name']}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Mail Adresi</label>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($item['email'])) value="{{$item['email']}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>
                                <!--/span-->

                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Cinsiyet</label>
                                        <input type="text" name="status" class="form-control" @if($item['gender'] === 0) value="Bay" @elseif($item['gender'] === 1) value="Bayan" @else value="Belirtilmemiş" @endif  disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Boy</label>
                                        <input type="text" name="email" class="form-control" placeholder="" value="{{$item['details']['height']}}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Hedef Kilo</label>
                                        <input type="text" name="email" class="form-control" placeholder="" value="{{$item['details']['target_weight']}}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Rumuz</label>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($item['nickname'])) value="{{$item['nickname']}}" @endif disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Lokasyon</label>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($item['location'])) value="{{$item['location']}}" @endif disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Cihaz Türü</label>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($device)) value="{{$device}}" @endif disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Yaş</label>
                                        <?php
                                        use Carbon\Carbon;$year = date_parse($item['birthday']);
                                        $now = Carbon::now()->format('Y');
                                        $birthday = $now - $year['year'];
                                        ?>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($item['birthday'])) value="{{$birthday}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Telefon</label>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($item['phone'])) value="{{$item['phone']}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Yemediğiniz ve/veya alerjiniz olan besinler var mı ? </b></label>
                                        <textarea class="form-control" rows="3" disabled>{{$item['details']['q1']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Kabızlık problemi yaşıyor musunuz ?</b></label>
                                        <textarea class="form-control" rows="3" disabled>{{$item['details']['q2']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Sürekli olarak kullanmakta olduğunuz ilaçlar var mı ? </b></label>
                                        <textarea class="form-control" rows="3" disabled>{{$item['details']['q3']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Sağlık durumunuz veya belirtmek istediğinz bir hastalığınız var mı ?</b></label>
                                        <textarea class="form-control" rows="3" disabled>{{$item['details']['q4']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Biyografi</b></label>
                                        <textarea class="form-control" rows="3" disabled>{{$item['bio']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Diyet Yapma Amacı</b></label>
                                        <textarea class="form-control" rows="3" disabled>@if($purpose['purpose'] == 0) Kilo Vermek İstiyorum @elseif($purpose['purpose'] == 1) Mevcut Kilomu Korumak İstiyorum @elseif($purpose['purpose'] == 2) Kilo Almak İstiyorum @endif</textarea>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <br>
                            <br>
                            <div class="row">

                                <!--/span-->
                            </div>
                            <br>
                            <br>

                        </div>
                        <br>
                    </form>
                    <!-- END FORM-->

                    <style>
                        .orderRow td:first-child{
                            display:none;
                        }

                        .orderRow  td:nth-child(3) {
                            text-align: center;
                        }
                        .table th:first-child{
                            display:none;
                        }
                    </style>

                    <br>
                    <br>





                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/admin/global/scripts/Chart.min.js')}}" type="text/javascript"></script>
    <script>
        var ctx = document.getElementById("myChart");
        var DATA = [];
        var DATES = [];
        function deneme(){
            var elems = document.getElementsByClassName("logs");
            for(elem of elems) {
                DATA.push(elem.innerHTML);
            }
            console.log(DATA);
        }

        function denemes(){
            var elems = document.getElementsByClassName("dates");
            for(elem of elems) {
                DATES.push(elem.innerHTML);
            }
            console.log(DATES);
        }
        deneme();
        denemes();

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: DATES,
                datasets: [{
                    label: 'Kilo Grafiği',
                    data: DATA,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
        });
    </script>

    <script>


        $(document).ready(function () {
            $('.modal_open').on('click',function (e) {
                e.preventDefault();
                $('#'+$(this).attr('href')).css('display','block');
            });

            $('.modal .close').on('click',function (e) {
                e.preventDefault();
                $(this).parents('.modal').css('display','none');
            });

            $(window).on('click',function (e) {

                if($(e.target).attr('class')==='modal') {
                    $('.modal').css('display', 'none');
                    return false;
                }

            });
        });


    </script>


@endsection