<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="tr">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Login Page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/admin/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/admin/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/admin/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/admin/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/admin/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/admin/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/admin/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{asset('assets/admin/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{asset('assets/admin/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{asset('assets/admin/pages/css/login-2.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <script src="{{asset('assets/admin/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
    <script>
        function validationErrors(prefix, errors)
        {
            $.each(errors, function (name, error) {
                findValidationItem(prefix, name, function (item) {

                    var form_group = item.parents('.form-group:eq(0)');

                    if (typeof form_group != typeof undefined) {
                        var error_message = '<p class="help-block validation-error">- ' + error + '</p>';
                        form_group.addClass('has-error').append(error_message);
                    }
                });
            });
        }

        function removeErrors(prefix)
        {
            $('.form-group.has-error.prefix_' + prefix).removeClass('has-error');
            $('.validation-error').remove();
        }

        function findValidationItem(prefix, name, callback)
        {
            var item = undefined;

            item = $('input[name="' + prefix + name + '"]');

            if (typeof item == typeof undefined || item == '' || item == null || item.prop('tagName') == undefined) {
                item = $('textarea[name="' + prefix + name + '"]');
            }

            if (typeof item == typeof undefined || item == '' || item == null || item.prop('tagName') == undefined) {
                item = $('select[name="' + prefix + name + '"]');
            }

            callback(item);
        }
    </script>
</head>
<!-- END HEAD -->

<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="#">
        {{--
                <img src="{{asset('assets/admin/layouts/layout2/img/logo-big-white.png')}}"  alt="" />
        --}}</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" id="login" action="javascript:void(0);" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-title">
            <span class="form-title">Hoş Geldiniz.</span>
            <span class="form-subtitle">Lütfen Giriş Yapınız.</span>
        </div>
        <div id="msg"></div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">E-posta Adresi</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="E-posta Adresi" name="email" /> </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Şifre</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Şifre" name="password" /> </div>
        <div class="form-actions">
            <button type="submit" class="btn red btn-block uppercase">Giriş</button>
        </div>

    </form>
    <!-- END LOGIN FORM -->
</div>
<div class="copyright hide"> 2016 © Cepte Diyetisyen</div>
<script src="{{asset('assets/admin/global/plugins/jquery.min.js')}}" type="text/javascript"></script>

<!--TODO: tek bir jss dosyası altında toparlanmalı -->
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
        });

        $('#login').submit(function () {
            $.ajax({
                url: '{{asset('backoffice/login')}}',
                data: $('#login').serialize(),
                type: 'POST',

                success: function (data) {
                    if (data.status){
                        location.href = '{{route('admin.dash')}}';
                    }else{
                        $('#msg').html("<div class='alert alert-danger'>"+data.message+"</div>")
                    }
                },
                error: function (data) {
                    validationErrors('Veri tabanına bağlanırken bir hata meydana geldi.',data.responseJSON);
                }
            });

            return false;
        });
    });
</script>
<!-- END LOGIN -->
<!--[if lt IE 9]>
<script src="{{asset('assets/admin/global/plugins/respond.min.js')}}"></script>
<script src="{{asset('assets/admin/global/plugins/excanvas.min.js')}}"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->

<script src="{{asset('assets/admin/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{asset('assets/admin/global/scripts/app.min.js')}}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('assets/admin/pages/scripts/login.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>