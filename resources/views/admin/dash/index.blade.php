@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 80px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$user_count}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>HASTA</small>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 80px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup"
                                  data-value="@if(is_int($premium)) {{$premium}} @else 0 @endif">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>PREMIUM HASTA</small>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 80px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$codes}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>ÖZEL HASTA</small>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 80px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup"
                                  data-value="@if(is_int($diet_list)){{$diet_list}} @else 0 @endif">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>DİYET LİSTESİ</small>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 80px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$comments}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>YORUMSUZ</small>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 80px;">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$diet}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>LİSTE BEKLEYEN</small>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label" for="single">Yorumlanan Öğünler</label>
                <canvas id="myChart" width="997px" height="200px">
                    @foreach($commentByM as $i)
                        <div class="logs">{!! $i !!}</div>
                    @endforeach
                </canvas>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-md-5">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-green-sharp bold uppercase">
                            Cevap Bekleyen 10 öğün
                        </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <ul class="list-group">
                        @foreach($commentsByR as $meal)
                            <li class="list-group-item">
                                <a href="{{route('admin.meals.chat')}}/{{$meal->id}}">
                                    <img src="{{$meal->pic}}" width="50"/>
                                    <small>
                                        #{{$meal->user->id}}</small> {{$meal->user->first_name}} {{$meal->user->last_name}}  {{$meal->description}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('script')
    <script src="{{asset('assets/admin/global/scripts/Chart.min.js')}}" type="text/javascript"></script>
    <script>
        var ctx = document.getElementById("myChart");
        var DATA = [];
        function deneme() {
            var elems = document.getElementsByClassName("logs");
            for (elem of elems) {
                DATA.push(elem.innerHTML);
            }
            console.log(DATA);
        }
        deneme();

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: DATA,
                datasets: [{
                    label: 'Yorumlanan Öğün Sayısı',
                    data: DATA,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
        });
    </script>


@endsection