@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Diyetisyen Detayı</span>
                    </div>

                </div>
                <div class="portlet-body">

                    <!-- BEGIN FORM-->

                    <form action="{{action('Panel\DetailController@postIndex')}}" method="post" class="horizontal-form">
                       @if(isset($detail['id'])) <input type="hidden" id="id" value="{{$detail['id']}}"> @endif
                    <div class="form-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Başlık</b></label>
                                    <input type="text" name="title" class="form-control" placeholder="" value="{{$detail['title']}}" >
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Video</b></label>
                                    <input type="text" name="video" class="form-control" placeholder="" value="{{$detail['video']}}" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Eğitim</b></label>
                                    <input type="text" name="egitim" class="form-control" placeholder="" value="{{$detail['information']}}" >

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Açıklama</b></label>
                                    <textarea class="form-control" name="description" rows="3" >{{$detail['description']}}</textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Instagram Hesabı</b></label>
                                    <input type="text" name="instagram" class="form-control" placeholder="" value="{{$detail['instagram']}}" >

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Facebook Hesabı</b></label>
                                    <input type="text" name="facebook" class="form-control" placeholder="" value="{{$detail['facebook']}}" >

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Twitter Hesabı</b></label>
                                    <input type="text" name="twitter" class="form-control" placeholder="" value="{{$detail['twitter']}}" >

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>LinkedIn Hesabı</b></label>
                                    <input type="text" name="linkedin" class="form-control" placeholder="" value="{{$detail['linkedin']}}" >

                                </div>
                            </div>



                        </div>
                        <!--/row-->
                    </div>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Kaydet
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


                    <!-- END FORM-->

                    <style>
                        .orderRow td:first-child{
                            display:none;
                        }

                        .orderRow  td:nth-child(3) {
                            text-align: center;
                        }
                        .table th:first-child{
                            display:none;
                        }
                    </style>
        <!-- END EXAMPLE TABLE PORTLET-->
@endsection