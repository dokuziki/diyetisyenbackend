@extends('admin.layouts.master')
@section('content')

    <style type="text/css">
        textarea {
            resize: none;
        }

    </style>

    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->


                <div class="portlet light ">

                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">{{$item->users['first_name']}} {{$item->users['last_name']}}</span>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <form action="{{asset('backoffice/dietlist/upsert')}}@if(isset($item['id']))/{{$item['id']}}@endif"
                              method="post" class="horizontal-form">
                            <input type="hidden" name="id" @if(isset($item['id'])) value="{{$item['id']}}"
                                   @else value="false" @endif />
                            <div class="form-body">
                                <h3 class="form-section">{{$item['purpose']}}</h3>
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label"><b>Tarih</b></label>
                                            <input type="date" name="date" class="form-control"
                                                   placeholder=""
                                                   value="{{$item['date']}}"
                                                   autocomplete="new-password"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <!--<h2 class="ustoge2"><a class="btn btn-default">Yorum Yap</a></h2>-->
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Diyet Yapma Sebebi</b></label>
                                                <textarea name="purpose" class="form-control"
                                                          rows="3">@if(isset($item['purpose'])){{$item['purpose']}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <!--<h2 class="ustoge2"><a class="btn btn-default">Yorum Yap</a></h2>-->
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Diyet Başlığı</b></label>
                                                <textarea name="title" class="form-control"
                                                          rows="3">@if(isset($item['title'])){{$item['title']}} @endif</textarea>
                                            </div>
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Açıklama</b></label>
                                                <textarea name="description" class="form-control"
                                                          rows="3">@if(isset($item['description'])){{$item['description']}} @endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Sabah</b></label>
                                                <textarea name="sabah" class="form-control"
                                                          rows="3">@if(isset($item['sabah'])){{$item['sabah']}} @endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Kahvaltı</b></label>
                                                <textarea name="kahvalti" class="form-control"
                                                          rows="3">@if(isset($item['kahvalti'])){{$item['kahvalti']}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Ara Öğün 1</b></label>
                                                <textarea name="ara_ogun1" class="form-control"
                                                          rows="3">@if(isset($item['ara_ogun1'])){{$item['ara_ogun1']}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Öğle</b></label>
                                                <textarea name="ogle" class="form-control"
                                                          rows="3">@if(isset($item['ogle'])){{$item['ogle']}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Ara Öğün 2</b></label>
                                                <textarea name="ara_ogun2" class="form-control"
                                                          rows="3">@if(isset($item['ara_ogun2'])){{$item['ara_ogun2']}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Akşam</b></label>
                                                <textarea name="aksam" class="form-control"
                                                          rows="3">@if(isset($item['aksam'])){{$item['aksam']}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Ara Öğün 3</b></label>
                                                <textarea name="ara_ogun3" class="form-control"
                                                          rows="3">@if(isset($item['ara_ogun3'])){{$item['ara_ogun3']}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <div class="icerik2">
                                                <label for="single" class="control-label"><b>Notlar</b></label>
                                                <textarea name="notlar" class="form-control"
                                                          rows="3">@if(isset($item['notlar'])){{$item['notlar']}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <!--/row-->

                                <div class="row">

                                    <!--/span-->

                                </div>
                            </div>
                            <br>
                            <div class="form-actions right">
                                <button type="submit" class="btn blue">
                                    <i class="fa fa-check"></i> Onayla
                                </button>
                            </div>
                        </form>
                        <!-- END FORM-->

                        <style>
                            .orderRow td:first-child {
                                display: none;
                            }

                            .orderRow td:nth-child(3) {
                                text-align: center;
                            }

                            .table th:first-child {
                                display: none;
                            }
                        </style>

                        <br>
                        <br>


                    </div>
                </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

        <div class="col-md-6">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="glyphicon glyphicon-info-sign"></i>
                        <span class="caption-subject bold uppercase"> Sağlık Bilgileri</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="menu">
                            <div class="icerik">
                                <div class="form-group">
                                    <label class="control-label" for="single">Kilo Grafiği</label>
                                    <canvas id="myChart" width="100%" height="35px">
                                        @foreach($info['weightLogs'] as $log)
                                            <div class="logs">{{$log->weight}}</div>
                                        @endforeach
                                    </canvas>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Cinsiyet</label>
                                        <input type="text" name="email" class="form-control" placeholder=""
                                               @if($info['gender'] == 0)value="Bay" @elseif($info['gender'] == 1) value="Bayan" @else value="Belirtilmemiş" @endif disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Boy</label>
                                        <input type="text" name="email" class="form-control" placeholder=""
                                               value="{{$info['details']['height']}}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Hedef Kilo</label>
                                        <input type="text" name="email" class="form-control" placeholder=""
                                               value="{{$info['details']['target_weight']}}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Yaş</label>
                                        <?php
                                        use Carbon\Carbon;
                                        $year = date_parse($info['birthday']);
                                        $now = Carbon::now()->format('Y');
                                        $birthday = $now - $year['year'];
                                        ?>
                                        <input type="text" name="email" class="form-control" placeholder="" @if($info['birthday'] !== null) value="{{$birthday}}" @endif disabled required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Yemediğiniz ve/veya
                                                alerjiniz olan
                                                besinler var mı ? </b></label>
                                        <textarea class="form-control" rows="4"
                                                  disabled>{{$info['details']['q1']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Kabızlık problemi yaşıyor
                                                musunuz
                                                ?</b></label>
                                        <textarea class="form-control" rows="4"
                                                  disabled>{{$info['details']['q2']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Sürekli olarak kullanmakta
                                                olduğunuz ilaçlar var mı ? </b></label>
                                        <textarea class="form-control" rows="4"
                                                  disabled>{{$info['details']['q3']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Sağlık durumunuz veya
                                                belirtmek
                                                istediğinz bir hastalığınız var mı ?</b></label>
                                        <textarea class="form-control" rows="4"
                                                  disabled>{{$info['details']['q4']}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>


        </div>

        @endsection

        @section('script')
            <script src="{{asset('assets/admin/global/scripts/Chart.min.js')}}" type="text/javascript"></script>
            <script>
                var ctx = document.getElementById("myChart");
                var DATA = [];
                function deneme() {
                    var elems = document.getElementsByClassName("logs");
                    for (elem of elems) {
                        DATA.push(elem.innerHTML);
                    }
                    console.log(DATA);
                }
                deneme();

                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: DATA,
                        datasets: [{
                            label: 'Kilo Grafiği',
                            data: DATA,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 2
                        }]
                    },
                });
            </script>
@endsection