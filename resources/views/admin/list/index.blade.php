@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Diyet Listesi Bekleyenler</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                               id="sample_3">
                            <thead>
                            <tr>
                                <th> Kullanıcı ID</th>
                                <th> Hasta</th>
                                <th> Açıklama</th>
                                <th> Durum</th>
                                <th> İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($diets as $diet)
                                <tr class="odd gradeX">
                                    <td>{{$diet->users->id}}</td>

                                    <td> {{$diet->users->first_name}} {{$diet->users->last_name}}</td>
                                    <td> {{$diet->purpose}}</td>
                                    <td>
                                        @if($diet['status'] == 0)
                                            <span class="label label-sm label-danger"> İnaktif </span>
                                        @else
                                            <span class="label label-sm label-success"> Aktif </span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{asset('backoffice/dietlist/upsert')}}/{{$diet->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i>Düzenle</a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Diyet Listesi Onaylananlar</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column"
                               id="sdgfsfdsf">
                            <thead>
                            <tr>
                                <th> Kullanıcı ID</th>
                                <th> Hasta</th>
                                <th> Açıklama</th>
                                <th> Durum</th>
                                <th> Verildiği Tarih</th>
                                <th> İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lists as $list)
                                <tr class="odd gradeX">
                                    <td>{{$list->users->id}}</td>
                                    <td> {{$list->users->first_name}} {{$list->users->last_name}}</td>
                                    <td> {{$list->purpose}}</td>
                                    <td>
                                        @if($list['status'] == 0)
                                            <span class="label label-sm label-danger"> İnaktif </span>
                                        @else
                                            <span class="label label-sm label-success"> Aktif </span>
                                        @endif
                                    </td>
                                    <td>
                                        {{$list->date}}
                                    </td>
                                    <td>
                                        <a href="{{asset('backoffice/dietlist/upsert')}}/{{$list->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i>Düzenle</a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#sdgfsfdsf').DataTable( {
                "order": [[ 3, "desc" ]]
            } );
        });
    </script>

@endsection