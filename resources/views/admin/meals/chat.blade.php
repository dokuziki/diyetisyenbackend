@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-hide hide"></i>
                        <span class="caption-subject font-hide bold uppercase">Yorum Bekleyen Öğün </span>
                        <a href="{{route('admin.meals.closeChat')}}?id={{$chat['id']}}"
                           class="label label-sm label-danger" style="margin-left: 30px"> Konuşmayı Sonlandır </a>
                    </div>
                </div>
                <div class="portlet-body" id="chats">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible1="1">
                        <ul class="chats">
                            @foreach(collect($chat->comments()->with(['user'])->orderBy('id','DESC')->get())->reverse() as $message)
                                <li class="@if($chat->user_id === $message->user_id) out @else in @endif">
                                    <?php $us = \App\Models\User::find($message->user_id);
                                    ?>
                                    <img class="avatar" alt="" src="{{$us->pic}}"/>
                                    <div class="message">
                                        <span class="arrow"> </span>
                                        <a href="javascript:;"
                                           class="name"> {{$message->user->first_name}} {{$message->user->last_name}}</a>
                                        <span class="datetime"> {{$message->created_at}} </span>
                                        <span class="body"> {{$message->comment}} </span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <form action="{{asset('backoffice/meal/comment')}}@if(isset($chat['id']))/{{$chat['id']}}@endif"
                          method="post" class="horizontal-form">
                        <input type="hidden" class="pull-left" name="id" @if(isset($chat['id'])) value="{{$chat['id']}}"
                               @else value="false" @endif />
                        <div class="row">
                            <div class="col-sm-12">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input type="text" name="comments" class="form-control"
                                               placeholder="Yorumunuzu yazın...">
                                      <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Gönder!</button>
                                      </span>
                                    </div><!-- /input-group -->
                                </div>
                            </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="portlet-body">
                        <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                               id="sample_1">
                            <thead>
                            <tr>
                                <th> ID</th>
                                <th> Öğün</th>
                                <th> Gönderen</th>
                                <th> İşlem</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr class="odd gradeX">
                                    <td> {{$item->id}}</td>
                                    <td>
                                        <img src="{{$item->pic}}" width="50"/>
                                    </td>
                                    <td>
                                        {{$item->user->first_name}} {{$item->user->last_name}}
                                    </td>
                                    <td>
                                        <a href="{{route('admin.meals.chat')}}/{{$item->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i>Cevapla</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>



    <script>
        $(document).ready(function () {

            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });

            var body = $('body');


            body.on('click', '.deleteOpenModal', function (event) {
                $('#deleteConfirm').modal('show');
                var dataID = $(this).attr('data-id');

                var button = $(this);
                var deleted = $(this).attr('data-deleted');

                $('.delete').click(function () {
                    window.location = '#' + '/' + dataID;
                });

            });
        });


    </script>
@endsection