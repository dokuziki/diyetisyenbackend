@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Öğünler</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="ozel_table" class="table table-striped table-bordered table-hover table-checkable order-column"
                        >
                            <?php
                            use Illuminate\Support\Facades\Request;
                            $current = Request::fullUrl();
                            if(!strpos($current, '?')){
                                $current .= '?';
                            }else{

                                if(strpos($current, 'order=')){
                                    $search = 'order='.\Illuminate\Support\Facades\Input::get('order');

                                    $current = str_replace($search,'',$current);
                                }

                                $current .= '&';
                            }
                            ?>
                            <thead>
                            <tr>
                                <th> Kullanıcı ID</th>
                                <th>Adı Soyadı</th>
                                <th> Açıklama</th>
                                <th> Durumu</th>
                                <th>Tarih</th>
                                <th> İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($meals as $meal)
                                <tr class="odd gradeX">
                                    <td> {{$meal->user->id}}</td>

                                    <td> {{$meal->user->first_name}} {{$meal->user->last_name}}</td>
                                    <td> {{$meal['desc']}}</td>
                                    <td>
                                        @if($meal['approved'] == 1)
                                            <span class="label label-sm label-success"> Onaylandı </span><br>
                                        @else
                                            <span class="label label-sm label-danger"> Onaylanmadı </span><br>
                                        @endif
                                            <?php
                                            $flag = false;
                                            $diyetci = \Illuminate\Support\Facades\Auth::user();

                                            foreach ($meal->comments as $comment){
                                                if($comment->user_id === $diyetci->id){
                                                    $flag = true;
                                                }
                                            }
                                            ?>
                                            @if($flag)
                                               <br> <span class="label label-sm label-success"> Yorumlandı </span>
                                            @else
                                               <br> <span class="label label-sm label-danger"> Yorumlanmadı </span>
                                            @endif
                                    </td>
                                    <td>{{$meal->created_at}}</td>
                                    <td>
                                        <a href="{{asset('backoffice/meal/upsert')}}/{{$meal->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i>Öğün Detayı</a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row ">
                        <div class="col-xs-12">
                           <center>
                               <?php
                                $linkArr = ['type' => $type];

                                if(Input::get('order', false)){
                                    $linkArr['order'] = \Illuminate\Support\Facades\Input::get('order');
                                }
                               ?>

                           </center>
                        </div>
                    </div>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
</div>

    <style>
        .number {
            list-style-type: none;
            float: right;
            font-family: 'Roboto Condensed', sans-serif;
            font-size: 20px;

        }

        .number li {
            display: block;
            float: left;
            text-align: center;
            border: 1px solid rgba(38, 38, 38, 0.44);
            padding: 5px;
            height: 40px;
            margin-left: 5px;
        }

        .number li a {
            padding: 5px;

        }

        .number .paginate {
            border: 0;
            font-family: 'Roboto Condensed', sans-serif;
            color: #4f4f4f;
            padding-top: 10px;
            font-size: 16px;
        }

        .number li span {
            padding: 5px;
            color: grey;

        }

    </style>


    <script>
        $(document).ready(function () {
            $(document).ready(function() {
                $('#ozel_table').DataTable( {
                    order: [[ 4, 'desc' ]]
                } );
            } );
            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });

            var body = $('body');



            body.on('click','.deleteOpenModal',function(event){
                $('#deleteConfirm').modal('show');
                var dataID = $(this).attr('data-id');

                var button = $(this);
                var deleted = $(this).attr('data-deleted');

                $('.delete').click(function (){
                    window.location= '#'+'/'+dataID;
                });

            });
        });


    </script>
        @endsection