@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Premium/Özel Öğünler</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                <h2>Premium Üye Öğünleri</h2>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="ozel_table" class="table table-striped table-bordered table-hover table-checkable order-column">
                            <thead>
                            <tr>
                                <th> Kullanıcı ID</th>
                                <th> Adı Soyadı</th>
                                <th> Açıklama</th>
                                <th> Durumu</th>
                                <th> İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($premiums as $premium)
                                <tr class="odd gradeX">
                                    <td> {{$premium->user->id}}</td>
                                    <td> {{$premium->user->first_name}} {{$premium->user->last_name}}</td>
                                    <td> {{$premium['desc']}}</td>
                                    <td>
                                        @if($premium['approved'] == 1)
                                            <span class="label label-sm label-success"> Onaylandı </span><br>
                                        @else
                                            <span class="label label-sm label-danger"> Onaylanmadı </span><br>
                                        @endif
                                        <?php
                                        $flag = false;
                                        $diyetci = \Illuminate\Support\Facades\Auth::user();

                                        foreach ($premium->comments as $comment){
                                            if($comment->user_id === $diyetci->id){
                                                $flag = true;
                                            }
                                        }
                                        ?>
                                        @if($flag)
                                            <br> <span class="label label-sm label-success"> Yorumlandı </span>
                                        @else
                                            <br> <span class="label label-sm label-danger"> Yorumlanmadı </span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{asset('backoffice/meal/upsert')}}/{{$premium->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i>Öğün Detayı</a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>

                    <h2> Özel Kod Kullanan Üye Öğünleri</h2>
                    <div class="portlet-body">
                        <table class="table  table-striped table-bordered table-hover table-checkable order-column"
                               id="sample_1">
                            <thead>
                            <tr>
                                <th> Kullanıcı ID</th>
                                <th> Adı Soyadı</th>
                                <th> Açıklama</th>
                                <th> Durum</th>
                                <th> Yorum Durumu</th>
                                <th> İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($codes as $code)
                                <tr class="odd gradeX">
                                    <td> {{$premium->user->id}}</td>
                                    <td> {{$code->user->first_name}} {{$code->user->last_name}}</td>
                                    <td> {{$code['desc']}}</td>
                                    <td>
                                        @if($code['approved'] == 1)
                                            <span class="label label-sm label-success"> Onaylandı </span>
                                        @else
                                            <span class="label label-sm label-danger"> Onaylanmadı </span>
                                        @endif
                                    </td>

                                    <td>
                                        <?php
                                        $flag = false;
                                        $diyetci = \Illuminate\Support\Facades\Auth::user();

                                        foreach ($code->comments as $comment){
                                            if($comment->user_id === $diyetci->id){
                                                $flag = true;
                                            }
                                        }
                                        ?>
                                        @if($flag)
                                            <span class="label label-sm label-success"> Yorumlandı </span>
                                        @else
                                            <span class="label label-sm label-danger"> Yorumlanmadı </span>
                                        @endif
                                    </td>

                                    <td>
                                        <a href="{{asset('backoffice/meal/upsert')}}/{{$code->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i>Öğün Detayı</a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(document).ready(function () {
                $('#ozel_table').DataTable({
                    order: [[4, 'desc']]
                });
            });
            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });
        });
    </script>
@endsection
