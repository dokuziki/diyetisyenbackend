@extends('admin.layouts.master')
@section('content')
    <style type="text/css">
        textarea {
            resize: none;
        }

        .modal-header {
            padding: 5px 10px;
            background-color: #61d6ae;
            color: #000000;
        }

        /* Modal Body */
        .modal-body {padding: 2px 16px;}

        /* Modal Footer */

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #ffffff;
            width: 50%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(185, 255, 91, 0);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {top: -200px; opacity: 0}
            to {top: 0; opacity: 1}
        }

        @keyframes animatetop {
            from {top: -200px; opacity: 0}
            to {top: 0; opacity: 1}
        }
        .modal_open{
            cursor:pointer;
        }
    </style>

    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            @if(count($items) === 0)
                <div class="portlet light ">

                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="glyphicon glyphicon-cutlery"></i>
                            <span class="caption-subject bold uppercase">Tebrikler</span> <br>
                            Bu kullanıcının yorum bekleyen öğünü kalmadı.
                        </div>

                    </div>
                </div>
            @endif
            @foreach($items as $item)
                <div class="portlet light ">

                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="glyphicon glyphicon-cutlery"></i>
                            <span class="caption-subject bold uppercase">{{$item['time']['name']}}</span>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <form action="{{asset('backoffice/meal/upsert')}}@if(isset($item['id']))/{{$item['id']}}@endif"
                              method="post" class="horizontal-form">
                            <input type="hidden" name="id" @if(isset($item['id'])) value="{{$item['id']}}"
                                   @else value="false" @endif />

                            <div class="form-body">
                                <h3 class="form-section">{{$item['desc']}}</h3>
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-4">
                                        <div class="form-group">
                                        </div>
                                        @if(isset($item['pic']) && $item['pic'] !== '' &&  $item['pic'] !== asset('uploads/meals/'))
                                            <img height="150" src="{{$item['pic']}}"/>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Tarih</label>
                                            <input type="text" name="last_name" class="form-control"
                                                   placeholder=""
                                                   value="{{$item['date']}}" disabled
                                                   autocomplete="new-password"
                                                   required>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="single">Paylaşan Kişi</label>
                                            <input type="text" name="email" class="form-control"
                                                   value="{{$item['user']['first_name']}} {{$item['user']['last_name']}}"
                                                   disabled required>
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="single">Öğün Vakti</label>
                                            <input type="text" name="meal_time" class="form-control"
                                                   value="{{$item['time']['name']}}" disabled
                                                   autocomplete="new-password">
                                        </div>
                                    </div>


                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="single">Onay Durumu</label>
                                            <select name="status" class="form-control " disabled>
                                                <option value="1" @if($item['approved'] === 1) selected @endif>
                                                    Onaylandı
                                                </option>
                                                <option value="0" @if($item['approved'] === 0) selected @endif>
                                                    Onaylanmadı
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="single">Puan</label>
                                            <select id="point" name="point" class="form-control ">
                                                <option value="0" @if($item['point'] === 0) selected @endif>0
                                                </option>
                                                <option value="1" @if($item['point'] === 1) selected @endif>1
                                                </option>
                                                <option value="2" @if($item['point'] === 2) selected @endif>2
                                                </option>
                                                <option value="3" @if($item['point'] === 3) selected @endif>3
                                                </option>
                                                <option value="4" @if($item['point'] === 4) selected @endif>4
                                                </option>
                                                <option value="5" @if($item['point'] === 5) selected @endif>5
                                                </option>
                                                <option value="6" @if($item['point'] === 6) selected @endif>6
                                                </option>
                                                <option value="7" @if($item['point'] === 7) selected @endif>7
                                                </option>
                                                <option value="8" @if($item['point'] === 8) selected @endif>8
                                                </option>
                                                <option value="9" @if($item['point'] === 9) selected @endif>9
                                                </option>
                                                <option value="10" @if($item['point'] === 10) selected @endif>10
                                                </option>

                                            </select>
                                        </div>
                                    </div>

                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="menu">
                                            <!--<h2 class="ustoge2"><a class="btn btn-default">Yorum Yap</a></h2>-->
                                            <div class="icerik2">
                                                <label for="single" class="control-label">Yorum Alanı</label>
                                                        <textarea name="comment" class="form-control"
                                                                  rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->

                                <div class="row">

                                    <!--/span-->

                                </div>
                            </div>
                            <br>
                            <div class="form-actions right">
                                <button type="submit" class="btn blue">
                                    <i class="fa fa-check"></i> Onayla
                                </button>
                            </div>
                        </form>
                        <!-- END FORM-->

                        <style>
                            .orderRow td:first-child {
                                display: none;
                            }

                            .orderRow td:nth-child(3) {
                                text-align: center;
                            }

                            .table th:first-child {
                                display: none;
                            }
                        </style>

                        <br>
                        <br>


                    </div>
                </div>
            @endforeach

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

        <div class="col-md-6">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="glyphicon glyphicon-info-sign"></i>
                        <span class="caption-subject bold uppercase"> Satın Alma Bilgileri</span>
                    </div>

                </div>


                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <b>Diyet Listesi</b><br>
                            @if( $payments['list'] !== null || count($lists)>0)
                                    @foreach($lists as $i => $list)
                                        <br>
                                    <span href="myModal{{$list->id}}" class="modal_open"> Liste {{$i + 1}} : {{$list->created_at}} </span>

                                    <div id="myModal{{$list->id}}" class="modal">

                                        <!-- Modal content -->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span class="close">&times;</span>
                                                <h2>{{$list->title}}</h2>
                                            </div>
                                            <div class="modal-body">
                                                <label class="control-label" for="single"> <b>Veriliş Tarihi</b> </label>
                                                <input type="text" name="email" class="form-control" value="@if($list->date&&$list->date !== null){{$list->date}}@endif" disabled>

                                                <label class="control-label" for="single"><b>Diyet Yapma Amacı</b></label>
                                                <textarea class="form-control" rows="2"
                                                          disabled>@if($purpose['purpose'] == 0) Kilo Vermek İstiyorum @elseif($purpose['purpose'] == 1) Mevcut Kilomu Korumak İstiyorum @elseif($purpose['purpose'] == 2) Kilo Almak İstiyorum @endif</textarea>

                                                <label class="control-label" for="single"><b>Sabah</b></label>
                                                <textarea class="form-control" disabled rows="5">@if($list->sabah&&$list->sabah !== null){{$list->sabah}} @endif</textarea>

                                                <label class="control-label" for="single"><b>Kahvaltı</b></label>
                                                <textarea class="form-control" disabled rows="5">@if($list->kahvalti&&$list->kahvalti !== null){{$list->kahvalti}}@endif</textarea>

                                                <label class="control-label" for="single"><b>Ara Öğün</b></label>
                                                <textarea class="form-control" disabled rows="5">@if($list->ara_ogun1&&$list->ara_ogun1 !== null){{$list->ara_ogun1}}@endif</textarea>

                                                <label class="control-label" for="single"><b>Öğle</b></label>
                                                <textarea class="form-control" disabled rows="5">@if($list->ogle&&$list->ogle !== null){{$list->ogle}}@endif</textarea>

                                                <label class="control-label" for="single"><b>Ara Öğün</b></label>
                                                <textarea class="form-control" disabled rows="5">@if($list->ara_ogun2&&$list->ara_ogun2 !== null){{$list->ara_ogun2}}@endif</textarea>

                                                <label class="control-label" for="single"><b>Akşam </b></label>
                                                <textarea class="form-control" disabled rows="5">@if($list->aksam&&$list->aksam !== null){{$list->aksam}}@endif</textarea>

                                                <label class="control-label" for="single"><b>Ara Öğün</b></label>
                                                <textarea class="form-control" disabled rows="5" disabled>@if($list->ara_ogun3&&$list->ara_ogun3 !== null){{$list->ara_ogun3}}@endif</textarea>

                                                <label class="control-label" for="single"><b>Notlar</b></label>
                                                <textarea class="form-control" disabled rows="5" disabled>@if($list->notlar&&$list->notlar !== null) {{$list->notlar}}@endif </textarea><br>



                                            </div>
                                        </div>

                                    </div>
                                    @endforeach
                            @else
                                Hiç alınmamış
                            @endif

                            <hr>


                            <b>Online Sıkı Takip</b> <br>
                            @if( $payments['premium'] !== null)
                                Satın alma
                                tarihi: {{\Carbon\Carbon::createFromFormat('Y-m-d',$payments['premium']->start_date)->format('d-m-Y')}}
                                <br>
                                Yenilenme
                                tarihi: {{\Carbon\Carbon::createFromFormat('Y-m-d',$payments['premium']->end_date)->format('d-m-Y')}}
                            @else
                                Hiç alınmamış
                            @endif
                            <br><br>
                        </div>

                    </div>
                </div>

                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="glyphicon glyphicon-info-sign"></i>
                        <span class="caption-subject bold uppercase"> Son 10 Gün Aktivite Kaydı</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list-group">
                                @foreach($water as $activity)
                                    <li class="list-group-item">
                                        <span class="badge badge-alert"> Su Miktarı: {{$activity->water}} bardak</span>
                                        {{$activity->activity}}<br>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>


                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="glyphicon glyphicon-info-sign"></i>
                        <span class="caption-subject bold uppercase"> Sağlık Bilgileri</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="menu">
                            {{--
                                                <h2 class="ustoge"><a class="btn btn-danger">Sağlık Bilgileri</a></h2>
                            --}}

                            <div class="icerik">
                                <div class="form-group">
                                    <label class="control-label" for="single">Kilo Grafiği</label>
                                    <canvas id="myChart" width="100%" height="35px">
                                        @foreach($info['weightLogs'] as $log)
                                            <div class="logs">{{$log->weight}}</div>
                                            <div class="dates">{{$log->created_at}}</div>

                                        @endforeach
                                    </canvas>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Diyet Yapma Amacı</b></label>
                                        <textarea class="form-control" rows="4"
                                                  disabled>@if($purpose['purpose'] == 0) Kilo Vermek İstiyorum @elseif($purpose['purpose'] == 1) Mevcut Kilomu Korumak İstiyorum @elseif($purpose['purpose'] == 2) Kilo Almak İstiyorum @endif</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Biyografi</b></label>
                                        <textarea class="form-control" rows="4"
                                                  disabled>@if(isset($info['bio']) && $info['bio'] !== null){{$info['bio']}}@endif</textarea>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Cinsiyet</label>
                                        <input type="text" name="email" class="form-control" placeholder=""
                                               @if($info['gender'] == 0)value="Bay" @elseif($info['gender'] == 1) value="Bayan" @else value="Belirtilmemiş" @endif disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Boy</label>
                                        <input type="text" name="email" class="form-control" placeholder=""
                                               value="{{$info['details']['height']}}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Hedef Kilo</label>
                                        <input type="text" name="email" class="form-control" placeholder=""
                                               value="{{$info['details']['target_weight']}}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Yaş</label>
                                        <?php
                                        use Carbon\Carbon;$year = date_parse($info['birthday']);
                                        $now = Carbon::now()->format('Y');
                                        $birthday = $now - $year['year'];
                                        ?>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($info['birthday'])) value="{{$birthday}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Rumuz</label>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($info['nickname'])) value="{{$info['nickname']}}" @endif disabled>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Yemediğiniz ve/veya
                                                alerjiniz olan
                                                besinler var mı ? </b></label>
                                        <textarea class="form-control" rows="4"
                                                  disabled>{{$info['details']['q1']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Kabızlık problemi yaşıyor
                                                musunuz
                                                ?</b></label>
                                        <textarea class="form-control" rows="4"
                                                  disabled>{{$info['details']['q2']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Sürekli olarak kullanmakta
                                                olduğunuz ilaçlar var mı ? </b></label>
                                        <textarea class="form-control" rows="4"
                                                  disabled>{{$info['details']['q3']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Sağlık durumunuz veya
                                                belirtmek
                                                istediğinz bir hastalığınız var mı ?</b></label>
                                        <textarea class="form-control" rows="4"
                                                  disabled>{{$info['details']['q4']}}</textarea>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>

                @if(count($comments) > 0)
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="glyphicon glyphicon-info-sign"></i>
                            <span class="caption-subject bold uppercase"> Yorum Yapılmayan Öğünler</span>
                        </div>
                    </div>

                        <div class="portlet-body">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Görsel</th>
                                    <th>Öğün Açıklaması</th>
                                    <th>Yorum Alanı</th>
                                    <th>İşlem</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($comments as $comment)
                                    <form action="{{action('Panel\MealController@postComment')}}" method="post"
                                          class="horizontal-form">
                                        <input type="hidden" name="id" value="{{$comment['id']}}" value="false"/>
                                        <tr class="odd gradeX">
                                            <td><a href="{{$comment['pic']}}" target="_blank">
                                                    <img src="{{$comment['pic']}}" style="width: 100%"/>
                                                </a>
                                            </td>
                                            <td>{{$comment['desc']}}</td>
                                            <td><textarea name="comments" id="comments" class="comments"></textarea>
                                            </td>
                                            <td>
                                                <div class="form-actions right">
                                                    <button type="submit" class="btn blue">
                                                        <i class="fa fa-check"></i> Yorum Yap
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </form>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
                @endif


            </div>


        </div>

        @endsection

        @section('script')
            <script src="{{asset('assets/admin/global/scripts/Chart.min.js')}}" type="text/javascript"></script>
            <script>
                var ctx = document.getElementById("myChart");
                var DATA = [];
                var DATES = [];

                function deneme() {
                    var elems = document.getElementsByClassName("logs");
                    for (elem of elems) {
                        DATA.push(elem.innerHTML);
                    }
                    console.log(DATA);
                }

                function denemes(){
                    var elems = document.getElementsByClassName("dates");
                    for(elem of elems) {
                        DATES.push(elem.innerHTML);
                    }
                    console.log(DATES);
                }
                deneme();
                denemes();

                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: DATES,
                        datasets: [{
                            label: 'Kilo Grafiği',
                            data: DATA,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 2
                        }]
                    },
                });
            </script>

            <script>

                /*$(".icerik").hide();
                 $("h2.ustoge").click(function(){
                 $(this).toggleClass("active").next().slideToggle(750);
                 });
                 */

                /* $(".icerik2").hide();
                 $("h2.ustoge2").click(function(){
                 $(this).toggleClass("active").next().slideToggle(750);
                 });*/

                $(document).ready(function () {
                    $('.modal_open').on('click',function (e) {
                       e.preventDefault();
                       $('#'+$(this).attr('href')).css('display','block');
                    });

                    $('.modal .close').on('click',function (e) {
                        e.preventDefault();
                        $(this).parents('.modal').css('display','none');
                    });

                    $(window).on('click',function (e) {

                        if($(e.target).attr('class')==='modal')
                            $('.modal').css('display','none');


                    });
                });


                /*
                // Get the modal
                var modal = document.getElementById('myModal');

                // Get the button that opens the modal
                var btn = document.getElementById("myBtn");

                // Get the <span> element that closes the modal

                // When the user clicks on the button, open the modal
                btn.onclick = function() {
                    modal.style.display = "block";
                }

                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                    modal.style.display = "none";
                }

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                        return false;
                    }
                }*/

            </script>


@endsection