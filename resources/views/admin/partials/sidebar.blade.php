<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start">
                <a href="{{route('admin.dash')}}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Anasayfa</span>
                </a>
                <!-- Sub menu example
                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="#" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Dashboard 1</span>
                        </a>
                    </li>
                </ul>
                Sub Menu End -->
            </li>

            <li class="nav-item start">
                <a href="{{route('admin.userinfo.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-info-sign"></i>
                    <span class="title">Hasta Bilgileri</span>
                </a>
            </li>
            <li class="nav-item start">
                <a class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-cutlery"></i>
                    <span class="title">Öğünler</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="{{route('admin.meals.premium')}}">Premium/Özel Öğünler</a></li>
                    <li><a href="{{route('admin.meals.index')}}?type=1">Genel Öğünler</a></li>
                    <li><a href="{{route('admin.meals.index')}}">Tüm Öğünler</a></li>
                    <li><a href="{{route('admin.meals.chat')}}">Cevap bekleyenler</a></li>
                </ul>
            </li>
            <li class="nav-item start">
                <a href="{{route('admin.codes.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-barcode"></i>
                    <span class="title">Kod Oluştur</span>
                </a>
            </li>

            <li class="nav-item start">
                <a href="{{route('admin.list.index')}}" class="nav-link nav-toggle">
                    <i class="fa fa-list"></i>
                    <span class="title">Diyet Listesi Hazırlama</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="{{route('admin.detail.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-user"></i>
                    <span class="title">Detay</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<script>
    $(document).ready(function () {

        if ($("a[href='{{Request::url()}}']").length > 0)
            $("a[href='{{Request::url()}}']").append('<span class="selected"></span><span class="arrow open"></span>').parents('li').addClass('active');
    });


</script>