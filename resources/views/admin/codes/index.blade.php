@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Oluşturulan Kodlar </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <form action="{{asset('backoffice/codes/index')}}" method="post" class="horizontal-form">
                                    <input type="hidden" name="id" value="false"/>

                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Üretilecek Kod Adeti</label>
                                                    <input type="text" name="code_count" class="form-control"required>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-actions right">
                                        <button type="submit" class="btn blue">
                                            <i class="fa fa-check"></i> Yeni Kod Oluştur
                                        </button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                               id="sample_1">
                            <thead>
                            <tr>
                                <th> Kod</th>
                                <th> Durum</th>
                                <th> Oluşturma Tarihi</th>
                                <th> Kullanıcı </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr class="odd gradeX">
                                    <td> {{$item->code}}</td>
                                    <td>
                                        @if($item['used'] == 1)
                                           <center><span class="label label-sm label-success"> Kullanıldı </span></center>
                                        @else
                                        <center><span class="label label-sm label-danger"> Kullanılmadı </span></center>
                                        @endif
                                    </td>
                                    <td><center>{{$item->created_at}}</center></td>
                                    <td><center>{{$item['user']['first_name']}} {{$item['user']['last_name']}}</center></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>



    <script>
        $(document).ready(function () {

            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });

            var body = $('body');



            body.on('click','.deleteOpenModal',function(event){
                $('#deleteConfirm').modal('show');
                var dataID = $(this).attr('data-id');

                var button = $(this);
                var deleted = $(this).attr('data-deleted');

                $('.delete').click(function (){
                    window.location= '#'+'/'+dataID;
                });

            });
        });


    </script>
@endsection