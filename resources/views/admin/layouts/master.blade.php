<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="tr">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Cepte Diyetisyen Yönetim Paneli</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta content="{{ csrf_token() }}" name="_token"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('assets/admin/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('assets/admin/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('/assets/admin/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('/assets/admin/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('/assets/admin/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('/assets/admin/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('/assets/admin/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/assets/admin/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/assets/admin/global/plugins/bootstrap-summernote/summernote.css')}}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<link href="{{asset('/assets/admin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}"
      rel="stylesheet" type="text/css"/>
<link href="{{asset('/assets/admin/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('/assets/admin/global/plugins/jquery-multi-select/css/multi-select.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('/assets/admin/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('/assets/admin/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('assets/admin/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css')}}"
      rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('assets/admin/global/plugins/bootstrap-editable/inputs-ext/address/address.css')}}"
      rel="stylesheet" type="text/css"/>

<link href="{{asset('assets/admin/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
      rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="{{asset('assets/admin/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components"
      type="text/css"/>
<link href="{{asset('assets/admin/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="{{asset('assets/admin/layouts/layout2/css/layout.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/admin/layouts/layout2/css/themes/blue.min.css')}}" rel="stylesheet" type="text/css"
      id="style_color"/>
<link href="{{asset('assets/admin/layouts/layout2/css/custom.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- END THEME LAYOUT STYLES -->
{{--<link rel="shortcut icon" href="favicon.ico"/>--}}
<script src="{{asset('assets/admin/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/admin/global/scripts/canvasjs.min.js')}}" type="text/javascript"></script>

    <style>
    #overlay {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
        background: #000;
        opacity: 0.8;
        z-index: 999999;
        filter: alpha(opacity=80);
    }
</style>
@if(null != Session::get('alert') || null != Session::get('error'))
    <script>
        $(document).ready(function () {

            @if(null != Session::get('alert'))
            toastr.success("{{Session::get('alert')}}");

            @elseif(null != Session::get('error'))
            toastr.error("{{Session::get('error')}}");
            @endif

                    toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        });
    </script>
@endif

<script>
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
    });

    function validationErrors(prefix, errors) {
        $.each(errors, function (name, error) {
            findValidationItem(prefix, name, function (item) {

                var form_group = item.parents('.form-group:eq(0)');

                if (typeof form_group != typeof undefined) {
                    var error_message = '<p class="help-block validation-error">* ' + error + '</p>';
                    form_group.find(".validation-error").remove();
                    form_group.addClass('has-error').append(error_message);

                }
            });
        });
    }

    function removeErrors(prefix) {
        $('.form-group.has-error.prefix_' + prefix).removeClass('has-error');
        $('.validation-error').remove();
    }

    function findValidationItem(prefix, name, callback) {
        var item = undefined;

        item = $('input[name="' + prefix + name + '"]');

        if (typeof item == typeof undefined || item == '' || item == null || item.prop('tagName') == undefined) {
            item = $('textarea[name="' + prefix + name + '"]');
        }

        if (typeof item == typeof undefined || item == '' || item == null || item.prop('tagName') == undefined) {
            item = $('select[name="' + prefix + name + '"]');
        }

        callback(item);
    }

    $(document).ready(function () {

        $('.modal').on('hidden.bs.modal', function () {
            $(this).find('input').each(function () {
                $(this).val("");
            });

        });

        $('.close-modal').click(function () {
            $(this).parents('modal')
        });


        $('.checkbox-inline').children().on('click', function () {
            var target = $(this).closest('input');

            if (target.val() == "1") {
                target.val("0");
            } else {
                target.val("1");
            }
        });

    });

    window.postModalForm = function (url, modalObject, success_function, error_function) {

        var modal = modalObject;
        if (typeof(success_function) === 'undefined') success_function = function (data) {

            modal.modal('hide');
            toastr.success(data.message);
            location.reload();
        };

        if (typeof(error_function) === 'undefined') error_function = function (data) {

            validationErrors('', data.messages);
            toastr.error("{{config('messages.admin.formError')}}");
        };

        var postdata = {};

        //Modal'ın içindeki inputları postDataya doldur
        modal.find('input,select').each(function () {
            postdata[$(this).attr("name")] = $(this).val();
        });

        //update mi insert mü?
        if (postdata['id'] == "false" || postdata['id'] == "")
            postdata['id'] = false;

        $.ajax({
            url: url,
            data: postdata,
            type: 'POST',

            success: function (data) {
                if (data.status)
                    success_function(data);

                else
                    error_function(data);


                $('meta[name="_token"]').attr('content', data.token);
            },
            error: function () {
                toastr.error("{{config('massages.admin.unknownError')}}");
            }
        });
    }


    window.clearPost = function (url, postdata, success_function, error_function) {

        if (typeof(success_function) === 'undefined') success_function = function (data) {
            toastr.success(data.message);
        };

        if (typeof(error_function) === 'undefined') error_function = function (data) {
            toastr.error(data.message);
        };

        console.log(postdata);
        var flag;
        $.ajax({
            url: url,
            data: postdata,
            type: 'POST',

            success: function (data) {
                if (data.status) {
                    success_function(data);
                }
                else {
                    error_function(data);
                }

                $('meta[name="_token"]').attr('content', data.token);

            },
            error: function () {
                toastr.error("{{config('massages.admin.unknownError')}}");
            }
        });
    }


    window.getEditInfo = function (dataId, modalObject, url) {

        modalObject.attr('data-id', dataId);

        $.ajax({
            url: url + '/' + dataId,

            success: function (data) {

                $.map(data, function (value, index) {
                    if (typeof value == "object") {
                        $('#' + index).multiSelect('deselect_all');
                        $('#' + index).multiSelect('select', value);
                    } else {
                        var target = modalObject.find('input[name="' + index + '"]');

                        if (target.length > 0) {

                            target.val(value);

                            if (target.attr('type') == "checkbox") {

                                if (value == "1") {
                                    target.prop('checked', true).closest('span').addClass('checked');
                                }
                                else {
                                    target.prop('checked', false).closest('span').removeClass('checked');
                                }

                            }

                        }else{
                            var alternative_target = modalObject.find('select[name="' + index + '"]');

                            if(alternative_target.length > 0){

                                if($.inArray(value,[0,1,2,3,4,5,6,7,8,9]))
                                    value = parseInt(value);

                                alternative_target.val(value);
                            }

                        }
                    }

                });
            }
        });

        modalObject.modal('show');
    }


</script>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">

<!-- BEGIN HEADER -->
@include('admin.partials.header')
<!-- END HEADER -->

<div class="clearfix"></div>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
@include('admin.partials.sidebar')
<!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            @include('admin.partials.breadcrumb')
            @yield('content')
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->


<!-- BEGIN FOOTER -->
@include('admin.partials.footer')
<!-- END FOOTER -->
<style>
    .error{
        color:red;
    }
</style>

<!--[if lt IE 9]>
<script src="{{asset('assets/admin/global/plugins/respond.min.js')}}"></script>
<script src="{{asset('assets/admin/global/plugins/excanvas.min.js')}}"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->

<script src="{{asset('assets/admin/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('/assets/admin/global/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/admin/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}"
        type="text/javascript"></script>
<script src="{{asset('/assets/admin/global/plugins/bootstrap-toastr/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/admin/pages/scripts/ui-toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/morris/morris.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/morris/raphael-min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/counterup/jquery.waypoints.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/counterup/jquery.counterup.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/flot/jquery.flot.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/flot/jquery.flot.resize.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/flot/jquery.flot.categories.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/admin/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('/assets/admin/global/plugins/jquery.mockjax.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/bootstrap-editable/inputs-ext/address/address.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/bootstrap-editable/inputs-ext/wysihtml5/wysihtml5.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
        type="text/javascript"></script>

<script src="{{asset('assets/admin/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/bootstrap-summernote/summernote.min.js')}}" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{asset('assets/admin/global/scripts/app.min.js')}}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('assets/admin/pages/scripts/dashboard.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/pages/scripts/components-bootstrap-tagsinput.min.js')}}"
        type="text/javascript"></script>

<script src="{{asset('assets/admin/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/admin/pages/scripts/table-datatables-responsive.min.js')}}"
        type="text/javascript"></script>


<script src="{{asset('assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
        type="text/javascript"></script>

<script src="{{asset('assets/admin/global/plugins/jquery-validation/js/localization/messages_tr.js')}}"
        type="text/javascript"></script>

<script src="{{asset('assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
        type="text/javascript"></script>


<script src="{{asset('assets/admin/pages/scripts/form-validation.min.js')}}"
        type="text/javascript"></script>


<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{asset('assets/admin/layouts/layout2/scripts/layout.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/layouts/layout2/scripts/demo.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    $(document).ready(function () {
        if ($('.table.data-table').length > 0)
            $('.table.data-table').DataTable();

        $('.select2').select2();
        $(".mask_phone").inputmask("mask",{mask:"(999) 999-9999"});

        $('form').validate();

    });
</script>
@yield('script', "")
</body>

</html>
