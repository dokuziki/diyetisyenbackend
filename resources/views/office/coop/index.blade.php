@extends('office.layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Mailler</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{asset('admin/coop/upsert')}}" id="sample_editable_1_new" class="btn sbold green"> Yeni Ekle
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table data-table table-striped table-bordered table-hover table-checkable order-column">
                            <thead>
                            <tr>
                                <th> ID</th>
                                <th> Mail Domaini</th>
                                <th> Bitiş Tarihi</th>
                                <th> İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($coops as $coop)
                                <tr class="odd gradeX">
                                    <td> {{$coop['id']}}</td>
                                    <td> {{$coop['email_extension']}}</td>
                                    <td> {{$coop['end_date']}}</td>
                                    <td>
                                        <button class="btn btn-sm btn-danger deleteOpenModal" data-id="{{$coop->id}}" data-deleted="tr.gradeX"><i class="fa fa-trash"></i> Sil </button>
                                        <a href="{{asset('admin/coop/upsert')}}/{{$coop->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row ">
                            <div class="col-xs-12">
                                <center>

                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
</div>

    <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{"Mail Adresini Silmek İstediğinize Emin Misiniz ?"}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Vazgeç</button>
                    <submit type="button" class="btn btn-danger delete" id="newEditorAdd">Maili Sil</submit>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });

            var body = $('body');



            body.on('click','.deleteOpenModal',function(event){
                $('#deleteConfirm').modal('show');
                var dataID = $(this).attr('data-id');

                var button = $(this);
                var deleted = $(this).attr('data-deleted');

                $('.delete').click(function (){
                    window.location= '{{action('Office\CoopController@getDelete')}}'+'/'+dataID;
                });

            });
        });


    </script>
        @endsection