@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Email Ayarı</span>
                    </div>

                </div>
                <div class="portlet-body">

                    <!-- BEGIN FORM-->
                    <form action="{{asset('admin/coop/upsert')}}@if(isset($coop['id']))/{{$coop['id']}}@endif"
                          method="post" class="horizontal-form" enctype="multipart/form-data">
                        <input type="hidden" name="id" @if(isset($coop['id'])) value="{{$coop['id']}}"
                               @else value="false" @endif />

                        <div class="form-body">
                            <h3 class="form-section">Mail Bilgileri</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Mail Adresi</label>
                                        <input type="text" name="email" class="form-control" placeholder=""
                                               @if(isset($coop['email_extension'])) value="{{$coop['email_extension']}}"
                                               @endif autocomplete="new-password" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Bitiş Tarihi</label>
                                        <input type="date" name="end_date" class="form-control"
                                                @if(isset($coop->end_date)) value="{{$coop->end_date}}"
                                               @endif required>

                                    </div>
                                </div>
                                <!--/span-->
                                <!--/span-->
                                <!--/span-->
                            </div>
                            <!--/row-->
                        </div>
                        <br>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Kaydet
                            </button>
                        </div>
                    </form>
                    <!-- END FORM-->

                    <style>
                        .orderRow td:first-child {
                            display: none;
                        }

                        .orderRow td:nth-child(3) {
                            text-align: center;
                        }

                        .table th:first-child {
                            display: none;
                        }
                    </style>

                    <br>
                    <br>


                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection