@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="btn-group">
                <a href="{{asset('admin/export/index')}}?time=w" id="sample_editable_1_new" class="btn sbold green"> Haftalık Excel Tablosu
                </a>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="btn-group">
                <a href="{{asset('admin/export/index')}}?time=m" id="sample_editable_1_new" class="btn sbold green"> Aylık Excel Tablosu
                </a>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="btn-group">
                <a href="{{asset('admin/export/index')}}?time=y" id="sample_editable_1_new" class="btn sbold green"> Yıllık Excel Tablosu
                </a>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 80px">
                <div class="display">
                    <a href="{{route('office.user.index')}}">
                        <div class="number">
                            <h3 class="font-green-sharp">
                                <span data-counter="counterup" data-value="{{$user_count}}">0</span>
                                <small class="font-green-sharp"></small>
                            </h3>
                            <small>ÜYE</small>
                        </div>
                    </a>
                    <div class="icon">
                        <i class="glyphicon glyphicon-user"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 80px">
                <div class="display">
                    <a href="{{route('office.nutrition.index')}}">
                        <div class="number">
                            <h3 class="font-green-sharp">
                                <span data-counter="counterup" data-value="{{$nutrition_count}}">0</span>
                                <small class="font-green-sharp"></small>
                            </h3>
                            <small>DİYETİSYEN</small>
                        </div>
                    </a>
                    <div class="icon">
                        <i class="glyphicon glyphicon-leaf"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 80px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$premium}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>Premium</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-money"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 80px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{!! $diet_list !!}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>Diyet Listesi</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-money"></i>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="dashboard-stat2">
                <div class="form-group">
                    <label class="control-label" for="single">Haftalık Ödeme Yapan Sayısı</label>
                    <canvas id="myChart" width="997px" height="200px">
                        @foreach($pay_weeks as $pay_week)
                            <div class="logs">{{$pay_week}}</div>
                        @endforeach
                    </canvas>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-5">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-green-sharp bold uppercase">Yakın Zamanda Diyet Listesi Alanlar</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <ul class="list-group">
                        @foreach($paid_users as $puser)
                            <li class="list-group-item"><small>#{{$puser->user->id}}</small> {{$puser->user->first_name}} {{$puser->user->last_name}}
                                <span class="badge badge-warning pull-right"> {{ \Carbon\Carbon::createFromFormat('Y-m-d',$puser->start_date)->diffInDays()}} gün önce </span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('script')
    <script src="{{asset('assets/admin/global/scripts/Chart.min.js')}}" type="text/javascript"></script>
    <script>
        var ctx = document.getElementById("myChart");
        var DATA = [];
        function deneme() {
            var elems = document.getElementsByClassName("logs");
            for (elem of elems) {
                DATA.push(elem.innerHTML);
            }
            console.log(DATA);
        }
        deneme();

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: DATA,
                datasets: [{
                    label: 'Haftalık Ödeme Yapan Sayısı',
                    data: DATA,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
        });
    </script>


@endsection
