@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Diyetisyen Değişiklik Talebinde Bulunanlar</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {{-- <div class="table-toolbar">
                         <div class="row">
                             <div class="col-md-6">
                                 <div class="btn-group">
                                     <a href="{{asset('admin/nutrition/upsert')}}" id="sample_editable_1_new" class="btn sbold green"> Yeni Ekle
                                         <i class="fa fa-plus"></i>
                                     </a>
                                 </div>
                             </div>

                         </div>
                     </div>--}}
                    <div class="portlet-body">
                        <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                               id="sample_1">
                            <thead>
                            <tr>
                                <th> Adı Soyadı</th>
                                <th> Diyetisyeni</th>
                                <th width="100"> İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($changes as $change)
                                <tr class="odd gradeX">
                                    <td> {{$change->users['first_name']}} {{$change->users['last_name']}}</td>
                                    <td> {{$change->users->nutritionist['first_name']}} {{$change->users->nutritionist['last_name']}}</td>
                                    <td>
                                        <a href="{{asset('admin/nutrition/detail')}}/{{$change->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <button class="btn btn-sm btn-danger deleteOpenModal" data-id="{{$change->id}}" data-deleted="tr.gradeX"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{'Talebi Silmek İstediğinize Emin Misiniz ?'}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Vazgeç</button>
                    <submit type="button" class="btn btn-danger delete" id="newEditorAdd">Talebi Sil</submit>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Onaylanan Talepler</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                               id="sample_1">
                            <thead>
                            <tr>
                                <th> Adı Soyadı</th>
                                <th> Yeni Diyetisyeni</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($approves as $approve)
                                <tr class="odd gradeX">
                                    <td> {{$approve->users['first_name']}} {{$approve->users['last_name']}}</td>
                                    <td> {{$approve->users->nutritionist['first_name']}} {{$approve->users->nutritionist['last_name']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });

            var body = $('body');



            body.on('click','.deleteOpenModal',function(event){
                $('#deleteConfirm').modal('show');
                var dataID = $(this).attr('data-id');

                var button = $(this);
                var deleted = $(this).attr('data-deleted');

                $('.delete').click(function (){
                    window.location= '{{action('Office\NutritionController@getChangeDelete')}}'+'/'+dataID;
                });

            });
        });


    </script>

@endsection