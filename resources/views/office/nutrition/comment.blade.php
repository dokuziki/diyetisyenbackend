@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Yorumlar</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover ">
                            <thead>
                            <tr>
                                <th> Diyetisyen</th>
                                <th> Yorum</th>
                                <th width="150"> Tarih</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($comments as $comment)
                                <tr class="odd gradeX">
                                    <td> {{$comment->user->first_name}} {{$comment->user->last_name}}</td>
                                    <td> {{$comment->comment}}</td>
                                    <td>
                                        {{$comment->created_at->format('d-m-Y')}}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if(count($comments) === 0)
                            <center>{{$day}} Gün Öncesine Ait Yorum Bulunamadı</center>
                        @endif
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="dataTables_paginate paging_bootstrap_number" id="sample_1_paginate">
                                    <ul class="pagination" style="visibility: visible;">
                                        @for($i=0;$i<20; ++$i)
                                            <li @if((int)$day===$i) class="active" @endif > <a href="?day={{$i}}">{{$i+1}}</a></li>
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">{{'Yorumu Silmek İstediğinize Emin Misiniz ?'}}</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Vazgeç
                                </button>
                                <submit type="button" class="btn btn-danger delete" id="newEditorAdd">Yorumu Sil
                                </submit>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });

            var body = $('body');


            body.on('click', '.deleteOpenModal', function (event) {
                $('#deleteConfirm').modal('show');
                var dataID = $(this).attr('data-id');

                var button = $(this);
                var deleted = $(this).attr('data-deleted');

                $('.delete').click(function () {
                    window.location = '{{action('Office\NutritionController@getCommentDelete')}}' + '/' + dataID;
                });

            });
        });


    </script>
@endsection