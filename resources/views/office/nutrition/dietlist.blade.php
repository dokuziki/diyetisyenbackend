@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="glyphicon glyphicon-phone font-dark"></i>
                        <span class="caption-subject bold uppercase">Kullanıcı Adına Göre Diyet Listesi Oluşturma Talebi</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-xs-12">
                                <form action="{{action('Office\DietListController@postIndex')}}" method="post" class="horizontal-form">
                                    <input type="hidden" name="id"/>
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label bold">Kullanıcı</label>
                                                    <select class="ajaxselect select2 col-md-12" name="username">
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label bold">Kaç Adet Diyet Listesi Oluşturulacak </label>
                                                    <textarea class="form-control" rows="1" id="message" name="message"
                                                              placeholder="Liste Sayısı"></textarea>
                                                </div>
                                                <div class="form-group">
                                                </div>


                                            </div>


                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn dark">
                                                <i class="fa fa-check"></i> Gönder
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>


        <div class="col-md-6">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="glyphicon glyphicon-phone font-dark"></i>
                        <span class="caption-subject bold uppercase">ID Numarasına Göre Diyet Listesi Oluşturma Talebi</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-xs-12">
                                <form action="{{action('Office\DietListController@postId')}}" method="post" class="horizontal-form">
                                    <input type="hidden" name="id"/>
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label bold">ID Numarası</label>
                                                    <select class="ajaxnumber select2 col-md-12" name="number">
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label bold">Kaç Adet Diyet Listesi Oluşturulacak </label>
                                                    <textarea class="form-control" rows="1" id="message" name="message"
                                                              placeholder="Liste Sayısı"></textarea>
                                                </div>
                                                <div class="form-group">
                                                </div>


                                            </div>


                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn dark">
                                                <i class="fa fa-check"></i> Gönder
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection
@section('script')
    <script>


        $(document).ready(function(){


            $(".ajaxselect").select2({
                ajax: {
                    dataType: "json",
                    url: "{{asset('/admin/notifications/personal-search')}}",
                    processResults: function (data) {
                        console.log(data);
                        return data;
                    }
                },
                templateResult: function (data) {
                    return  data.first_name + " " + data.last_name+ " " + data.id;
                },
                templateSelection: function (data) {
                    return data.first_name + " " + data.last_name + " " + data.id;
                },
                cache: true,
                minimumInputLength: 3,
                placeholder: "Kullanıcı Adı"
            });

            $(".ajaxnumber").select2({
                ajax: {
                    dataType: "json",
                    url: "{{asset('/admin/notifications/personal-id-search')}}",
                    processResults: function (data) {
                        console.log(data);
                        return data;
                    }
                },
                templateResult: function (data) {
                    return  data.first_name + " " + data.last_name+ " " + data.id;
                },
                templateSelection: function (data) {
                    return data.first_name + " " + data.last_name + " " + data.id;
                },
                cache: true,
                minimumInputLength: 1,
                placeholder: "ID Numarası"
            });
        });


    </script>
@endsection