@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Detaylar</span>
                    </div>

                </div>
                <div class="portlet-body">

                    <!-- BEGIN FORM-->
                    <form action="{{asset('admin/nutrition/detail')}}@if(isset($item['id']))/{{$item['id']}}@endif" method="post" class="horizontal-form">
                        <input type="hidden" name="id"   @if(isset($item['id'])) value="{{$item['id']}}" @else value="false" @endif />
                        <input type="hidden" name="userId" value="{{$item->users['id']}}">
                        <div class="form-body">
                            <h3 class="form-section">Kullanıcı Bilgileri</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Adı</label>
                                        <input type="text" name="first_name" class="form-control" placeholder="" @if(isset($item->users['first_name'])) value="{{$item->users['first_name']}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Soyadı</label>
                                        <input type="text" name="last_name" class="form-control" placeholder="" @if(isset($item->users['last_name'])) value="{{$item->users['last_name']}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>
                                <!--/span-->

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Mail Adresi</label>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($item->users['email'])) value="{{$item->users['email']}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>
                                <!--/span-->
                                <!--/span-->
                            </div>
                            <div class="row">
                                <h3 class="form-section">Diyetisyen Bilgileri</h3>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Adı</label>
                                        <input type="text" name="first_name" class="form-control" placeholder="" @if(isset($item->users->nutritionist['first_name'])) value="{{$item->users->nutritionist['first_name']}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Soyadı</label>
                                        <input type="text" name="last_name" class="form-control" placeholder="" @if(isset($item->users->nutritionist['last_name'])) value="{{$item->users->nutritionist['last_name']}}" @endif autocomplete="new-password" disabled required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Değişiklik Talep Sebebi</label>
                                        <textarea class="form-control" rows="4" disabled>@if(isset($item['content'])) {{$item['content']}} @endif</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Yeni Diyetisyen</label>
                                        <select id="nutritionist"  name="nutritionist" class="form-control ">
                                            @foreach($nutritions as $nutrition)
                                            <option value="{{$nutrition->id}}">{{$nutrition->first_name}} {{$nutrition->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <!--/row-->
                            <br>
                            <br>
                        </div>
                        <br>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Onayla
                            </button>
                        </div>
                    </form>
                    <!-- END FORM-->

                    <style>
                        .orderRow td:first-child{
                            display:none;
                        }

                        .orderRow  td:nth-child(3) {
                            text-align: center;
                        }
                        .table th:first-child{
                            display:none;
                        }
                    </style>

                    <br>
                    <br>





                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection
