@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 90px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$users}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>HASTALAR</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 90px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$codes}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>ÖZEL HASTA</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 90px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$meals}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>ONAYLANMAMIŞ ÖĞÜNLER</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 90px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$approve_meal}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>ONAYLANMIŞ ÖĞÜNLER</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 90px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$approve_list}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>HAZIRLANAN LİSTE</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2" style="height: 90px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$wait_list}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>BEKLEYEN LİSTE</small>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="dashboard-stat2">
                <div class="form-group">
                    <center> <label class="control-label" for="single">Son 30 Günlük Ödeme Yapan Sayısı</label></center>
                    <canvas id="myChart" width="997px" height="200px">
                        @foreach($diet_lists as $key => $diet_list)
                            <div class="lists">{{count($diet_list[1])}}</div>
                            <div class="lists2">{{count($diet_list[0])}}</div>
                            <div class="dates">{!! (string)$key!!}</div>
                        @endforeach
                    </canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="dashboard-stat2">
                <div class="form-group">
                    <center> <label class="control-label" for="single">Aylık Yapılan Yorum Sayısı</label></center>
                    <canvas id="commentChart" width="997px" height="200px">
                        @foreach($commentByM as $key => $comment)
                            <div class="yorumlar">{{count($comment)}}</div>
                            <div class="aylar">{{$key}}</div>
                        @endforeach
                    </canvas>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Diyetisyen Ayarı</span>
                    </div>

                </div>
                <div class="portlet-body">

                    <!-- BEGIN FORM-->
                    <form action="{{asset('admin/nutrition/upsert')}}@if(isset($item['id']))/{{$item['id']}}@endif"
                          method="post" class="horizontal-form">
                        <input type="hidden" name="id" @if(isset($item['id'])) value="{{$item['id']}}"
                               @else value="false" @endif />

                        <div class="form-body">
                            <h3 class="form-section">Hesap Bilgileri</h3>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <div class="form-group">
                                    </div>
                                    @if(isset($item['pic']) && $item['pic'] !== '' &&  $item['pic'] !== asset('uploads/'))
                                        <img height="200" src="{{asset($item['pic'])}}"/>
                                    @endif
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Adı</label>
                                        <input type="text" name="first_name" class="form-control" placeholder=""
                                               @if(isset($item['first_name'])) value="{{$item['first_name']}}"
                                               @endif autocomplete="new-password" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Soyadı</label>
                                        <input type="text" name="last_name" class="form-control" placeholder=""
                                               @if(isset($item['last_name'])) value="{{$item['last_name']}}"
                                               @endif autocomplete="new-password" required>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Mail Adresi</label>
                                        <input type="text" name="email" class="form-control" placeholder=""
                                               @if(isset($item['email'])) value="{{$item['email']}}"
                                               @endif autocomplete="new-password" required>
                                    </div>
                                </div>
                                <!--/span-->
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <br>
                            <br>
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Hesap Tipi</label>
                                        <select id="role" name="role" class="form-control ">
                                            <option value="0" @if($item['status'] === 0) selected @endif>Normal Üye
                                            </option>
                                            <option value="1" @if($item['status'] === 1) selected @endif>Diyetisyen
                                            </option>
                                            <option value="2" @if($item['status'] === 2) selected @endif>Yönetici
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>

                        </div>
                        <br>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Kaydet
                            </button>
                        </div>
                    </form>
                    <!-- END FORM-->

                    <style>
                        .orderRow td:first-child {
                            display: none;
                        }

                        .orderRow td:nth-child(3) {
                            text-align: center;
                        }

                        .table th:first-child {
                            display: none;
                        }
                    </style>

                    <br>
                    <br>


                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>



    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Diyetisyen Detayı</span>
                    </div>

                </div>
                <div class="portlet-body">

                    <!-- BEGIN FORM-->

                    <div class="form-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Başlık</b></label>
                                    <input type="text" name="email" class="form-control" placeholder=""
                                           value="{{$detail['title']}}" disabled>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Video</b></label>
                                    <input type="text" name="email" class="form-control" placeholder=""
                                           value="{{$detail['video']}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Eğitim</b></label>
                                    <input type="text" name="email" class="form-control" placeholder=""
                                           value="{{$detail['information']}}" disabled>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Açıklama</b></label>
                                    <textarea class="form-control" rows="3"
                                              disabled>{{$detail['description']}}</textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Instagram Hesabı</b></label>
                                    <input type="text" name="email" class="form-control" placeholder=""
                                           value="{{$detail['instagram']}}" disabled>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Facebook Hesabı</b></label>
                                    <input type="text" name="email" class="form-control" placeholder=""
                                           value="{{$detail['facebook']}}" disabled>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>Twitter Hesabı</b></label>
                                    <input type="text" name="email" class="form-control" placeholder=""
                                           value="{{$detail['twitter']}}" disabled>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="single"><b>LinkedIn Hesabı</b></label>
                                    <input type="text" name="email" class="form-control" placeholder=""
                                           value="{{$detail['linkedin']}}" disabled>

                                </div>
                            </div>


                        </div>
                        <!--/row-->
                    </div>

                    <!-- END FORM-->

                    <style>
                        .orderRow td:first-child {
                            display: none;
                        }

                        .orderRow td:nth-child(3) {
                            text-align: center;
                        }

                        .table th:first-child {
                            display: none;
                        }
                    </style>


                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>

@endsection
@section('script')
    <script src="{{asset('assets/admin/global/scripts/Chart.min.js')}}" type="text/javascript"></script>
    <script>
        var ctx = document.getElementById("myChart");

        var DATA = [];
        var DATA2 = [];
        var DATE = [];


        function deneme() {
            var elems = document.getElementsByClassName("lists");
            var dates = document.getElementsByClassName("dates");
            var data2 = document.getElementsByClassName("lists2");

            for (elem of data2) {
                DATA2.push(elem.innerHTML);
            }


            for (elem of elems) {
                DATA.push(elem.innerHTML);
            }
            console.log(DATA);


            for (dt of dates) {
                DATE.push(dt.innerHTML);
            }
            console.log(DATE);
        }
        deneme();

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: DATE,
                datasets: [{
                    label: 'Diyet Listesi Satın Alan Kullanıcılar',
                    data: DATA,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                },
                    {
                        label: 'Online Sıkı Takip Satın Alan Kullanıcılar',
                        data: DATA2,
                        backgroundColor: [
                            'rgba(65,105,225, 0.2)',
                            'rgba(65,105,225, 0.2)',
                            'rgba(65,105,225, 0.2)',
                            'rgba(65,105,225, 0.2)',
                            'rgba(65,105,225, 0.2)',
                            'rgba(65,105,225, 0.2)'
                        ],
                        borderColor: [
                            'rgba(65,105,225,1)',
                            'rgba(65,105,225, 1)',
                            'rgba(65,105,225, 1)',
                            'rgba(65,105,225, 1)',
                            'rgba(65,105,225, 1)',
                            'rgba(65,105,225, 1)'
                        ],
                        borderWidth: 1
                    }
                ]
            },
        });
    </script>

    <script>
        var ctx = document.getElementById("commentChart");

        var MONTHS = [];
        var COMMENTS = [];


        function deneme() {
            var yorumlar = document.getElementsByClassName("aylar");
            var aylar = document.getElementsByClassName("yorumlar");

            for (yorum of yorumlar) {
                COMMENTS.push(yorum.innerHTML);
            }

            for (ay of aylar) {
                MONTHS.push(ay.innerHTML);
            }
            console.log(MONTHS);
        }
        deneme();

        var commentChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: COMMENTS,
                datasets: [{
                    label: 'Aylık Yorum Sayısı Grafiği',
                    data: MONTHS,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }
                ]
            },
        });
    </script>


@endsection
