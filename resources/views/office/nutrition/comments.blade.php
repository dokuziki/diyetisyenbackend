@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Yorumlar</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase"> Yorum</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Adı Soyadı</label>
                                    <input type="text" name="first_name" class="form-control" placeholder="" @if(isset($usr['first_name'])) value="{{$usr['first_name']}} {{$usr['last_name']}}" @endif autocomplete="new-password" disabled required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Diyetisyen Adı Soyadı</label>
                                    <input type="text" name="nutritionist_name" class="form-control" placeholder="" @if(isset($usr->nutritionist['first_name'])) value="{{$usr->nutritionist['first_name']}} {{$usr->nutritionist['last_name']}}" @endif autocomplete="new-password" disabled required>
                                </div>
                            </div>
                            <div class="content">
                                <form method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="id" value="{{$comment['id']}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="row no-margin">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Yorum İçeriği</label>
                                                <textarea class="form-control" rows="3" style="resize: none;" disabled>@if(isset($comment->id)) {{$comment->content}} @endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row no-margin">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> Onayla</button>
                                                <a href="#" class="btn btn-flat btn-danger deleteOpenModal" data-id="{{$comment['id']}}" data-deleted="tr.gradeX">Yorumu Sil</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body">
                        <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                               id="sample_1">
                            <thead>
                            <tr>
                                <th> Yorumu Yapan</th>
                                <th> Diyetisyen</th>
                                <th width="150"> İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr class="odd gradeX">
                                    <td> {{$user->user['first_name']}} {{$user->user['last_name']}}</td>
                                    <td> {{$user->user->nutritionist['first_name']}} {{$user->user->nutritionist['last_name']}}</td>
                                   {{-- <td> {{$user['content']}}</td>--}}
                                    <td>
                                        <a href="{{action('Office\NutritionController@postComments', $user->id)}}" data-action="add" class="btn btn-primary btn-sm edit">Yorumu Oku</a>
                                        {{--<button class="btn btn-sm btn-danger deleteOpenModal" data-id="{{$comment['id']}}" data-deleted="tr.gradeX"><i class="fa fa-trash"></i></button>--}}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">{{'Yorumu Silmek İstediğinize Emin Misiniz ?'}}</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Vazgeç</button>
                                <submit type="button" class="btn btn-danger delete" id="newEditorAdd">Yorumu Sil</submit>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });

            var body = $('body');



            body.on('click','.deleteOpenModal',function(event){
                event.preventDefault();

                $('#deleteConfirm').modal('show');
                var dataID = $(this).attr('data-id');

                var button = $(this);
                var deleted = $(this).attr('data-deleted');

                $('.delete').click(function (){
                    window.location= '{{action('Office\NutritionController@getCommentDelete')}}'+'/'+dataID;
                });

            });
        });


    </script>
@endsection