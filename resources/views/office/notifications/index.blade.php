@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="glyphicon glyphicon-phone font-dark"></i>
                        <span class="caption-subject bold uppercase"> Bildirimler</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-xs-6">
                                <form action="#" method="post" class="horizontal-form">
                                    <input type="hidden" name="id"/>
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label bold">Cinsiyet </label>
                                                    <label class="radio-inline"> 
                                                        <input type="radio" name="gender" id="gender" value="0"> Bay 
                                                    </label> 
                                                    <label class="radio-inline"> 
                                                        <input type="radio" name="gender" id="gender" value="1"> Bayan 
                                                    </label>
                                                    <label class="radio-inline"> 
                                                        <input type="radio" name="gender" id="gender" value="2" checked> Hepsi 
                                                    </label> <br><br>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label bold">Üyelik Tipi </label>
                                                    <select class="form-control select2" name="type">
                                                        <option value="all">Tümü</option>
                                                        <option value="premium">Premium</option>
                                                        <option value="ozel">Özel Hasta</option>
                                                        <option value="normal">Normal</option>
                                                        <option value="list">Diyet Listesi (Son 1 Hafta)</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label bold">Mesaj </label>
                                        <textarea class="form-control" rows="2" id="message" name="message"
                                                  placeholder="Mesaj"></textarea>
                                                </div>
                                                <div class="form-group">
                                                </div>


                                            </div>


                                        </div>


                                        <br><br>
                                        <div class="form-actions">
                                            <button type="submit" class="btn dark">
                                                <i class="fa fa-check"></i> Gönder
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>

                            <div class="col-xs-3">
                                <form action="/admin/notifications/personal" method="post" class="horizontal-form">
                                    <input type="hidden" name="id"/>
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label bold">Kullanıcı Adı </label>
                                                    <select class="ajaxselect select2 col-md-12" name="username">
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label bold">Mesaj </label>
                                        <textarea class="form-control" rows="2" id="message" name="message"
                                                  placeholder="Mesaj"></textarea>
                                                </div>
                                                <div class="form-group">
                                                </div>


                                            </div>


                                        </div>


                                        <br><br>
                                        <div class="form-actions">
                                            <button type="submit" class="btn dark">
                                                <i class="fa fa-check"></i> Gönder
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            <div class="col-xs-3">
                                <form action="/admin/notifications/personal" method="post" class="horizontal-form">
                                    <input type="hidden" name="id"/>
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label bold">ID'ye Göre </label>
                                                    <select class="idselect select2 col-md-12" name="username">
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label bold">Mesaj </label>
                                                    <textarea class="form-control" rows="2" id="message" name="message"
                                                              placeholder="Mesaj"></textarea>
                                                </div>
                                                <div class="form-group">
                                                </div>


                                            </div>


                                        </div>


                                        <br><br>
                                        <div class="form-actions">
                                            <button type="submit" class="btn dark">
                                                <i class="fa fa-check"></i> Gönder
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet-body">
                            <table class="table  table-striped table-bordered table-hover table-checkable order-column">
                                <thead>
                                <tr>
                                    <th>Mesaj</th>
                                    <th width="20%">Gönderilen /Ulaşan</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pushs as $push)
                                    <tr>
                                        <td>
                                            @if(isset($push->contents->tr)) {{$push->contents->tr}} @endif
                                        </td>
                                        <td>{{count($push->include_player_ids)}} / {{$push->successful}} </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection

@section('script')
    <script>


        $(document).ready(function(){


            $(".ajaxselect").select2({
                ajax: {
                    dataType: "json",
                    url: "{{asset('/admin/notifications/personal-search')}}",
                    processResults: function (data) {
                        console.log(data);
                        return data;
                    }
                },
                templateResult: function (data) {
                    return  data.first_name + " " + data.last_name + " " + data.id;;
                },
                templateSelection: function (data) {
                    return data.first_name + " " + data.last_name + " " + data.id;
                },
                cache: true,
                minimumInputLength: 3,
                placeholder: "Kullanıcı Adı"
            });

            $(".idselect").select2({
                ajax: {
                    dataType: "json",
                    url: "{{asset('/admin/notifications/personal-id-search')}}",
                    processResults: function (data) {
                        console.log(data);
                        return data;
                    }
                },
                templateResult: function (data) {
                    return  data.first_name + " " + data.last_name + " " + data.id;;
                },
                templateSelection: function (data) {
                    return data.first_name + " " + data.last_name + " " + data.id;
                },
                cache: true,
                minimumInputLength: 1,
                placeholder: "ID"
            });
        });


    </script>
@endsection