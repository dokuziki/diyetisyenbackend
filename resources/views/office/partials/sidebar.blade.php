<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start">
                <a href="{{route('office.dash')}}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Anasayfa</span>
                </a>
                <!-- Sub menu example
                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="#" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Dashboard 1</span>
                        </a>
                    </li>
                </ul>
                Sub Menu End -->
            </li>

            <li class="nav-item start">
                <a href="{{route('office.nutrition.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-leaf"></i>
                    <span class="title">Diyetisyenler</span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="{{route('office.nutrition.change')}}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title"> Değişiklik Talebi</span>
                        </a>
                    </li>

                    <li class="nav-item start ">
                        <a href="{{route('office.nutrition.comments')}}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title"> Diyetisyen Yorumları</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item start">
                <a href="{{route('office.notifications.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-phone"></i>
                    <span class="title">Bildirimler</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="{{route('office.portion.index')}}" class="nav-link nav-toggle">
                    <i class="fa fa-list"></i>
                    <span class="title">Porsiyon Listesi</span>
                </a>
            </li>

            <li class="nav-item start">
                <a href="{{route('office.nutrition.dietlist')}}" class="nav-link nav-toggle">
                    <i class="fa fa-plus-square"></i>
                    <span class="title">Diyet Listesi Talebi</span>
                </a>
            </li>


            <li class="nav-item start">
                <a href="{{--{{route('office.dash')}}--}}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Sayfalar</span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="{{route('office.pages.about')}}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Hakkımızda</span>
                        </a>
                    </li>

                    <li class="nav-item start ">
                        <a href="{{route('office.pages.terms')}}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Üyelik Sözleşmesi</span>
                        </a>
                    </li>

                    <li class="nav-item start ">
                        <a href="{{route('office.pages.privacy')}}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Gizlilik Koşulları</span>
                        </a>
                    </li>

                    <li class="nav-item start ">
                        <a href="{{route('office.pages.blog')}}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Blog</span>
                        </a>
                    </li>
                </ul>

            </li>


            <li class="nav-item start">
                <a href="{{route('office.user.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-user"></i>
                    <span class="title">Üyeler</span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="{{route('office.coop.index')}}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title"> Email Domaini</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{route('office.user.spam')}}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title"> Spam Şikayetleri</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{route('office.user.diyetisyen')}}" class="nav-link ">
                            <i class="glyphicon-user"></i>
                            <span class="title"> Diyetisyen Listesi</span>
                        </a>
                    </li>
                </ul>
            </li>


        </ul>
    </div>
</div>
<script>
    $(document).ready(function () {

        if ($("a[href='{{Request::url()}}']").length > 0)
            $("a[href='{{Request::url()}}']").append('<span class="selected"></span><span class="arrow open"></span>').parents('li').addClass('active');
    });


</script>