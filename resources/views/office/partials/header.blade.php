<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <div class="page-logo">
            <a href="#">
{{--
                <img src="{{asset('assets/admin/layouts/layout2/img/logo-default.png')}}" alt="logo" class="logo-default" /> </a>
--}}
            <div class="menu-toggler sidebar-toggler">
            </div>
        </div>
        <a href="javascript:" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
{{--
        <div class="page-actions">
            <div class="btn-group">
                <button type="button" class="btn btn-circle btn-outline red dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-plus"></i>&nbsp;
                    <span class="hidden-sm hidden-xs">Yeni&nbsp;</span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="javascript:">
                        <a href="/admin/user"> <i class="fa fa-users"></i> Üyeler</a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <i class="icon-docs"></i> Yeni Kategori </a>
                    </li>
                    <li>
                        <a href="javascript:">
                            <i class="icon-tag"></i> Yeni Ürün </a>
                    </li>
                </ul>
            </div>
        </div>
--}}
        <div class="page-top">
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown dropdown-user">
                        <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile">  <small><small></small></small> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{action('Auth\OfficeController@getLogout')}}">
                                    <i class="icon-key"></i> Çıkış Yap </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
