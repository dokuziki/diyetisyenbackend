@extends('office.layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Porsiyon Listesi</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                    </div>

                    <div class="portlet-body">
                        <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                               id="sample_1">
                            <thead>
                            <tr>
                                <th> Kategori</th>
                                <th width="100"> İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr class="odd gradeX">
                                    <td> {{$item['name']}}</td>

                                    <td>
                                        {{--<button class="btn btn-sm btn-danger deleteOpenModal" data-id="{{$user['id']}}" data-deleted="tr.gradeX"><i class="fa fa-trash"></i></button>--}}
                                        <a href="{{asset('admin/portion/detail')}}/{{$item->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-search"></i></a>
                                        <a href="{{asset('admin/portion/index')}}/{{$item->id}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <button class="btn btn-sm btn-danger deleteOpenModal" data-id="{{$item->id}}" data-deleted="tr.gradeX"><i class="fa fa-trash"></i></button>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
</div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Yeni Kategori</span>
                    </div>
                </div>
                <div class="content">
                    <form method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id"   @if(isset($portion['id'])) value="{{$portion['id']}}" @else value="false" @endif />
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="row no-margin">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label class="control-label">Kategori Adı</label>
                                    <input type="text" class="form-control" name="title" @if(isset($portion['name'])) value="{{$portion['name']}}" @endif />
                                </div>
                            </div>
                            <div class="col-xs-8">
                                <div class="form-group">
                                    <label class="control-label">Açıklama</label>
                                    <input type="text" class="form-control" name="description"  @if(isset($portion['description'])) value="{{$portion['description']}}" @endif  />
                                </div>
                            </div>
                        </div>
                        <div class="row no-margin">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> Ekle</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{'Kategoriyi Silmek İstediğinize Emin Misiniz ?'}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Vazgeç</button>
                    <submit type="button" class="btn btn-danger delete" id="newEditorAdd">Kategoriyi Sil</submit>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });

            var body = $('body');



            body.on('click','.deleteOpenModal',function(event){
                $('#deleteConfirm').modal('show');
                var dataID = $(this).attr('data-id');

                var button = $(this);
                var deleted = $(this).attr('data-deleted');

                $('.delete').click(function (){
                    window.location= '{{action('Office\PortionController@getDeleteCategory')}}'+'/'+dataID;
                });

            });
        });


    </script>
        @endsection