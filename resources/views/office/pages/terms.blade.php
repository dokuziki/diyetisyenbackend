@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Üyelik Sözleşmesi ve Kullanım Koşulları </span>
                    </div>

                </div>
                <div class="portlet-body">

                    <!-- BEGIN FORM-->
                    <form action="{{action('Office\TermsController@postTerms')}}" method="post" class="horizontal-form">
                        <input type="hidden" id="id" value="{{$item['id']}}">
                        <div class="form-body">
                            <div class="row">


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Açıklama</label>
                                        <textarea class="form-control" rows="12" id="description" name="description">{{$item['content']}}</textarea>
                                    </div>
                                </div>
                                <!--/span-->
                                <!--/span-->
                            </div>
                            <!--/row-->

                        </div>
                        <br>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Kaydet
                            </button>
                        </div>
                    </form>
                    <!-- END FORM-->

                    <style>
                        .orderRow td:first-child{
                            display:none;
                        }

                        .orderRow  td:nth-child(3) {
                            text-align: center;
                        }
                        .table th:first-child{
                            display:none;
                        }

                        textarea {
                            resize: none;
                        }
                    </style>

                    <br>
                    <br>





                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>

@endsection