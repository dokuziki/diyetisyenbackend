@extends('office.layouts.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Şikayetler</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                           id="sample_1">
                        <thead>
                        <tr>
                            <th> ID</th>
                            <th> Adı Soyadı</th>
                            <th> Şikayet Eden</th>
                            <th> İşlemler</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($spams as $spam)
                        <tr class="odd gradeX">
                            <td> {{$spam['id']}}</td>
                            <td> {{$spam->user->first_name}} {{$spam->user->last_name}}</td>
                            <td> {{$spam->adder->first_name}} {{$spam->adder->last_name}}</td>
                            <td>
                                {{--<button class="btn btn-sm btn-danger deleteOpenModal" data-id="{{$user['id']}}" data-deleted="tr.gradeX"><i class="fa fa-trash"></i></button>--}}
                                <a href="{{asset('admin/user/delete-spam')}}/{{$spam->id}}" data-action="add" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>

<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{config('messages.admin.confirmDelete')}}</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Vazgeç</button>
                <submit type="button" class="btn btn-danger delete" id="newEditorAdd">Üyeyi Sil</submit>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        $("#mask_date").inputmask("d/m/y", {
            autoUnmask: true
        });

        var body = $('body');



        body.on('click','.deleteOpenModal',function(event){
            $('#deleteConfirm').modal('show');
            var dataID = $(this).attr('data-id');

            var button = $(this);
            var deleted = $(this).attr('data-deleted');

            $('.delete').click(function (){
                window.location= '#'+'/'+dataID;
            });

        });
    });


</script>
@endsection