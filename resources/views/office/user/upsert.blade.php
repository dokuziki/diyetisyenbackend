@extends('office.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Kullanıcı Ayarı</span>
                    </div>

                </div>
                <div class="portlet-body">

                    <!-- BEGIN FORM-->
                    <form action="{{asset('admin/user/upsert')}}@if(isset($item['id']))/{{$item['id']}}@endif" method="post" class="horizontal-form" enctype="multipart/form-data">
                            <input type="hidden" name="id"   @if(isset($item['id'])) value="{{$item['id']}}" @else value="false" @endif />

                        <div class="form-body">
                            <h3 class="form-section">Hesap Bilgileri</h3>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <div class="form-group">
                                    </div>
                                    @if(isset($item['pic']) && $item['pic'] !== '' &&  $item['pic'] !== asset('uploads/'))
                                        <img height="200" src="{{asset($item['pic'])}}" />
                                    @endif
                                </div>
                            </div><br><br>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Adı</label>
                                        <input type="text" name="first_name" class="form-control" placeholder="" @if(isset($item['first_name'])) value="{{$item['first_name']}}" @endif autocomplete="new-password" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Soyadı</label>
                                        <input type="text" name="last_name" class="form-control" placeholder="" @if(isset($item['last_name'])) value="{{$item['last_name']}}" @endif autocomplete="new-password" required>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Mail Adresi</label>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($item['email'])) value="{{$item['email']}}" @endif autocomplete="new-password" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Şifre</label>
                                        <input type="password" name="password" class="form-control" placeholder="" >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single" >Resim</label>
                                        <input type="file" id="image" name="image" class="form-control" placeholder="Resim">
                                    </div>
                                </div>
                                <!--/span-->
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <br>
                            <br>
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Cinsiyet</label>
                                        <select id="gender"  name="gender" class="form-control ">
                                            <option value="0" @if((int)$item['gender'] === 0) selected @endif>Bayan</option>
                                            <option value="1" @if((int)$item['gender'] === 1) selected @endif>Bay</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Hesap Tipi</label>
                                        <select id="role"  name="role" class="form-control ">
                                            <option value="0" @if((int)$item['status'] === 0) selected @endif>Normal Üye</option>
                                            <option value="1" @if((int)$item['status'] === 1) selected @endif>Diyetisyen</option>
                                            <option value="2" @if((int)$item['status'] === 2) selected @endif>Yönetici</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Üyelik Tipi</label>
                                        <select id="forcep"  name="forcep" class="form-control ">
                                            <option value="0" @if((int)$item['forcep'] === 0) selected @endif>Standart</option>
                                            <option value="1" @if((int)$item['forcep'] === 1) selected @endif>Premium</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>

                        </div>
                        <br>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Kaydet
                            </button>
                        </div>
                    </form>
                    <!-- END FORM-->

                        <style>
                            .orderRow td:first-child{
                                display:none;
                            }

                            .orderRow  td:nth-child(3) {
                                text-align: center;
                            }
                            .table th:first-child{
                                display:none;
                            }
                        </style>

                    <br>
                    <br>





                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
    <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2"  style="height: 90px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$total_count}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>TOPLAM ÖĞÜN</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2"  style="height: 90px;">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$approve_count}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>ONAYLANMIŞ ÖĞÜN</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2"  style="height: 90px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$comment_count}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>TOPLAM YORUM</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat2"  style="height: 90px">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$point_count}}">0</span>
                            <small class="font-green-sharp"></small>
                        </h3>
                        <small>TAM PUAN ÖĞÜN</small>
                    </div>
                </div>
            </div>
        </div>

            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="dashboard-stat2"  style="height: 90px;">
                    <div class="display">
                        <div class="number">
                                @if($premium !== null)
                                    <span style="font-size: 18px;">EVET</span>
                                @else
                                    <span style="font-size: 18px;">HAYIR</span>
                                @endif
                                <small class="font-green-sharp"></small><br><br>
                            <small>PREMIUM ÜYE</small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="dashboard-stat2"  style="height: 90px;">
                    <div class="display">
                        <div class="number">
                            @if($diet_list !== null)
                                <span style="font-size: 18px;">EVET</span>
                            @else
                                <span style="font-size: 18px;">HAYIR</span>
                            @endif
                            <small class="font-green-sharp"></small><br><br>
                            <small>DİYET LİSTESİ</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Hasta Detayı</span>
                    </div>

                </div>


                <div class="portlet-body">
                    <div class="row">

                    <!-- BEGIN FORM-->
                    <form action="{{asset('backoffice/user/upsert')}}@if(isset($item['id']))/{{$item['id']}}@endif" method="post" class="horizontal-form">
                        <input type="hidden" name="id"   @if(isset($item['id'])) value="{{$item['id']}}" @else value="false" @endif />

                        <div class="form-body">
                            <h3 class="form-section">Hasta Bilgileri</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"></label>
                                        <canvas id="myChart" width="997px" height="200px">
                                            @if(isset($item['weightLogs']))
                                                @foreach($item['weightLogs'] as $log)
                                                    <div class="logs">{{$log->weight}}</div>
                                                @endforeach
                                            @endif
                                        </canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--/span-->
                                <!--/span-->

                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Boy</label>
                                        <input type="text" name="email" class="form-control" placeholder="" value="{{$item['details']['height']}}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Hedef Kilo</label>
                                        <input type="text" name="email" class="form-control" placeholder="" value="{{$item['details']['target_weight']}}" disabled>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Yemediğiniz ve/veya alerjiniz olan besinler var mı ? </b></label>
                                        <textarea class="form-control" rows="3" disabled>{{$item['details']['q1']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Kabızlık problemi yaşıyor musunuz ?</b></label>
                                        <textarea class="form-control" rows="3" disabled>{{$item['details']['q2']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Sürekli olarak kullanmakta olduğunuz ilaçlar var mı ? </b></label>
                                        <textarea class="form-control" rows="3" disabled>{{$item['details']['q3']}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="single"><b>Sağlık durumunuz veya belirtmek istediğinz bir hastalığınız var mı ?</b></label>
                                        <textarea class="form-control" rows="3" disabled>{{$item['details']['q4']}}</textarea>
                                    </div>
                                </div>

                            </div>
                            <!--/row-->
                            <br>
                            <br>
                            <div class="row">

                                <!--/span-->
                            </div>
                            <br>
                            <br>

                        </div>
                        <br>
                    </form>
                    <!-- END FORM-->

                    <style>
                        .orderRow td:first-child{
                            display:none;
                        }

                        .orderRow  td:nth-child(3) {
                            text-align: center;
                        }
                        .table th:first-child{
                            display:none;
                        }
                    </style>

                    <br>
                    <br>





                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/admin/global/scripts/Chart.min.js')}}" type="text/javascript"></script>
    <script>
        var ctx = document.getElementById("myChart");
        var DATA = [];
        function deneme(){
            var elems = document.getElementsByClassName("logs");
            for(elem of elems) {
                DATA.push(elem.innerHTML);
            }
            console.log(DATA);
        }
        deneme();

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: DATA,
                datasets: [{
                    label: 'Kilo Grafiği',
                    data: DATA,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
        });
    </script>


@endsection