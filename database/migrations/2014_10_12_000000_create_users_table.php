<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('nickname')->nullable()->default(null);
            $table->string('fb_id',255)->default("");
            $table->text('fb_token')->nullable();
            $table->string('email', 255)->unique();
            $table->string('password', 255);
            $table->date('birthday')->nullable();
            $table->string('pic',200)->default("");
            $table->string('phone')->nullable();
            $table->string('location')->nullable();
            $table->string('bio')->nullable();
            $table->integer('forcep')->unsigned()->default(0);


            $table->integer('gender')->unsigned()->default(2);
            $table->integer('status')->unsigned();

            $table->integer('nutritionist_id')->unsigned()->nullable();

            $table->rememberToken();
            $table->index('remember_token');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table)
        {
            $table->dropIndex(['remember_token']);
        });

        Schema::drop('users');
    }
}
