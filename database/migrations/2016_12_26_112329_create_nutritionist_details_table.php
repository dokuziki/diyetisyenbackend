<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionistDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutritionist_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nutritionist_id')->unsigned();
            $table->string('title',255);
            $table->string('video',255);
            $table->string('information',500);
            $table->text('description');
            $table->string('instagram',255);
            $table->string('facebook',255);
            $table->string('twitter',255);
            $table->string('linkedin',255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nutritionist_details');
    }
}
