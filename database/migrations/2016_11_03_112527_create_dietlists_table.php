<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDietlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dietlists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('nutritionist_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('purpose');
            $table->string('description')->nullable();
            $table->date('date')->nullable();
            $table->text('sabah')->nullable();
            $table->text('kahvalti')->nullable();
            $table->text('ara_ogun1')->nullable();
            $table->text('ogle')->nullable();
            $table->text('ara_ogun2')->nullable();
            $table->text('aksam')->nullable();
            $table->text('ara_ogun3')->nullable();
            $table->text('notlar')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dietlists');
    }
}
