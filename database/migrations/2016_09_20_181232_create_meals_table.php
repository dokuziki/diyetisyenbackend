<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('pic', 255);
            $table->date('date');
            $table->integer('time_id')->unsigned();
            $table->integer('approved');
            $table->integer('status')->default(1);
            $table->integer('chat')->default(0);
            $table->text('desc');
            $table->integer('point');
            $table->integer('push_status');
            $table->timestamps();
        });

        Schema::table('meals', function($table){
            $table->foreign('time_id')->references('id')->on('times')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meals');
    }
}
