<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class LikessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $faker = Faker::create();

        $users = User::where('status',0)->get();
        $meals = \App\Models\Meal::select(['id'])->get();
        $mealIDs = [];

        foreach($meals as $meal){
            $mealIDs[] =  $meal->id;
        }

        foreach($users as $user){
            $count = mt_rand (1,30);
            for($i=0;$i<$count; ++$i){
                $id = $faker->randomElement($mealIDs);

                $check = \App\Models\Likes::where('meal_id',$id)->where('user_id',$user->id)->first();

                if($check === null)
                $data[] = [
                    'user_id' => $user->id,
                    'meal_id' => $id
                ];
            }

            \App\Models\Likes::insert($data);

        }


    }
}
