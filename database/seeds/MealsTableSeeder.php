<?php

use App\Models\Meal;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class MealsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
        $imageA = ['test.jpg'];


        $data = [
            [
                'user_id' => 4,
                'pic'     => $faker->randomElement($imageA),
                'date'    => '2016-08-16',
                'time_id' => 1,
                'approved' => 1,
                'desc'    => 'Deneme içerik',
                'point'   => 1,
                'push_status' => 1
            ],
            [
                'user_id' => 4,
                'pic'     => $faker->randomElement($imageA),
                'date'    => '2016-08-16',
                'time_id' => 2,
                'approved' => 1,
                'desc'    => 'Deneme içerik',
                'point'   => 2,
                'push_status' => 1
            ],
            [
                'user_id' => 4,
                'pic'     => $faker->randomElement($imageA),
                'date'    => '2016-08-16',
                'time_id' => 4,
                'approved' => 1,
                'desc'    => 'Deneme içerik',
                'point'   => 5,
                'push_status' => 1
            ]
        ];


        \App\Models\Meal::insert($data);

        $users = User::where('status',0)->get();

        foreach($users as $user){
            $count = mt_rand (4,400);
            $data = [];
            for($i=0;$i<$count; ++$i){
                $date = \Carbon\Carbon::now()->subDays(mt_rand (0,100))->subHours(mt_rand (0,30));
                $timeid = mt_rand (1,6);

                $check = Meal::where('date',$date->format('Y-m-d'))->where('time_id',$timeid)->first();
                if($check === null)
                $data[] = [
                    'user_id' => $user->id,
                    'pic'     => $faker->randomElement($imageA),
                    'date'    => $date->format('Y-m-d'),
                    'time_id' => mt_rand (1,6),
                    'approved' =>  mt_rand (0,1),
                    'desc'    => $faker->sentence(4),
                    'point'   => mt_rand (3,10),
                    'push_status' => 1,
                    'created_at' => $date
                ];

            }

            \App\Models\Meal::insert($data);
        }


    }
}
