<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\User::where('status',0)->get();
        $data = [];
        $faker = Faker::create();

        foreach($users as $user){
            $target = mt_rand(45,120);
            $data[] = [
                'user_id' => $user->id,
                'target_weight' => $target,
                'height'=> mt_rand($target+10,$target+50),
                'purpose' =>  mt_rand(1,3),
                'q1' => $faker->sentence(8),
                'q2' => $faker->sentence(10),
                'q3' => $faker->sentence(11),
                'q4' => $faker->sentence(8)
            ];
        }

        \App\Models\UserDetail::insert($data);
    }
}
