<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class WeightsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = \App\Models\User::where('status',0)->get();
        $data = [];
/*        $faker = Faker::create();*/

        foreach($users as $user){
            $target = mt_rand(45,120);
            $data[] = [
                'user_id' => $user->id,
                'weight' => $target
            ];
        }


        \App\Models\Weight::insert($data);
    }
}
