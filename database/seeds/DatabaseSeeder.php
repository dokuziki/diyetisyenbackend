<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(TimessTableSeeder::class);
        $this->call(MealsTableSeeder::class);
        $this->call(LikessTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(NotificationsTableSeeder::class);
        $this->call(UserDetailsTableSeeder::class);
        $this->call(WeightsTableSeeder::class);
        $this->call(DietlistsTableSeeder::class);
        //$this->call(NutritionistsTableSeeder::class);



        Model::reguard();
    }
}
