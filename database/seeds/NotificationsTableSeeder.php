<?php

use Illuminate\Database\Seeder;

class NotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [

                'user_id'     => 2,
                'message'     => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'content_type'=> 'meal',
                'content_id'  => 1,
                'is_global'   => 0

            ],
            [

                'user_id'     => 2,
                'message'     => 'Aliquam molestie dignissim massa, et lacinia tellus.',
                'content_type'=> 'system',
                'content_id'  => 1,
                'is_global'   => 0

            ],
            [

                'user_id'     => 3,
                'message'     => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'content_type'=> 'meal',
                'content_id'  => 1,
                'is_global'   => 0

            ],
            [

                'user_id'     => 3,
                'message'     => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'content_type'=> 'system',
                'content_id'  => 1,
                'is_global'   => 0

            ],
        ];


        \App\Models\Notification::insert($data);
    }
}
