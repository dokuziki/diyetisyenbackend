<?php

use Illuminate\Database\Seeder;

class TimessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'id' => 1,
                'name'     => 'Kahvaltı',
                'push_title' => 'kahvaltınıza'

            ],
            [
                'id' => 2,
                'name'     => 'Ara Öğün 1',
                'push_title' => 'ara öğününüze'

            ],
            [
                'id' => 3,
                'name'     => 'Öğle',
                'push_title' => 'öğle yemeğinize'

            ],
            [
                'id' => 4,
                'name'     => 'Ara Öğün 2',
                'push_title' => 'ara öğününüze'
            ],
            [
                'id' => 5,
                'name'     => 'Akşam',
                'push_title' => 'akşam yemeğinize'

            ],
            [
                'id' => 6,
                'name'     => 'Ara Öğün 3',
                'push_title' => 'ara öğününüze'

            ],
        ];


        \App\Models\Times::insert($data);
    }
}
