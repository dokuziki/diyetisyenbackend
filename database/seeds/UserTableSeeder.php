<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $imageA = ['default.jpg'];

        $data = [
            [
                'first_name' => 'Bilgin',
                'last_name' => 'Alizan',
                'nickname' => 'bilginalizan',
                'email'=> 'cosku@dokuziki.com',
                'password' => bcrypt('123456'),
                'status' => 1,
                'nutritionist_id' => null,
                'pic' => $faker->randomElement($imageA)
            ],

            [
                'first_name' => 'Okan',
                'last_name' => 'Yüksel',
                'nickname' => 'okanyuksel',
                'email'=> 'okan.yuksel@ward.agency',
                'password' => bcrypt('123456'),
                'status' => 1,
                'nutritionist_id' => 1,
                'pic' => $faker->randomElement($imageA)
            ],
            [
                'first_name' => 'Meşhur',
                'last_name' => 'Diyetisyen',
                'nickname' => 'diyeterboy',
                'email'=> 'diyet@ward.agency',
                'password' => bcrypt('123456'),
                'status' => 1,
                'nutritionist_id' => 1,
                'pic' => $faker->randomElement($imageA)
            ],
            [
                'first_name' => 'Nurten Gül',
                'last_name' => 'Gülen',
                'nickname' => 'diyeterboy',
                'email'=> 'diyet@ward.nurtencom',
                'password' => bcrypt('123456'),
                'status' => 1,
                'nutritionist_id' => 1,
                'pic' => $faker->randomElement($imageA)
            ],
            [
                'first_name' => 'Pakize',
                'last_name' => 'Suda',
                'nickname' => 'sudapakize',
                'email'=> 'pakize@ward.nurtencom',
                'password' => bcrypt('123456'),
                'status' => 1,
                'nutritionist_id' => 1,
                'pic' => $faker->randomElement($imageA)
            ],
            [
                'first_name' => 'Can',
                'last_name' => 'Aksoy',
                'nickname' => 'canaksoy',
                'email'=> 'canaksoy@gmail.com',
                'password' => bcrypt('123456'),
                'status' => 2,
                'nutritionist_id' => 1,
                'pic' => $faker->randomElement($imageA)
            ],
            [
                'first_name' => 'Özgür',
                'last_name' => 'Ersoz',
                'nickname' => 'ozgurersoz',
                'email'=> 'ozgur@test.com',
                'password' => bcrypt('123456'),
                'status' => 0,
                'nutritionist_id' => 1,
                'pic' => $faker->randomElement($imageA)
            ],
            [
                'first_name' => 'Nurten',
                'last_name' => 'Peköz',
                'nickname' => 'nurtenn',
                'email'=> 'nurten@test.com',
                'password' => bcrypt('123456'),
                'status' => 0,
                'nutritionist_id' => 1,
                'pic' => $faker->randomElement($imageA)
            ],
            [
                'first_name' => 'Aliye',
                'last_name' => 'Semar',
                'nickname' => 'aliyesemar',
                'email'=> 'aliye@test.com',
                'password' => bcrypt('123456'),
                'status' => 0,
                'nutritionist_id' => 1,
                'pic' => $faker->randomElement($imageA)
            ]
        ];


        \App\Models\User::insert($data);

        $dataT = [];
        for($i=0;$i<130; ++$i){
            $date = \Carbon\Carbon::now()->subDays(mt_rand (0,30))->subHours(mt_rand (0,10));
            $nit = User::where('status',1)->orderByRaw("RAND()")->first();
            $dataT[] = [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'nickname' => $faker->userName,
                'email'=> $faker->email,
                'password' => bcrypt( $faker->password(6)),
                'status' => 0,
                'nutritionist_id' => $nit->id,
                'created_at' => $date,
                'pic' => $faker->randomElement($imageA)
            ];
        }

        \App\Models\User::insert($dataT);
    }
}
