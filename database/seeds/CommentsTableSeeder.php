<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $data = [];
        $faker = Faker::create();

        $users = User::where('status',1)->get();

        foreach($users as $user){

            $meals = \App\Models\Meal::select(['id'])->get();

            foreach($meals as $meal){
                $date = \Carbon\Carbon::now()->subDays(mt_rand (0,10))->subHours(mt_rand (0,10));
                $data[] = [
                    'meal_id' => $meal->id,
                    'user_id' => $user->id,
                    'comment' => $faker->sentence(5),
                    'created_at' => $date
                ];
            }


        }

        \App\Models\Comment::insert($data);




    }
}
