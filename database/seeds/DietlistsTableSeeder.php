<?php

use Illuminate\Database\Seeder;

class DietlistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'user_id' => 3,
                'title'     => 'Yaza Hazırlık Diyeti',
                'date'    => '2016-08-16',
                'sabah' => '2 Bardak Su',
                'kahvalti' => '3 Haşlanmış Yumurta, 5 Tane Zeytin, Zencefilli Yeşil Çay',
                'ara_ogun1'    => '3 tane ceviz',
                'ogle'   => '180 Gram ızgara tavuk, bol yeşillikli salata',
                'ara_ogun2' => '1 kase yoğurt',
                'aksam' => '3 kaşık fasulye, 2 yemek kaşığı bulgur pilavı',
                'ara_ogun3' => '1 bardak kefir',
                'notlar' => 'yemekler kızartılmamalı, ekmek günde 4 dilimden fazla olmamalı, gün içerisinde bol su'
            ]
    ];
        \App\Models\Dietlist::insert($data);
    }
}
